package entities;

import static utilz.constants.Projectiles.*;

public abstract class Projectiles extends Entity{
	
	private int aniIndex, projectileType;
	private int aniTick, aniSpeed = 25;

	public Projectiles(float x, float y, int width, int height,int projectileType) {
		super(x, y, width, height);
		
		this.projectileType =  projectileType;
		initHitbox(x, y, width, height);
		
	}
	
	
	private void updateAnimationTick() {
		aniTick++;
		if(aniTick>=aniSpeed) {
			aniTick=0;
			aniIndex++;
			if(aniIndex >= GetSpriteAmount_projectiles(projectileType) ) {
				aniIndex = 0;
			}
		}
	}
	public void update() {
		updateAnimationTick();
	}
	
	public int getAniIndex() {
		return aniIndex;
	}
	
	
	
}
