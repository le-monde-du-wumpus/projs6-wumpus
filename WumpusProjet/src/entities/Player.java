package entities;
import static utilz.constants.PlayerConstants.GetSpriteAmount;
import static utilz.constants.PlayerConstants.idle;
import static utilz.constants.PlayerConstants.running;
import static utilz.constants.PlayerConstants.shooting;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import gamestates.*;
import main.Game;
import main.LecteurAudio;
import utilz.LoadSave;  

public class Player extends Entity {

	
	private int animationTick, animationIndex, animationSpeed = 40;
	private int player_action = idle;
	private boolean moving = false,shooting = false;
	private boolean left,up,right,down;
	private float playerSpeed = 2.0f;
	private ArrayList<int[]> OrRamasses;

	private float xDrawOffset = 5 * Game.SCALE;
	private float yDrawOffset = 30 * Game.SCALE;
	private Game game;
	private Playing playing;
	private int Arrow;
	
	private int WumpusTuer;
	
	private BufferedImage[][] animations;
	
	
	
	public Player(float x, float y, int width, int height, Playing playing) {
		super(x, y,width,height);
		loadAnimations();
		initHitbox(x, y,(float)( 0.6*Game.TILES_DEFAULT_SIZE*Game.SCALE),(float)(0.6*Game.TILES_DEFAULT_SIZE*Game.SCALE));
		this.playing = playing;
		this.OrRamasses = new ArrayList<int[]>();
		this.Arrow = playing.getPlateauHandler().getPlateau().getNbWumus();
		WumpusTuer = 0;
	}
	
	public void update() { 
		updatePos();
		updateAnimationTick();
		setAnimation();
		
		
	}
	
	public void render(Graphics g) {
		switch(gamestates.state) {
		case PLAYING:
			g.drawImage(animations[player_action][animationIndex],(int)( hitbox.x - xDrawOffset), (int)(hitbox.y - yDrawOffset),  50,100, null);
			g.drawImage(animations[player_action][animationIndex],(int)( 23.3*Game.TILES_SIZE ), (int)(3.15*Game.TILES_SIZE),  20,40, null);
			//drawHitbox(g);
			break;
		case MAP:
			int DimX = playing.getPlateauHandler().getPlateau().getX();
			int DimY = playing.getPlateauHandler().getPlateau().getY();
			
			
			int tailleX = Game.GAME_WIDTH, tailleY = Game.GAME_HEIGHT;
			
			int size =  Math.min(tailleX/(DimY*2),tailleY/(DimX*2));
			int offsetX=(int)(tailleX/(2.0)-(DimY/2.0)*(size+3));
			int offsetY = (int)(tailleY/(2.0)-(DimX/2.0)*(size+3));
			
			
			int[] playerPos = playing.getLevelManager().getPlayerPos();
			g.drawImage(animations[player_action][animationIndex],(int)(( offsetX+ size/4) + (playerPos[1]*(size+3))), (int)(offsetY)+(playerPos[0]*(size+3)),  size/2,size, null);
			
			break;
		case DEATHSCREEN : 
			 DimX = playing.getPlateauHandler().getPlateau().getX();
			 DimY = playing.getPlateauHandler().getPlateau().getY();
			
			
			 tailleX = Game.GAME_WIDTH;
			tailleY = Game.GAME_HEIGHT;
			
			 size =  Math.min(tailleX/(DimY*2),tailleY/(DimX*2));
			 offsetX=(int)(tailleX/(2.0)-(DimY/2.0)*(size+3));
			 offsetY = (int)(tailleY/(2.0)-(DimX/2.0)*(size+3));
			 
			
			
			 playerPos = playing.getLevelManager().getPlayerPos();
			g.drawImage(animations[player_action][animationIndex],(int)((offsetX+ size/4) + (playerPos[1]*(size+3))), (int)(offsetY)+ (playerPos[0]*(size+3)),  size/2,size, null);
			break;
		case WINSCREEN : 
			 DimX = playing.getPlateauHandler().getPlateau().getX();
			 DimY = playing.getPlateauHandler().getPlateau().getY();
			
			 
			 tailleX = Game.GAME_WIDTH;
				tailleY = Game.GAME_HEIGHT;
			
			 size =  Math.min(tailleX/(DimY*2),tailleY/(DimX*2));
			 offsetX=(int)(tailleX/(2.0)-(DimY/2.0)*(size+3));
			 offsetY = (int)(tailleY/(2.0)-(DimX/2.0)*(size+3));
			 
			
			
			 playerPos = playing.getLevelManager().getPlayerPos();
			g.drawImage(animations[player_action][animationIndex],(int)((offsetX+ size/4) + (playerPos[1]*(size+3))), (int)(offsetY)+ (playerPos[0]*(size+3)),  size/2,size, null);
			break;	
		case AGENTWIN : 
			 DimX = playing.getPlateauHandler().getPlateau().getX();
			 DimY = playing.getPlateauHandler().getPlateau().getY();
			
			 
			 tailleX = Game.GAME_WIDTH;
				tailleY = Game.GAME_HEIGHT;
			
			 size =  Math.min(tailleX/(DimY*2),tailleY/(DimX*2));
			 offsetX=(int)(tailleX/(2.0)-(DimY/2.0)*(size+3));
			 offsetY = (int)(tailleY/(2.0)-(DimX/2.0)*(size+3));
			 
			
			
			 playerPos = playing.getLevelManager().getPlayerPos();
			g.drawImage(animations[player_action][animationIndex],(int)((offsetX+ size/4) + (playerPos[1]*(size+3))), (int)(offsetY)+ (playerPos[0]*(size+3)),  size/2,size, null);
			break;
		default:
			break;
			}
		
		
	}
	
	
	private void updateAnimationTick() {
		
		animationTick++;
		if(animationTick>= animationSpeed) {
			animationTick=0;
			animationIndex++;
			if(animationIndex >= GetSpriteAmount(player_action)) {
				animationIndex = 0; 
				shooting = false;
			}
		}	
	}
	
	public void tuerWumpus(int dir) {
		boolean success = false; 
		//0 est
		// 1 sud 
		// 2 ouest 
		//3 nord
		if(this.Arrow > 0 ) {
			int[] playerPos = playing.getLevelManager().getPlayerPos();
			
			switch(dir) {
				default : 
					break;
				case 0:
					success=playing.getPlateauHandler().getPlateau().killWumpus(playerPos[0], playerPos[1]+1);
					break;
				case 1 : 
					success= playing.getPlateauHandler().getPlateau().killWumpus(playerPos[0]+1, playerPos[1]);
					break;
				case 2 : 
					success= playing.getPlateauHandler().getPlateau().killWumpus(playerPos[0], playerPos[1]-1);
					break;
				case 3 : 
					success= playing.getPlateauHandler().getPlateau().killWumpus(playerPos[0]-1, playerPos[1]);
					break;	
					
			}
			if(success) {
				playing.getLevelManager().updateMap();
				playing.WumpusRugissement = true;
				WumpusTuer++;
			}
			Arrow--;
			LecteurAudio.ArrowFire();
		}else {
			LecteurAudio.ArrowErr();
		}
		
		
	}
	

	private void setAnimation() {
		
		int startAnimation = player_action;
		
		if(moving) {
			player_action = running;
		} else {
			player_action= idle;
		}
		
		if(shooting) {
			player_action = utilz.constants.PlayerConstants.shooting;
			
		}
		if (startAnimation != player_action) {
			resetAnimation();
		}
		
		
		
	}
	private void resetAnimation() {
		animationTick = 0;
		animationIndex = 0;
		
	}

	private void updatePos() {
		
		moving = false;
		if(!left && !right && !up && !down) {
			return;
		}
		float xSpeed = 0, ySpeed = 0;
		
		if(left && !right) {
		 xSpeed = -playerSpeed;	
		 
		}else if(right && !left){
			xSpeed = playerSpeed;	
		}
		if(up && !down) {
			ySpeed = -playerSpeed;
			
		}else if(down && !up) {
			ySpeed = playerSpeed;
			
		}
		
		if(playing.hitboxMethods.CanMoveHere(hitbox.x+xSpeed, hitbox.y+ySpeed, hitbox.width, hitbox.height, playing.levelManager.getCurrentLevel().getLevelData())) {
			hitbox.x+=xSpeed;
			hitbox.y+=ySpeed;
			moving=true;
			
		}
		float tilesize = Game.TILES_DEFAULT_SIZE*Game.SCALE;
		float offscale = 2*tilesize;
		
		
		switch(playing.hitboxMethods.isOnDoor( xDrawOffset + hitbox.x,yDrawOffset + hitbox.y, playing.levelManager.getCurrentLevel().getLevelData())) {
		case 1 : //Porte Nord
			if(playing.Enableagent && playing.dif_agent == 0) {
				playing.MakeAgentPlay();
			}
			
			hitbox.x = tilesize*9 +offscale ;
			hitbox.y =  tilesize*9 +offscale;
			break;
		case 2: //Porte Ouest
			if(playing.Enableagent && playing.dif_agent == 0) {
				playing.MakeAgentPlay();
			}
			hitbox.x = tilesize*17 +offscale  ;
			hitbox.y = tilesize*5 +offscale ;
			break;
		case 3: //Porte est
			if(playing.Enableagent && playing.dif_agent == 0) {
				playing.MakeAgentPlay();
			}
			hitbox.x =  tilesize*1 +offscale;
			hitbox.y = tilesize*5 +offscale ;
			break;
		case 4: //Porte sud
			if(playing.Enableagent && playing.dif_agent == 0) {
				playing.MakeAgentPlay();
			}
			hitbox.x = tilesize*9 +offscale;
			hitbox.y = tilesize*1 +offscale ;
			break;
		}
		
	}
	
	
	private void loadAnimations() { // crée un double Array de toutes les animations de la spritesheet
		
				BufferedImage img = LoadSave.GetSpriteAtlas(LoadSave.PLAYER_ATLAS);
				
				animations = new BufferedImage[5][6];
				for(int j = 0; j < animations.length;j++) {
					for(int i = 0; i <  animations.length; i++) {
						animations[j][i] = img.getSubimage(i*50, j*100, 50, 100);
					}
					
				}
			
	}
	
	public void refresh() {
		OrRamasses = new ArrayList<int[]>();
		this.Arrow = playing.getPlateauHandler().getPlateau().getNbWumus();
		WumpusTuer = 0;
		hitbox.x = (int)( 200);
		hitbox.y = (int)( 200);
	}
	
	public void ramasserOr() {
		int[] pos = playing.levelManager.getPlayerPos();
		int valpos = playing.getPlateauHandler().getPlateau().getMatrice()[pos[0]][pos[1]];
		if(valpos==2 || valpos==9 || valpos==10) {
			boolean exist =true;
			
			for(int[] o : this.OrRamasses) {
				if(o[0] == pos[0] && o[1] == pos[1]) {
					exist = false;
				}
			}
			if(exist) {
				
				OrRamasses.add(new int[] {pos[0],pos[1]});
				LecteurAudio.Gold();
			}
		} 
	}
	
	
	
	public void resetDirBooleans() {
		left = false;
		right = false;
		up = false;
		down= false;
	}

	public boolean isLeft() {
		return left;
	}

	public void setLeft(boolean left) {
		this.left = left;
	}

	public boolean isUp() {
		return up;
	}

	public void setUp(boolean up) {
		this.up = up;
	}

	public boolean isRight() {
		return right;
	}

	public void setRight(boolean right) {
		this.right = right;
	}

	public boolean isDown() {
		return down;
	}

	public void setDown(boolean down) {
		this.down = down;
	}

	public void setShooting(boolean shooting) {
		this.shooting = shooting;
	}
	
	public int nbOrRamasses() {
		return this.OrRamasses.size();
	}
	public int getArrow() {
		return this.Arrow;
	}
	public int getNbWumpusKill() {
		return this.WumpusTuer;
	}
	
}


