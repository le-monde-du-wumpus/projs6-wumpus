package entities;


import static utilz.constants.PlayerConstants.GetSpriteAmount;
import static utilz.constants.PlayerConstants.idle;
import static utilz.constants.PlayerConstants.running;
import static utilz.constants.PlayerConstants.shooting;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;

import gamestates.gamestates;
import main.Game;
import solve.Agent;
import utilz.LoadSave;  

public class EAgent extends Entity {

	
	private int animationTick, animationIndex, animationSpeed = 40;
	private int player_action = idle;
	private boolean moving = false,shooting = false;
	private boolean left,up,right,down;
	private float playerSpeed = 2.0f;


	private float xDrawOffset = 5 * Game.SCALE;
	private float yDrawOffset = 30 * Game.SCALE;
	private Game game;
	
	private BufferedImage image;
	
	private Agent agent;
	
	public EAgent(float x, float y, int width, int height, Game game) {
		super(x, y,width,height);
		loadAnimations();
		initHitbox(x, y,(float)( 0.6*Game.TILES_DEFAULT_SIZE*Game.SCALE),(float)(0.6*Game.TILES_DEFAULT_SIZE*Game.SCALE));
		this.game = game;
		
	}
	
	public void update() { 
		
	}

	
	public void render(Graphics g) {
		switch(gamestates.state) {
			default:
				break;
			case PLAYING:
				g.drawImage(image, (int)x,(int)y, Game.TILES_SIZE-10,2*(Game.TILES_SIZE-10), null);
				break;
			case MAP:
				int DimX = game.getPlaying().getPlateauHandler().getPlateau().getX();
				int DimY = game.getPlaying().getPlateauHandler().getPlateau().getY();
				
				
				int tailleX = Game.GAME_WIDTH, tailleY = Game.GAME_HEIGHT;
				
				int size =  Math.min(tailleX/(DimY*2),tailleY/(DimX*2));
				int offsetX=(int)(tailleX/(2.0)-(DimY/2.0)*(size+3));
				int offsetY = (int)(tailleY/(2.0)-(DimX/2.0)*(size+3));
				
				
				int[] playerPos = game.getPlaying().getAgent().getPos();
				g.drawImage(image,(int)(( offsetX+ size/4) + (playerPos[1]*(size+3))), (int)(offsetY)+(playerPos[0]*(size+3)),  size/2,size, null);
				break;
			case DEATHSCREEN:
				DimX = game.getPlaying().getPlateauHandler().getPlateau().getX();
				DimY = game.getPlaying().getPlateauHandler().getPlateau().getY();
				
				
				tailleX = Game.GAME_WIDTH;
				tailleY = Game.GAME_HEIGHT;
				
				size =  Math.min(tailleX/(DimY*2),tailleY/(DimX*2));
				offsetX=(int)(tailleX/(2.0)-(DimY/2.0)*(size+3));
				offsetY = (int)(tailleY/(2.0)-(DimX/2.0)*(size+3));
				
				
				playerPos = game.getPlaying().getAgent().getPos();
				g.drawImage(image,(int)(( offsetX+ size/4) + (playerPos[1]*(size+3))), (int)(offsetY)+(playerPos[0]*(size+3)),  size/2,size, null);
				break;
			case WINSCREEN:
				DimX = game.getPlaying().getPlateauHandler().getPlateau().getX();
				DimY = game.getPlaying().getPlateauHandler().getPlateau().getY();
				
				
				tailleX = Game.GAME_WIDTH;
				tailleY = Game.GAME_HEIGHT;
				
				size =  Math.min(tailleX/(DimY*2),tailleY/(DimX*2));
				offsetX=(int)(tailleX/(2.0)-(DimY/2.0)*(size+3));
				offsetY = (int)(tailleY/(2.0)-(DimX/2.0)*(size+3));
				
				
				playerPos = game.getPlaying().getAgent().getPos();
				g.drawImage(image,(int)(( offsetX+ size/4) + (playerPos[1]*(size+3))), (int)(offsetY)+(playerPos[0]*(size+3)),  size/2,size, null);
				break;
			case AGENTWIN:
				DimX = game.getPlaying().getPlateauHandler().getPlateau().getX();
				DimY = game.getPlaying().getPlateauHandler().getPlateau().getY();
				
				
				tailleX = Game.GAME_WIDTH;
				tailleY = Game.GAME_HEIGHT;
				
				size =  Math.min(tailleX/(DimY*2),tailleY/(DimX*2));
				offsetX=(int)(tailleX/(2.0)-(DimY/2.0)*(size+3));
				offsetY = (int)(tailleY/(2.0)-(DimX/2.0)*(size+3));
				
				
				playerPos = game.getPlaying().getAgent().getPos();
				g.drawImage(image,(int)(( offsetX+ size/4) + (playerPos[1]*(size+3))), (int)(offsetY)+(playerPos[0]*(size+3)),  size/2,size, null);
				break;
			
		}
	}
		
	private void loadAnimations() { // crée un double Array de toutes les animations de la spritesheet
		
				this.image = LoadSave.GetSpriteAtlas(LoadSave.AGENT_SPRITE);
				
			
	}

	
}


