package entities;

import static utilz.constants.Projectiles.*;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import java.util.List;

import gamestates.gamestates;
import main.Game;
import utilz.LoadSave;

public class Arrow {
	
	private BufferedImage[][] ArrowArr; 

	
	
	private Game game;
	
	public Arrow(Game game) {
		
		this.game = game;
		loadProjectileImgs();

		
	}
	
	


	public void update() {
		
	}
	
	public void draw(Graphics g) {
		switch(gamestates.state) {
			default:
				break;
			case PLAYING:				
				g.drawImage(ArrowArr[0][0],(int)(Game.TILES_SIZE*9) - Arrow_width_default, (int)(Game.TILES_SIZE*1.5) - Arrow_height_default ,  2*Arrow_width_default,2*Arrow_height_default, null);
				break;
		}
	}
	
	
	public void loadProjectileImgs(){
		ArrowArr = new BufferedImage[1][1]; 
		BufferedImage temp = LoadSave.GetSpriteAtlas(LoadSave.ARROW_SPRITE);
		for(int j = 0; j < ArrowArr.length; j++) {
			for(int i=0; i<ArrowArr[j].length;i++) {
				ArrowArr[j][i] = temp.getSubimage(i * Arrow_width_default, j* Arrow_height_default,  Arrow_width_default, Arrow_height_default);
				
			}
		}
	}
}
