package gen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import solve.Solver;



/**

La classe Plateau représente la carte du jeu "Le monde de Wumpus".
Elle est utilisée pour générer une matrice représentant la carte de jeu,
ainsi que pour stocker les informations sur les éléments de jeu, tels que
la position des trous, des wumpus, des ors, etc.
Les éléments de la carte sont représentés par des codes entiers :
0 => vides; 2 => Ors; 3 => Wumpus; 4 => Trou; 5 => Brise; 6 => Odeur;
7 => Odeur + Brise; 8 => Wumpus mort; 9 => Or + Brise; 10 => Or + Odeur 11 => Or + Odeur + Brise.
Le constructeur de la classe génére directement la carte dans la matrice du Plateau obtenable avec la methode public getMatrice
@see #getMatrice()
*/
public class Plateau {
    private int[][] matrice;
    private int x;
    private int y;
    private int[][] marked;
    private int seed;
    private Random rand;
    private int nbOr;
    private int recursionMax;
    private double tauxDeRecouvrement;
    private int nbWumpus;
    private int[][] explored;
    
    /**
    Constructeur de la classe Plateau qui permet de créer une nouvelle instance de Plateau.
    @param x la largeur du plateau
    @param y la hauteur du plateau
    @param seed de génération aléatoire du plateau
    @param recursionMax la profondeur maximale de récursion pour la génération de la carte
    @param tauxDeRecouvrement le taux de recouvrement maximal pour la génération de la carte
    @param nbOr le nombre d'or à placer sur le plateau
    @param nbWumpus le nombre de wumpus à placer sur le plateau
    */
    public Plateau(int x, int y, int seed, int recursionMax,
        double tauxDeRecouvrement, int nbOr,int nbWumpus)  {
        this.x = x;
        this.y = y;
        this.marked = new int[x][y];
        // génération de la seed aléatoire ou recupération de la seed transmise en paramètre
        int randseed;
        if (seed == -1) {
            Random rand = new Random();
            randseed = rand.nextInt((int) Math.pow(2, 32) - 1);
        } else {
            randseed = seed;
        }
        this.seed = randseed;
        this.rand = new Random(this.seed);
        this.nbOr = nbOr;
        this.tauxDeRecouvrement = tauxDeRecouvrement;
        this.recursionMax = recursionMax;
        
        // matrice des cellules explorer par le joueur
        this.explored = new int[x][y];        
		for(int i = 0 ; i<x;i++) {
			for(int j = 0 ; j<y ; j++) {
				explored[i][j] = 0;
			}
		}
		
		
		
		// rejet de la matrice et regénération si nécéssaire
		int nbWUmpusNeeded = -1; 
		int iteration = 0;
		int NbWumpusAimed = Math.max(1, ((x+y)/25));
		int[][] bestMap = new int[x][y];
		int best = 0;
		
		
		while(nbWUmpusNeeded < Math.max(NbWumpusAimed/4., NbWumpusAimed - (0.5 * iteration))) {	
			
			if(best > Math.max(NbWumpusAimed/2., NbWumpusAimed - (0.5 * iteration))) {
				for(int i = 0;i<x;i++) {
					for(int j = 0 ; j <y;j++) {
						matrice[i][j] = bestMap[i][j];
					}
				}
			}else {
				this.matrice = this.generateGrid();	 
			} 
			
			Solver solver = new Solver(this); 	
			nbWUmpusNeeded = solver.nbWumpusRequired();
			
			if(nbWUmpusNeeded >best) {
				best = nbWUmpusNeeded;
				for(int i = 0;i<x;i++) {
					for(int j = 0 ; j <y;j++) {
						bestMap[i][j] = matrice[i][j];
					}
				}
			}
			iteration++;
	  }
		int countW = 0;	
		int countG = 0;
		for(int i =0;i<x;i++) {
			for(int j = 0 ; j<y;j++) {
				if(this.matrice[i][j] == 3) {
					countW++;
				}
				if(this.matrice[i][j] == 2 || this.matrice[i][j] == 9 || this.matrice[i][j] ==10) {
					countG++;
				}
			}
		}
		this.nbWumpus = countW;
		this.nbOr = countG;
    }
    /**
    Constructeur de la classe Plateau qui permet de créer une nouvelle instance de Plateau.
    @param matrice du plateau de jeu
    */
   
    /**

    Cette méthode génère une matrice représentant la carte du jeu "Le monde de Wumpus".
    0 => vides; 2 => Ors; 3 => Wumpus; 4 => Trou; 5 => Brise; 6 => Odeur; 7 => Odeur + Brise; 8 => Wumpus mort; 9 => Or + Brise; 10 => Or + Odeur
    @return la matrice représentant la carte du jeu "Le monde de Wumpus"
    */
    private int[][] generateGrid() {     
        // genère la base de la matrice
        int[][] matrice = new int[x][y];
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                matrice[i][j] = 4;
            }
        }
        // création de zone convexe avec apelles recursif de DFS en plusieurs points+
        int[][] chemin = this.generateCave(matrice, recursionMax, tauxDeRecouvrement);     
        // applique le chemin a la base en supprimant les trous sittuer sur le chemin
        this.applyCave(matrice, chemin);
        // placement du Gold sur le matrice
        this.placementGold(matrice, chemin, nbOr);
        // ajoute les puanteur et brise autour des trou et du wumpus
        this.placementBrise(matrice);
        // pathfinding entre les Ors
        int[][] GoldPath = this.getGoldPath(matrice, nbOr);
        // ajustement du chemin        
        GoldPath = verificationPath(GoldPath, matrice); 
        // placement du wumpus sur le matrice
        this.placementWumpus(matrice, GoldPath);
        this.nbWumpus = this.updateNbWumpus(matrice);
        this.placementOdeur(matrice);
        //TempDisplayPath(matrice,GoldPath);
        verificationPath(GoldPath, matrice); 
        return matrice;
    } 


    /**
    Génère une carte de grotte en utilisant l'algorithme Depth-First Search.
    @param matrice la matrice de la carte avec les éléments initiaux
    @param recursionMax la profondeur maximale pour l'algorithme de recherche
    @param tauxDeRecouvrement le taux de recouvrement souhaité pour la carte de grotte générée
    @return la carte de grotte générée
    */
    private int[][] generateCave(int[][] matrice, int recursionMax,
            double tauxDeRecouvrement) {
        int[][] cave = new int[x][y];
        int[] coords = { 0, 0 };
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                this.marked[i][j] = 0;
            }
        }
        int tailleChemin = lenWay(cave);
        int sizeToMatch = (int) ((x*y)*tauxDeRecouvrement);
        while(tailleChemin < sizeToMatch) {
        	cave =  depthFirstSearch(coords, cave, matrice, recursionMax, tauxDeRecouvrement);
        	
        	int maxIte = 20;
        	while(this.countNeighborsNeighbors(coords[0],coords[1], cave) > 4 && maxIte > 0 ) {        		 
        		coords[0] = this.rand.nextInt(x);
        		coords[1] = this.rand.nextInt(y);
        	    maxIte--;
        	}
        	int newTailleChemin = lenWay(cave);
        	if(tailleChemin == newTailleChemin) {
        		tailleChemin = x*y; // cas d'arret si la condition de recouvrement est inaténiable
        	}else {
            	tailleChemin = newTailleChemin;
        	}
        } 
        return cave;
    }

    /**

    Réalise une recherche en profondeur pour élargir un chemin dans une matrice.
    Ajoute le node courant au chemin et marque le node comme visité.
    Puis, récursivement, pour chaque voisin non visité, non bloqué et non présent dans le chemin,
    ajoute le voisin au chemin, le marque comme visité et le visite récursivement.
    La récursion s'arrête si le nombre maximum de récursions est atteint ou si la taille du chemin atteint la taille maximale souhaitée.
    @param coords les coordonnées du node courant
    @param chemin la matrice représentant le chemin actuel
    @param matrice la matrice de la carte, utilisée pour vérifier si un voisin est bloqué ou non
    @param recursionMax le nombre maximal de récursions autorisées
    @param tauxDeRecouvrement le pourcentage de la carte à recouvrir
    @return le chemin élargi
    */
    private int[][] depthFirstSearch(int[] coords, int[][] chemin, int[][] matrice, int recursionMax,
            double tauxDeRecouvrement) {
        // ajout du node au chemin
        chemin[coords[0]][coords[1]] = 1;
        marked[coords[0]][coords[1]] = 1;

        // recursion
        int MaxSize = (int) Math.floor(x * y * tauxDeRecouvrement);
        
        
        
        for (int[] neighborCoord : this.neighbors(coords,true)) {
        		if (!(lenWay(chemin) >= MaxSize) && !(recursionMax <= 0)) {

                    if (marked[neighborCoord[0]][neighborCoord[1]] != 1
                            && this.countNeighborsNeighbors(neighborCoord[0], neighborCoord[1], chemin) < 6
                            && chemin[neighborCoord[0]][neighborCoord[1]] == 0) {
                    	
                    	//elargisement du chemin
                        int offSetX = coords[0] - neighborCoord[0];
                        int offSety = coords[1] - neighborCoord[1];
                        if (offSetX != 0) {
                            if (insideMatrix(coords[0], coords[1] + 1)) {
                                chemin[coords[0]][coords[1] + 1] = 1;
                                marked[coords[0]][coords[1] + 1] = 1;
                            }else if (insideMatrix(coords[0], coords[1] - 1)) {
                                chemin[coords[0]][coords[1] - 1] = 1;
                                marked[coords[0]][coords[1] - 1] = 1;
                            }
                        } else if (offSety != 0) {
                            if (insideMatrix(coords[0] + 1, coords[1])) {
                                chemin[coords[0] + 1][coords[1]] = 1;
                                marked[coords[0] + 1][coords[1]] = 1;
                            }else  if (insideMatrix(coords[0] - 1, coords[1])) {
                                chemin[coords[0] - 1][coords[1]] = 1;
                                marked[coords[0] - 1][coords[1]] = 1;
                            }
                        }
                        // récursion
                        int[] newCoord = { neighborCoord[0], neighborCoord[1] };
                        chemin = depthFirstSearch(newCoord, chemin, matrice, recursionMax - 1,
                                tauxDeRecouvrement);

                    }
                }
        	
            
        }        

        return chemin;
    }
    private int countNeighborsNeighbors(int x ,int y , int[][] chemin) {
    	int[] Coords = {x,y};
    	int somme = 0;
    	for(int[] neighbor : this.neighbors(Coords,false)) {
    		somme += this.countNeighbors(neighbor[0],neighbor[1], chemin);
    	}
    	return somme;
    }
    private int lenWay(int[][] chemin) {
        int sizeChemin = 0;
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                if (chemin[i][j] == 1) {
                    sizeChemin++;
                }
            }
        }
        return sizeChemin;
    }

    /**

    Retourne une liste de coordonnées des voisins d'une case donnée dans la matrice.
    @param coords Les coordonnées de la case dont on cherche les voisins.
    @param shuffle Indique si l'ordre des voisins doit être aléatoire ou non.
    @return Une liste d'entiers contenant les coordonnées des voisins de la case donnée.
    */
    private ArrayList<int[]> neighbors(int[] coords,boolean shuffle ) {
        ArrayList<int[]> neighbors = new ArrayList<int[]>();
        if (insideMatrix(coords[0] + 1, coords[1])) {
            int[] newNeighbor = { coords[0] + 1, coords[1] };
            neighbors.add(newNeighbor);
        }
        if (insideMatrix(coords[0] - 1, coords[1])) {
            int[] newNeighbor = { coords[0] - 1, coords[1] };
            neighbors.add(newNeighbor);
        }
        if (insideMatrix(coords[0], coords[1] + 1)) {
            int[] newNeighbor = { coords[0], coords[1] + 1 };
            neighbors.add(newNeighbor);
        }
        if (insideMatrix(coords[0], coords[1] - 1)) {
            int[] newNeighbor = { coords[0], coords[1] - 1 };
            neighbors.add(newNeighbor);

        }
       if(shuffle) {
    	      Collections.shuffle(neighbors, this.rand);
       }
      return neighbors;
    }
    /**
    Compte le nombre de voisins directs de la case (i,j) qui appartiennent au chemin donné.
    @param i coordonnée x de la case
    @param j coordonnée y de la case
    @param chemin le chemin sur le quel on compte les voisins
    @return le nombre de voisins directs de la case (i,j) qui appartiennent au chemin donné
    */
    private int countNeighbors(int i, int j, int[][] chemin) {
        int nbNeighbors = 0;
        if (insideMatrix(i + 1, j)) {
            if (chemin[i + 1][j] == 1) {
                nbNeighbors++;
            }
        }
        if (insideMatrix(i - 1, j)) {
            if (chemin[i - 1][j] == 1) {
                nbNeighbors++;
            }
        }
        if (insideMatrix(i, j + 1)) {
            if (chemin[i][j + 1] == 1) {
                nbNeighbors++;
            }
        }
        if (insideMatrix(i, j - 1)) {
            if (chemin[i][j - 1] == 1) {
                nbNeighbors++;
            }
        }
        return nbNeighbors;
    }

    private void applyCave(int[][] matrice, int[][] cave) {
        // netoyage des case correspondant au chemin + selection du point du chemin non
        // élargi le plus loin
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                if (cave[i][j] >= 1) {
                    matrice[i][j] = 0;
                }

            }
        }
    }

    /**
    Cette méthode calcule le chemin optimal pour récupérer un certain nombre d'or dans une matrice donnée.
    Elle utilise l'algorithme A* pour trouver le chemin le plus court entre chaque point d'or.
    @param matrice la matrice d'origine contenant les différents éléments du jeu (trou, brise, or...)
    @param nbOr le nombre d'or à récupérer
    @return un tableau à deux dimensions représentant le chemin optimal pour récupérer l'or, avec des 1 indiquant les cases à parcourir et des 0 pour les autres
    */
    private int[][] getGoldPath(int[][] matrice, int nbOr) {
        int[][] GoldPath = new int[x][y];
        int[][] GoldCoords = new int[nbOr][2];

        // sort gold node
        int index = 0;
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                if (matrice[i][j] == 2 || matrice[i][j] == 9 || matrice[i][j] == 10 ) {
                    int[] Gold = { i, j };
                    GoldCoords[index] = Gold;
                    index++;
                }
            }
        }
        
        int[][] SortedGoldCoords = new int[nbOr][2];
        int[][] marked = new int[x][y];
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                marked[i][j] = 0;
            }
        }
        
        index = 0;
        int[] startPoint = { 0, 0 };
        double distancemin = x * x + y * y;
        int[] nearestGold = { 0, 0 };
        for (int i = 0; i < nbOr; i++) {
            for (int j = 0; j < nbOr; j++) {

                if (marked[GoldCoords[j][0]][GoldCoords[j][1]] != 1) {
                    double currentdistnace = ManhattanDistance(startPoint[0], startPoint[1], GoldCoords[j][0],
                            GoldCoords[j][1]);
                    if (currentdistnace <= distancemin) {
                        distancemin = currentdistnace;
                        nearestGold = GoldCoords[j];
                    }
                }
            }
            SortedGoldCoords[index] = nearestGold;
            index++;
            startPoint = nearestGold;
            marked[nearestGold[0]][nearestGold[1]] = 1;
            distancemin = x * x + y * y;
        }
        
        // path finding
        index = 0;
        int[] coordStart = { 0, 0 };
        for (int i = 0; i < nbOr; i++) {
            int[] coordEnd = SortedGoldCoords[i];
            GoldPath[coordEnd[0]][coordEnd[1]] = 1;
           
            // PathFinding algoritme avec A*
            int[][] newPath = AStarPathFinding(coordStart[0], coordStart[1], coordEnd[0], coordEnd[1],matrice);
            //
            coordStart = coordEnd;

            // add newPath to GoldPath
            for (int j = 0; j < x; j++) {
                for (int k = 0; k < y; k++) {
                    if (newPath[j][k] == 1) {
                        GoldPath[j][k] = 1;
                    }
                }
            }
        }

        return GoldPath;
    }
    
    /**
    * Cette méthode implémente l'algorithme A* pour trouver le chemin le plus court entre deux points dans une matrice.
    * les type de case du plateau sont pondérés afin d'éviter au maximum les trou/brise/odeur mais ils ne sont pas impossible a prendre
    * @param xStart la coordonnée x du point de départ
    * @param yStart la coordonnée y du point de départ
    * @param xEnd la coordonnée x du point d'arrivée
    * @param yEnd la coordonnée y du point d'arrivée
    * @param matrice la matrice contenant les informations sur les objet du jeu
    * @return le chemin le plus court sous forme d'une matrice de même taille que la matrice d'entrée, avec des 1 indiquant les cases appartenant au chemin
    */
    private int[][] AStarPathFinding( int xStart, int yStart, int xEnd, int yEnd,int[][] matrice) {
    	 //// set up pathfinding
        // parents matrice
        int[][][] parents = new int[x][y][2];
        for (int j = 0; j < x; j++) {
            for (int k = 0; k < y; k++) {
                parents[j][k][0] = -1;
                parents[j][k][1] = -1;
            }
        }
        parents[xStart][yStart][0] = -2;
        //
        // State matrice
        int[][] state = new int[x][y];
        for (int j = 0; j < x; j++) {
            for (int k = 0; k < y; k++) {
                state[j][k] = 0;
            }
        }
        state[xStart][yStart] = 2;
        //
    	int[][] Path = new int[x][y];
        // init
        boolean endIsFound = false;
    			// ajout des nouveau node a evaluer
    	        while (!endIsFound) {
    	            for (int i = 0; i < x; i++) {
    	                for (int j = 0; j < y; j++) {
    	                    if (state[i][j] == 2) {// case vu par l'algoritme
    	                        int[] StartCoord = { i, j };
    	                        ArrayList<int[]> voisins = this.neighbors(StartCoord,false);
    	                        for (int[] coordVoisin : voisins) {
    	                            if (state[coordVoisin[0]][coordVoisin[1]] == 0) {
    	                                state[coordVoisin[0]][coordVoisin[1]] = 1;
    	                                parents[coordVoisin[0]][coordVoisin[1]][0] = i;
    	                                parents[coordVoisin[0]][coordVoisin[1]][1] = j;
    	                            }
    	                        }
    	                    }
    	                }
    	            }
    	            // evaluation des node
    	            double minFinal = -1;
    	            double endFinal = -1;
    	            int[] newNode = { 0, 0 };
    	            for (int i = 0; i < x; i++) {
    	                for (int j = 0; j < y; j++) {
    	                    if (state[i][j] == 1) {// case a évaluer par l'agorithme
    	                    	//weight celon le type des case
    	                    	double weight = 1 ;
    	                    	switch(matrice[i][j]) {
    	                    		default:
    	                    			break;
    	                    		case 5: // poids d'une brise
    	                    			weight = 5;
    	                    			break;
    	                    		case 4:// d'un trou
    	                    			weight = 10;
    	                    			break;
    	                    		case 6:
    	                    			weight = 15;
    	                    			break;
    	                    		case 7:
    	                    			weight = 15;
    	                    	}
    	                    	//calcul des distance euclidienne
    	                        double startDistance = ManhattanDistance(xStart, yStart, i, j);
    	                        double endDistance = ManhattanDistance(i, j, xEnd, yEnd);
    	                        double Somme = (startDistance + (endDistance * 100)) * weight;
    	                        if (minFinal == -1) {
    	                            minFinal = Somme;
    	                            endFinal = endDistance;
    	                            newNode[0] = i;
    	                            newNode[1] = j;
    	                        } else if (Somme < minFinal) {
    	                            minFinal = Somme;
    	                            newNode[0] = i;
    	                            newNode[1] = j;
    	                            endFinal = endDistance;
    	                        } else if (Somme == minFinal) {
    	                            if (endFinal == -1) {
    	                                minFinal = Somme;
    	                                endFinal = endDistance;
    	                                newNode[0] = i;
    	                                newNode[1] = j;
    	                            } else if (endDistance < endFinal) {
    	                                minFinal = Somme;
    	                                endFinal = endDistance;
    	                                newNode[0] = i;
    	                                newNode[1] = j;
    	                            }
    	                        }
    	                    }
    	                }
    	            }
    	            state[newNode[0]][newNode[1]] = 2;

    	            if (newNode[0] == xEnd && newNode[1] == yEnd) {
    	                endIsFound = true;
    	                int[] currentPoint = { newNode[0], newNode[1] };
    	                boolean startFound = false;
    	                while (!startFound) {

    	                    Path[currentPoint[0]][currentPoint[1]] = 1;

    	                    if (parents[currentPoint[0]][currentPoint[1]][0] != -2
    	                            && parents[currentPoint[0]][currentPoint[1]][1] != -2) {
    	                        currentPoint = parents[currentPoint[0]][currentPoint[1]];
    	                    } else {
    	                        startFound = true;
    	                    }
    	                }

    	            }

    	        }
    	 return Path;

    }
    /**

    Calcule la distance de Manhattan entre deux points en deux dimensions.
    La distance de Manhattan est la somme des différences absolues des coordonnées x et y.
    @param x1 La coordonnée x du premier point.
    @param y1 La coordonnée y du premier point.
    @param x2 La coordonnée x du deuxième point.
    @param y2 La coordonnée y du deuxième point.
    @return La distance de Manhattan entre les deux points.
    */  
    private double ManhattanDistance(int x1, int y1, int x2, int y2) {
        return Math.abs(x1 - x2) + Math.abs(y1 - y2) ;
    }
    /**

    Vérifie le chemin pour détecter les brisures et les angles.
    Si une brisure est détectée, elle est supprimée.
    Si un angle est détecté, une vérification est effectuée pour déterminer si le chemin peut être contourné.
    Si le chemin peut être contourné, les cases sont mises à jour en conséquence.
    Sinon la matrice est mise à jour pour supprimer les case génante.
    @param Path la matrice de chemin à vérifier
    @param matrice la matrice du jeu
    @return la matrice de chemin mise à jour
    */

    private int[][] verificationPath(int[][] Path, int[][] matrice) {
        /*
         * cas problématique:
         * brise sur le chemin
         * si en ligne essayé de décaler avant de supprimer la brise
         * si angle verifier si la diagonal complémentaire est vide sinon supprimé la
         * brise
         */
        int done = 0;
        while (done != 2) {
        	
            int nbaction = 0;
            for (int i = 0; i < x; i++) {
                for (int j = 0; j < y; j++) {
                	if(Path[i][j] == 1 && matrice[i][j] == 4) {
	                		this.removePit(i, j, matrice);
	                		nbaction++;
                	}
                    if (Path[i][j] == 1 && (matrice[i][j] == 5 || matrice[i][j] == 9 || matrice[i][j] ==7)) {
                    	
                    	boolean resolu = false;
                    	
                        // determine si la case siblé est un angle ou dans une ligne
                        int cas = -1; // varaible du acs actuelle , 0 -> ligne sur x , 1 -> ligne sur y , 2 -> angle
                                      // x+,y+ , 3-> angle x-,y-
                        // cas ligne
                        if (insideMatrix(i + 1, j)) {
                            if (insideMatrix(i - 1, j)) {
                                if (Path[i - 1][j] == 1 && Path[i + 1][j] == 1) {
                                    cas = 0;
                                }
                            }
                        }
                        if (insideMatrix(i, j + 1)) {
                            if (insideMatrix(i, j - 1)) {
                                if (Path[i][j - 1] == 1 && Path[i][j + 1] == 1) {
                                    cas = 1;
                                }
                            }
                        }
                        // cas angle
                        if (insideMatrix(i, j + 1)) {
                            if (insideMatrix(i + 1, j)) {
                                if (Path[i][j + 1] == 1 && Path[i + 1][j] == 1) {
                                    cas = 2;
                                }
                            }
                        }
                        if (insideMatrix(i - 1, j)) {
                            if (insideMatrix(i, j - 1)) {
                                if (Path[i][j - 1] == 1 && Path[i - 1][j] == 1) {
                                    cas = 3;
                                }
                            }
                        }
                        if (cas != -1) {
                            nbaction++;
                        }
                        
                        // resolution
                        if (cas < 2 && cas > 0) {// cas ligne
                            // verification si on peut contourner
                            if (cas == 0) { // cas ligne sur axe x
                                if (insideMatrix(i, j + 1) && insideMatrix(i - 1, j + 1)
                                        && insideMatrix(i + 1, j + 1)) {
                                    if (matrice[i][j + 1] == 0 && matrice[i + 1][j + 1] == 0
                                            && matrice[i - 1][j + 1] == 0) { // resolution par déplacement du chemin
                                    	Path[i][j] = 0;
                                    	Path[i][j + 1] = 1;
                                    	Path[i + 1][j + 1] = 1;
                                    	Path[i - 1][j + 1] = 1;
                                        resolu = true;
                                    }

                                } else if (insideMatrix(i, j - 1) && insideMatrix(i - 1, j - 1)
                                        && insideMatrix(i + 1, j - 1)) {
                                    if (matrice[i][j - 1] == 0 && matrice[i + 1][j - 1] == 0
                                            && matrice[i - 1][j - 1] == 0) { // resolution par déplacement du chemin
                                    	Path[i][j] = 0;
                                        Path[i][j - 1] = 1;
                                        Path[i + 1][j - 1] = 1;
                                        Path[i - 1][j - 1] = 1;
                                        resolu = true;
                                    }
                                }

                            }
                            if (cas == 1) { // cas ligne sur l'ax y
                                if (insideMatrix(i + 1, j) && insideMatrix(i + 1, j - 1)
                                        && insideMatrix(i + 1, j + 1)) {
                                    if (matrice[i + 1][j] == 0 && matrice[i + 1][j - 1] == 0
                                            && matrice[i + 1][j + 1] == 0) { // resolution par déplacement du chemin
                                    	Path[i][j] = 0;
                                    	Path[i + 1][j] = 1;
                                    	Path[i + 1][j - 1] = 1;
                                    	Path[i + 1][j + 1] = 1;
                                        resolu = true;
                                    }
                                } else if (insideMatrix(i, j - 1) && insideMatrix(i - 1, j - 1)
                                        && insideMatrix(i + 1, j - 1)) {
                                    if (matrice[i - 1][j] == 0 && matrice[i - 1][j - 1] == 0
                                            && matrice[i - 1][j + 1] == 0) { // resolution par déplacement du chemin
                                    	Path[i][j] = 0;
                                    	Path[i - 1][j] = 1;
                                    	Path[i - 1][j - 1] = 1;
                                    	Path[i - 1][j + 1] = 1;
                                        resolu = true;
                                    }
                                }
                            }

                        } else {// cas angle
                            if (cas == 2) {// angle direction x+,y+
                                if (insideMatrix(i + 1, j + 1)) {// resolution par déplacement du chemin
                                    if (matrice[i + 1][j + 1] == 0) {
                                    	Path[i][j] = 0;
                                    	Path[i + 1][j + 1] = 1;
                                        resolu = true;
                                    }
                                }
                            }
                            if (cas == 3) {// angle direction x-,y-
                                if (insideMatrix(i - 1, j - 1)) {// resolution par déplacement du chemin
                                    if (matrice[i - 1][j - 1] == 0) {
                                    	Path[i][j] = 0;
                                    	Path[i - 1][j - 1] = 1;
                                        resolu = true;
                                    }
                                }
                            }
                        }
                        
                        
                        
                        if (!resolu) { // resolution par suppresion
                            int[] currentCoord = { i, j };
                        	ArrayList<int[]> voisins = this.neighbors(currentCoord,false);
                            if(matrice[currentCoord[0]][currentCoord[1]] == 6 || matrice[currentCoord[0]][currentCoord[1]] == 7) {
                            	for (int[] coordVoisin : voisins) {
                                    switch(matrice[coordVoisin[0]][coordVoisin[1]]) {
                                    	default:
                                    		break;
                                    	case 4:
                                    		this.removePit(coordVoisin[0], coordVoisin[1], matrice);
                                    		break;
                                    	case 5:
	                                    	for (int[] coordVoisin2 : this.neighbors(new int[] {coordVoisin[0],coordVoisin[1]},false)) {
	                                                this.removePit(coordVoisin2[0], coordVoisin2[1], matrice);	                                            
	                                        }
                                    }
                                }
                            }else {
                                for (int[] coordVoisin : voisins) {
                                        this.removePit(coordVoisin[0], coordVoisin[1], matrice);                                    
                                }
                            }
                            
                        }
                    }
                }
            }
            if(done == 1) {
            	done = 2;
            }
            if (nbaction == 0 && done != 2) {
                done = 1;
            }
            
           this.placementBrise(matrice);
        }
        return Path;
    }

    /**
    Cette méthode permet de supprimer un trou situé à l'indice (i, j) d'une matrice donnée, ainsi que toutes les brisures adjacentes.
    Si la case (i, j) ne contient pas un trou (valeur 4), cette méthode ne fait rien.
    @param i l'indice de ligne de la case contenant le trou à supprimer
    @param j l'indice de colonne de la case contenant le trou à supprimer
    @param matrice la matrice contenant la case à modifier
    */
    private void removePit(int i, int j, int[][] matrice) {
        if (matrice[i][j] == 4) {
            matrice[i][j] = 0;
            for(int[] voisin : this.neighbors(new int[] {i,j}, false)) {
            	removeBrise(voisin[0],voisin[1], matrice);
            }            
        }
    }
    /**

    Cette méthode enlève une brise de la matrice donnée à la position spécifiée par les indices i et j.
    Si la case contient une brise de type 5 ou 9 et qu'il n'y a pas de trou à proximité, la brise est enlevée.
    Si la brise enlevée était de type 5, la case sera mise à zéro. Si la brise enlevée était de type 9, la case sera mise à deux.
    @param i l'indice de ligne de la position de la brise dans la matrice
    @param j l'indice de colonne de la position de la brise dans la matrice
    @param matrice la matrice qui contient la brise à enlever
    */
    private void removeBrise(int i, int j, int[][] matrice) {
        if (matrice[i][j] == 5 || matrice[i][j] == 9) {
            boolean pitNearBy = false;
            for(int[] voisin : this.neighbors(new int[] {i,j}, false)) {
            	if(matrice[voisin[0]][voisin[1]] == 4) {
            		pitNearBy = true;
            	}
            }            
            if (!pitNearBy) {
                if(matrice[i][j] == 5) {
                	matrice[i][j] = 0;
                }else {
                	matrice[i][j] =2;
                }
            }
        }
    }
    /**
    Place les pépites d'or sur la grille de jeu.
    @param matrice la grille de jeu sur laquelle placer les pépites d'or
    @param chemin la grille représentant le chemin le plus court pour atteindre la case de coordonnées (0,0)
    @param nbOr le nombre de pépites d'or à placer
    */
    private void placementGold(int[][] matrice, int[][] chemin, int nbOr) {

        int nbOrPlace = 0;
        int tentative = 400;
        while (nbOrPlace < nbOr && tentative > 0) {
        	tentative--;
            int i = rand.nextInt(x);
            int j = rand.nextInt(y);
            if (chemin[i][j] == 1 && this.ManhattanDistance(0, 0, i, j) >= (x+y)/(4*nbOr)) {
            	boolean valid = true;
                for(int h =0;h<x;h++) {
                	for(int k = 0;k<y;k++) {
                		if(matrice[h][k] == 2 && this.ManhattanDistance(h, k, i, j) < (x+y)/(2*nbOr)){valid = false;}
                		}
                }
                if(valid) {
                	matrice[i][j] =2;
                    nbOrPlace++;
                }
            }
        }
        if(tentative  == 0 ) {
        	while (nbOrPlace < nbOr) {
                int i = rand.nextInt(x);
                int j = rand.nextInt(y);
                if (chemin[i][j] == 1 && this.ManhattanDistance(0, 0, i, j) >= (x+y)/(6*nbOr)) {
                    	matrice[i][j] =2;
                        nbOrPlace++;
                }
            }
        }
    }

    /**

    Place les Wumpus dans la matrice de jeu en évitant les emplacements inaccessibles, les emplacements proches d'un autre Wumpus.
    En visant les emplacements sur le chemin menant aux ors.
    @param matrice la matrice du jeu
    @param Path le chemin menant aux ors
    */
    private void placementWumpus(int[][] matrice, int[][] Path) { // ajout cas d'arret apres x tentative et calcul de distance entre les wumpus
    		ArrayList<int[]> WumpusPos = new ArrayList<int[]>();
    		ArrayList<int[]> GoldPath = new ArrayList<int[]>();
    		for(int i = 0;i<x;i++) {
    			for(int j = 0 ; j<y;j++) {
    				if(Path[i][j] == 1 && this.ManhattanDistance(0, 0, i,j) >= 2 && matrice[i][j] != 2) {
    					GoldPath.add(new int[] {i,j});
    				}
    			}
    		}
    		ArrayList<int[]> temp = new ArrayList<int[]>();
    		for(int[] c : GoldPath) {
    			for(int[] n : this.neighbors(c, false)) {
    				if(this.ManhattanDistance(0, 0, n[0],n[1]) >= 2 && matrice[n[0]][n[1]] != 2) {
    					temp.add(n);    					
    				}
    			}
    		}
    		for(int [] n : temp) {
    			GoldPath.add(n);
    		}
    		
    		Collections.shuffle(GoldPath, rand);
    		for(int[] cell : GoldPath) {
    			if(WumpusPos.size() <= this.nbWumpus ) {
    				boolean testTerritoire = true;
    	            if (Path[cell[0]][cell[1]] == 1 && this.ManhattanDistance(0, 0, cell[0], cell[1]) >= 2 && matrice[cell[0]][cell[1]] != 2) {        
    	            			
    	            			for(int[] Wumpus : WumpusPos) {
    	            				int deltaX = Math.abs(cell[0]-Wumpus[0]);
    	            				int deltaY = Math.abs(cell[1]-Wumpus[1]);
    	            				if(deltaX < 4 && deltaY < 4) {
    	            					testTerritoire = false;
    	            				}
    	            			}
    	            			if(testTerritoire) {
    	            				matrice[cell[0]][cell[1]] =3;    
        	                    	this.placementOdeur(matrice);
        	                    	int nbOdeurAccesible = 0;
        	                    	for(int[] voisin : this.neighbors(new int[] {cell[0],cell[1]},false)) {                    		
        	                    		if(WumpusVerif(0,0,voisin[0],voisin[1],matrice)) {    	                    			
        	                    			nbOdeurAccesible++;
        	                    		}    	                    		
        	                    	}
        	                    	if(nbOdeurAccesible >= 2) {
        	                    		WumpusPos.add(new int[] {cell[0],cell[1]});
        	                    		deleteWumpus(cell[0],cell[1],matrice);
        	                    	}else {
        	                    		deleteWumpus(cell[0],cell[1],matrice);
        	                    	}
    	            			}    	                    	
    	                }
    	                	
    	            
    			}
    			}  		
    		
			
    			
	
    		
    		for(int[] Wumpus : WumpusPos) {
    			matrice[Wumpus[0]][Wumpus[1]] = 3;
    		}
    		
    		
    }
    /**
    Cette méthode permet de supprimer un wumpus de la grille en remplaçant les cases adjacentes à ce wumpus selon les règles suivantes :
    Si la case adjacente contient or + odeur, elle est remplacée par un or.
    Si la case adjacente contient odeur, elle est remplacée par une case vide.
    Si la case adjacente contient brise + odeur, elle est remplacée par une brise.
    Sinon elle ne change rien à la case vosine
    Enfin, la case contenant le wumpus est également remplacée par une case vide.
    @param x Coordonnée x du wumpus à supprimer
    @param y Coordonnée y du wumpus à supprimer
    @param matrice La grille de jeu représentée sous forme de matrice d'entiers
    */
    private void deleteWumpus(int x ,int y,int[][] matrice) {
    	for(int[] voisin : this.neighbors(new int[] {x,y} ,false  )) {
    		switch(matrice[voisin[0]][voisin[1]]) {
    			default:
    				break;
    			case 6:
    				matrice[voisin[0]][voisin[1]] = 0;
    				break;
    			case 7:
    				matrice[voisin[0]][voisin[1]] = 5;
    				break;
    			case 10:
    				matrice[voisin[0]][voisin[1]] = 2;
    				break;
    		}
    	}
    	matrice[x][y] = 0;
    }
    	
    /**
     * Vérifie si un chemin existe entre deux positions spécifiées dans une matrice donnée.
     * Utilise un algorithme de recherche de chemin basé sur une distance de Manhattan pondérée.
     * Permet de verifier si les odeurs d'un wumpus sont atteignable depuis le debut de la carte.
     * @param xStart la position X de départ
     * @param yStart la position Y de départ
     * @param xEnd la position X de fin
     * @param yEnd la position Y de fin
     * @param matrice la matrice dans laquelle la recherche de chemin est effectuée
     * @return true si un chemin existe, false sinon
     */
    private boolean WumpusVerif( int xStart, int yStart, int xEnd, int yEnd,int[][] matrice) {

    
    	 //// set up pathfinding
        // parents matrice
        int[][][] parents = new int[x][y][2];
        for (int j = 0; j < x; j++) {
            for (int k = 0; k < y; k++) {
                parents[j][k][0] = -1;
                parents[j][k][1] = -1;
            }
        }
        parents[xStart][yStart][0] = -2;
        //
        // State matrice
        int[][] state = new int[x][y];
        for (int j = 0; j < x; j++) {
            for (int k = 0; k < y; k++) {
                state[j][k] = 0;
            }
        }
        state[xStart][yStart] = 2;
    	int nbCellNonEval = 0;
    	int nbCellEval = 0;
    	int[][] Path = new int[x][y];
        // init
        boolean endIsFound = false;
    			// ajout des nouveau node a evaluer
    	        while (!endIsFound) {
    	            for (int i = 0; i < x; i++) {
    	                for (int j = 0; j < y; j++) {
    	                    if (state[i][j] == 2 && matrice[i][j] != 10 && matrice[i][j] != 6 && matrice[i][j] != 7) {// case vu par l'algoritme
    	                    	//matrice[i][j] = 11;
    	                        int[] StartCoord = { i, j };
    	                        ArrayList<int[]> voisins = this.neighbors(StartCoord,false);
    	                        for (int[] coordVoisin : voisins) {
    	                            if (state[coordVoisin[0]][coordVoisin[1]] == 0 
    	                            		&& matrice[coordVoisin[0]][coordVoisin[1]] != 3 
    	                            		&& matrice[coordVoisin[0]][coordVoisin[1]] != 4 
    	                            		&& matrice[coordVoisin[0]][coordVoisin[1]] != 5 
    	                            		&& matrice[coordVoisin[0]][coordVoisin[1]] != 9) {
    	                                state[coordVoisin[0]][coordVoisin[1]] = 1;
    	                                parents[coordVoisin[0]][coordVoisin[1]][0] = i;
    	                                parents[coordVoisin[0]][coordVoisin[1]][1] = j;
    	                            }
    	                        }
    	                    }
    	                }
    	            }
    	            // evaluation des node
    	            double minFinal = -1;
    	            double endFinal = -1;
    	            int[] newNode = { 0, 0 };
    	            for (int i = 0; i < x; i++) {
    	                for (int j = 0; j < y; j++) {
    	                    if (state[i][j] == 1) {// case a évaluer par l'agorithme
    	                    	//weight celon le type des case
    	                    	
    	                    	//calcul des distance euclidienne
    	                        double startDistance = ManhattanDistance(xStart, yStart, i, j);
    	                        double endDistance = ManhattanDistance(i, j, xEnd, yEnd);
    	                        double Somme = (startDistance + (endDistance * 100)) ;
    	                        if (minFinal == -1) {
    	                            minFinal = Somme;
    	                            endFinal = endDistance;
    	                            newNode[0] = i;
    	                            newNode[1] = j;
    	                        } else if (Somme < minFinal) {
    	                            minFinal = Somme;
    	                            newNode[0] = i;
    	                            newNode[1] = j;
    	                            endFinal = endDistance;
    	                        } else if (Somme == minFinal) {
    	                            if (endFinal == -1) {
    	                                minFinal = Somme;
    	                                endFinal = endDistance;
    	                                newNode[0] = i;
    	                                newNode[1] = j;
    	                            } else if (endDistance < endFinal) {
    	                                minFinal = Somme;
    	                                endFinal = endDistance;
    	                                newNode[0] = i;
    	                                newNode[1] = j;
    	                            }
    	                        }
    	                    }
    	                }
    	            }
    	            state[newNode[0]][newNode[1]] = 2;
    	            
    	            if (newNode[0] == xEnd && newNode[1] == yEnd) {
    	                endIsFound = true;
    	                int[] currentPoint = { newNode[0], newNode[1] };
    	                boolean startFound = false;
    	                while (!startFound) {

    	                    Path[currentPoint[0]][currentPoint[1]] = 1;

    	                    if (parents[currentPoint[0]][currentPoint[1]][0] != -2
    	                            && parents[currentPoint[0]][currentPoint[1]][1] != -2) {
    	                        currentPoint = parents[currentPoint[0]][currentPoint[1]];
    	                    } else {
    	                        startFound = true;
    	                    }
    	                }

    	            }
    	            
    	            // test si le path finding est bloqué
    	            int nbNewCellNonEval = 0;
    	            int nbNewCellEval = 0;
    	            for (int i = 0; i < x; i++) {
    	                for (int j = 0; j < y; j++) {
    	                	if(state[i][j] == 1) {
    	                		nbNewCellNonEval++;
    	                	}else if(state[i][j] == 2) {
    	                		nbNewCellEval++;
    	                	}
    	                }
    	            }
    	            if(nbCellNonEval <= nbNewCellNonEval && nbCellEval == nbNewCellEval) {
    	            	return false;
    	            }
    	            nbCellNonEval = nbNewCellNonEval;
    	            nbCellEval = nbNewCellEval;
    	        }	
    	return true;    
    }
    /**
     * Place des brises sur les cases voisines des cases contenant des trous (valeur 4).
     *
     * @param matrice la matrice à mettre à jour
     */
    private void placementBrise( int[][] matrice) {
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                if (matrice[i][j] == 4) {
                    for(int[] neighbor : this.neighbors( new int[]  {i,j} ,false)) {
                    	switch(matrice[neighbor[0]][neighbor[1]]) {
                    		case 2: // si la case voisine est un or
                    			matrice[neighbor[0]][neighbor[1]] = 9;
                    			break;
                    		case 6: // si la case voisine contient une Odeur
                    			matrice[neighbor[0]][neighbor[1]] = 7;
                    			break;
                    		case 0: // si la case voisine est vide
                    			matrice[neighbor[0]][neighbor[1]] = 5;
                    			break;
                    		default:
                    			break;
                    	}             
                    }
                }
            }
        }
    }
     /**
     * Place des Odeur sur les cases voisines des cases contenant des Wumpus (valeur 3).
     *
     * @param matrice la matrice à mettre à jour
     */
    private void placementOdeur(int[][] matrice) {
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {               
                if (matrice[i][j] == 3) {
                	for(int[] neighbor : this.neighbors( new int[]  {i,j} ,false)) {
                    	switch(matrice[neighbor[0]][neighbor[1]]) {
                    		case 2: // si la case voisine est un or
                    			matrice[neighbor[0]][neighbor[1]] = 10;
                    			break;
                    		case 5: // si la case voisine contient une Brise
                    			matrice[neighbor[0]][neighbor[1]] = 7;
                    			break;
                    		case 0: // si la case voisine est vide
                    			matrice[neighbor[0]][neighbor[1]] = 6;
                    			break;
                    		default:
                    			break;
                    	} 
                    }
                }
            }
        }
    }
    /**
     * Renvoie true si les coordonnées donner sont dans la matrice
     * @param i coordonnées sur x
     * @param j coordonnées sur y
     * @return true si (i,j) est dans la matrice , false sinon
     */
    public boolean insideMatrix(int i, int j) {
        return !(i > this.x - 1 || i < 0 || j > this.y - 1 || j < 0);
    }
    /**
     * Met à jour le nombre de Wumpus sur le plateau en comptant le nombre de cases contenant des Wumpus.
     * @param matrice La matrice représentant le plateau.
     * @return Le nombre de Wumpus sur le plateau.
     */
    private int updateNbWumpus(int[][] matrice) {
    	int updatedNbWumpus = 0;
        for(int i =0 ; i<x ; i++) {
        	for(int j = 0;j<y;j++) {
        		if(matrice[i][j] == 3) {
        			updatedNbWumpus++;
        		}
        	}
        }
        return updatedNbWumpus;
    }
    /**
    * @return return la matrice représentant le plateau de jeu.
    */
    public int[][] getMatrice() {
        return this.matrice;
    }
    
    /**
     * @param CoordonneeX La coordonnée x de la case à modifier
	 * @param CoordonneeY La coordonnée y de la case à modifier
	 * @param Valeur La nouvelle valeur à assigner à la case
     *
	 * @throws IndexOutOfBoundsException si les coordonnées fournies ne se situent pas dans les limites du plateau.
     */

    public void setMatrice(int CoordonneeX, int CoordonneeY, int Valeur) {
        this.matrice[CoordonneeX][CoordonneeY] = Valeur;
    }
    
    /**
    Tente de tuer un Wumpus dans la case aux coordonnées (CoordonneeX, CoordonneeY) et retourne un booléen indiquant si
    l'opération a réussi. Si la case contient un Wumpus (représenté par la valeur 3), la méthode met à jour la matrice
    en remplaçant la valeur de la case par 8(représentant un Wumpus mort) et retourne true. Sinon, la méthode retourne false.
    @param CoordonneeX l'abscisse de la case à vérifier
    @param CoordonneeY l'ordonnée de la case à vérifier
    @return true si la case contient un Wumpus et a été mise à jour avec la valeur 8, false sinon
    */

    public boolean killWumpus(int CoordonneeX, int CoordonneeY) {
    	if(this.insideMatrix(CoordonneeX,CoordonneeY)) {
	        if (this.matrice[CoordonneeX][CoordonneeY] == 3) {
	            this.setMatrice(CoordonneeX, CoordonneeY, 8);
	            return true;
	        } else {
	            return false;
	        }
    	}else 
    		return false;
	   }
    /**
     * @return return le nombre d'or sur la matrice.
     */
    public int getNbOr() {
    	return this.nbOr;
    }
    
    /**
     * @return return le nombre de Wumpus sur la matrice.
     */
    public int getNbWumus() {
    	return this.nbWumpus;
    }
    /**
     * @return retourne la matrice correspondant aux cellules explorées
     */
    public int[][] getExplored(){
		return this.explored;
	}
    
    /**
     * @param CoordonneeX La coordonnée x de la case à modifier
	 * @param CoordonneeY La coordonnée y de la case à modifier
	 * @param Valeur La nouvelle valeur à assigner à la case    
	 * 
	 * @throws IndexOutOfBoundsException si les coordonnées fournies ne se situent pas dans les limites du plateau.
     */
    public void setExplored(int CoordonneeX , int CoordonneeY , int Valeur){
		this.explored[CoordonneeX][CoordonneeY] = Valeur;
	}
    
    public int getX() {
    	return this.x;
    }

    public int getY() {
    	return this.y;
    }
    public int getSeed() {
    	return this.seed;
    }
}
