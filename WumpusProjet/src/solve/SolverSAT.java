package solve;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.reader.DimacsReader;
import org.sat4j.reader.ParseFormatException;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;
import org.sat4j.specs.TimeoutException;

import gen.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
/*
* Definition des symbolmes
* Vide(x,y) : (x,y) est vide
* Wumpus(x,y) : (x,y) contient le wumpus
* Pit(x,y) : (x,y) contient un trou
* Brise(x,y) : (x,y) contient une brise
* Odeur(x,y) : (x,y) contient une odeur
* Gold(x,y) : (x,y) contient un gold
* Eclat(x,y) ; (x,yà un eclat est perceptible
* Numérotation unique des symboles en chaque point de la matrice
* pour une grille de taille x , y:
* V -> de (x*y)*0 a (x*y)*1-1 | formule depuis i,j -> j + y*i + (y*x)*0
* W -> de (x*y)*1 a (x*y)*2-1 | formule depuis i,j -> j + y*i + (y*x)*1
* P -> de (x*y)*2 a (x*y)*3-1 | formule depuis i,j -> j + y*i + (y*x)*2
* B -> de (x*y)*3 a (x*y)*4-1 | formule depuis i,j -> j + y*i + (y*x)*3
* O -> de (x*y)*4 a (x*y)*5-1 | formule depuis i,j -> j + y*i + (y*x)*4
* G -> de (x*y)*5 a (x*y)*6-1 | formule depuis i,j -> j + y*i + (y*x)*5
* E -> de (x*y)*6 a (x*y)*7-1 | formule depuis i,j -> j + y*i + (y*x)*6
*/

public class SolverSAT {
    private Plateau plateau;
    private ISolver solver;
    private DimacsReader reader;
    private String fileName;
    private int x;
    private int y;
    ArrayList<int[]> WumpusFound;
    
public SolverSAT() {}   
public SolverSAT(Plateau plateau) {
        this.plateau = plateau;
        this.x = plateau.getMatrice().length;
        this.y = plateau.getMatrice()[0].length;  
        this.WumpusFound = new ArrayList<int[]>();
        
        
        this.fileName = "src/res/dimacs.dimacs";
        
        try {
    		this.generateDimacs();
    		this.updateNbClause();
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	} 
        
	    this.solver = SolverFactory.newDefault();
	    solver.setTimeout(10);            
	    this.reader = new DimacsReader(solver); 
	    try {
			reader.parseInstance(fileName);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ContradictionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }

public boolean isSolvable()  {    	
    	/*
    	 * Codage des entier dans la matrice SolvedCells
    	 * 0 case non evalué
    	 * 2 case vide deja evaluée
    	 * 3 Trou
    	 * 4 Wumpus
    	 */
		
		// matrice des cellules visitée
		int[][] solvedCells = new int[x][y];
	    int[][] explored = new int[x][y];
	    
		for(int i =0 ;i<x;i++) {
			for(int j = 0 ; j<y;j++) {
				solvedCells[i][j] = 0;
				explored[i][j] = 0;
			}
		}
		solvedCells[0][0] = 2;
		explored[0][0] = 1;
        //initialisation du dimacs
    	try {
			this.generateDimacs();
			this.updateNbClause();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    	 
    	
    	// initialisation du solver    		
    	
	    
	    ArrayList<int[]> cellsToSolve = new ArrayList<int[]>();
	    ArrayList<int[]> newSafeCells = new ArrayList<int[]>();
	    WumpusFound = new ArrayList<int[]>();
	    // initialisation
	    boolean Continue = true;
	    int[] CountOld = new int[5];
	    for(int i = 0 ; i<5;i++) {
	    	CountOld[i] = 0;
	    }
        int[] Count = new int[5];
        do{     
        	
        	// ajout des cellules vides
        	this.addSafeCells(solvedCells,explored);
        	//reset newSafeCells
        	newSafeCells= new ArrayList<int[]>();
        	
        	if(this.nbOrFound(solvedCells) == plateau.getNbOr()) {
        		return true;
        	}
         	
         	cellsToSolve = newcellsToSolve(solvedCells);
         	
         	ArrayList<int[]> newCells = new ArrayList<int[]>();
           // relecture du dimacs   	        
  	       if(cellsToSolve.size() == 0) {
  	    	   return false;
  	       }
  	    
         	for(int[] cell : cellsToSolve) {
         		
         		
         		
         		if(solvedCells[cell[0]][cell[1]] == 0) {
         	       	int result = this.solveCell(cell, solver);
         			solvedCells[cell[0]][cell[1]] = result;
         			newCells.add(cell);
         			switch(result) {
         				default:
         					break;
         				case 1:
         					newSafeCells.add(cell);  
         					break;
         				case 4:
         					WumpusFound.add(cell);
         					solvedCells[cell[0]][cell[1]] = 1;
         					break;
         			}
         			
         		}       
         		
         	}
         	this.addCellsToDimacs(newCells);         	
         
         for(int i = 0 ;i<x;i++) {
        	 for(int j = 0 ; j<y;j++) {
        		 Count[solvedCells[i][j]]++;
        	 }
         }
         Continue = false;
         for(int i = 0 ; i<5;i++) {
 	    	if(Count[i] != CountOld[i]) {
 	    		Continue = true;
 	    	}
 	    }
        }while(Continue);
       
         return false;
    }

public int nbWumpusRequired()  {    	
	if(this.isSolvable()) {		
		return this.WumpusFound.size();
	}else {
		return -1;
	}	
}
private int nbOrFound(int[][] solvedCells) {
	int nbOr = 0;
	for(int i =0 ;i<x;i++) {
		for(int j = 0 ; j<y;j++) {	
			if((solvedCells[i][j] == 1 || solvedCells[i][j] == 2)
					&& (
						this.plateau.getMatrice()[i][j] == 2 || 
						this.plateau.getMatrice()[i][j] == 9 || 
						this.plateau.getMatrice()[i][j] == 10 )) {
				nbOr++;
			}
			}
		}
	
	return nbOr;
}



private ArrayList<int[]> newcellsToSolve(int[][] solvedCells){
	ArrayList<int[]> newCells = new ArrayList<int[]>();
	for(int i =0 ;i<x;i++) {
		for(int j = 0 ; j<y;j++) {	
				if(solvedCells[i][j] == 1 && (this.plateau.getMatrice()[i][j] != 5 && this.plateau.getMatrice()[i][j] != 7)) {
					for(int [] coords : this.neighbors(new int[] {i,j})) {
						if(solvedCells[coords[0]][coords[1]] == 0) {
							boolean test = true;
							for( int[] c : newCells)
							{
								if(c[0] == coords[0] && coords[1] == c[1]) {
									test =false;
								}
							}
							if(test) {
								newCells.add(coords);
							}
						}
					}
				}
			}		
		}
	return newCells;
}

public void addCellsToDimacs(ArrayList<int[]> newCells) {
	String CellsClauses  = "";
	for(int[] newCell : newCells) {
		switch(this.plateau.getMatrice()[newCell[0]][newCell[1]]) {
		default:
			break;
		case 0:
			CellsClauses += ""+Vide(newCell[0],newCell[1]) +" 0";	
			try {
				solver.addClause(new VecInt(new int[] {Vide(newCell[0], newCell[1]) }));
			} catch (ContradictionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 2:
			CellsClauses += (""+Gold(newCell[0],newCell[1]) +" 0");
			try {
				solver.addClause(new VecInt(new int[] {Gold(newCell[0], newCell[1]) }));
			} catch (ContradictionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 3:
			CellsClauses += (""+Wumpus(newCell[0],newCell[1])+" 0");
			try {
				solver.addClause(new VecInt(new int[] {Wumpus(newCell[0], newCell[1]) }));
			} catch (ContradictionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 4:
			CellsClauses += (""+Pit(newCell[0],newCell[1])+" 0");
			try {
				solver.addClause(new VecInt(new int[] {Pit(newCell[0], newCell[1]) }));
			} catch (ContradictionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 5:
			CellsClauses += (""+Brise(newCell[0],newCell[1])+" 0");
			try {
				solver.addClause(new VecInt(new int[] {Brise(newCell[0], newCell[1]) }));
			} catch (ContradictionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 6:
			CellsClauses += (""+Odeur(newCell[0],newCell[1])+" 0");
			try {
				solver.addClause(new VecInt(new int[] {Odeur(newCell[0], newCell[1]) }));
			} catch (ContradictionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 7:
			CellsClauses += (""+Brise(newCell[0],newCell[1])+" 0");
			CellsClauses += System.getProperty("line.separator");
			CellsClauses += (""+Odeur(newCell[0],newCell[1])+" 0");
			
			try {
				solver.addClause(new VecInt(new int[] {Brise(newCell[0], newCell[1]) }));
				solver.addClause(new VecInt(new int[] {Odeur(newCell[0], newCell[1]) }));
			} catch (ContradictionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 9:
			CellsClauses += (""+Gold(newCell[0],newCell[1])+" 0");
			CellsClauses += System.getProperty("line.separator");
			CellsClauses += (""+Brise(newCell[0],newCell[1])+" 0");
			try {
				solver.addClause(new VecInt(new int[] {Gold(newCell[0], newCell[1]) }));
				solver.addClause(new VecInt(new int[] {Brise(newCell[0], newCell[1]) }));
			} catch (ContradictionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 10:
			CellsClauses += (""+Gold(newCell[0],newCell[1])+" 0");
			CellsClauses += System.getProperty("line.separator");
			CellsClauses += (""+Odeur(newCell[0],newCell[1])+" 0");
			try {
				solver.addClause(new VecInt(new int[] {Gold(newCell[0], newCell[1]) }));
				solver.addClause(new VecInt(new int[] {Odeur(newCell[0], newCell[1]) }));
			} catch (ContradictionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
	}
		CellsClauses += System.getProperty("line.separator");
	}
	
	try {
		this.addClauseToDimacs(CellsClauses);
		this.updateNbClause();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
private void addSafeCells(int[][] solvedCells,int[][] explored) {
	for(int i = 0;i<x;i++) {
		for(int j = 0 ; j<y;j++) {
			if(solvedCells[i][j] == 1 && this.plateau.getMatrice()[i][j] == 0) {
				solvedCells[i][j] = 2;
			}
		}
	}
	int nbaction = 1;
	ArrayList<int[]> newCells = new ArrayList<int[]>();
	while(nbaction > 0) {
		nbaction = 0;
		for(int i = 0;i<x;i++) {
			for(int j = 0 ; j<y;j++) {
				if(solvedCells[i][j] == 2 ) {
					for(int[] c : this.neighbors(new int[] {i,j})) {
						if(explored[c[0]][c[1]] == 0) {
							nbaction++;
							newCells.add(c);
							explored[c[0]][c[1]] = 1;
							if( this.plateau.getMatrice()[c[0]][c[1]]  == 0) {
								solvedCells[c[0]][c[1]] = 2;
							}else {
								solvedCells[c[0]][c[1]] = 1;
							}
						}
					}
				}
			}
		}
	}
	this.addCellsToDimacs(newCells);
}



public int solveCell(int[] newCell,ISolver solver) {
    	
    	// initialisation du solver
		
    	// test si un pit est deductible
    	IConstr lastClauseContr = null;
        try {        	
             lastClauseContr = solver.addClause(new VecInt(new int[] { -1 * Pit(newCell[0], newCell[1]) }));
        } catch (ContradictionException e) {
            // H^-C est insatisfiable donc c est conséquence de H 
            return 3;
            
        }        
        try {
             if (!solver.isSatisfiable()) {// calcule si la contradiction n'a pas été levée
                // H^-C est insatisfiable donc c est conséquence de H
            	solver.removeConstr(lastClauseContr);
                return 3;
                }
         } catch (TimeoutException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
         }
        
        
        // reset solver
        solver.removeConstr(lastClauseContr);
        
    	
        // test si un wumpus est deductible
    	lastClauseContr = null;
        try {        	
        	lastClauseContr = solver.addClause(new VecInt(new int[] { -1 * Wumpus(newCell[0], newCell[1]) }));
        } catch (ContradictionException e) {
            // H^-C est insatisfiable donc c est conséquence de H 
            return 4;
            
        }
       
            try {
                if (!solver.isSatisfiable()) {// calcule si la contradiction n'a pas été levée
                    // H^-C est insatisfiable donc c est conséquence de H
                	solver.removeConstr(lastClauseContr);
                    return 4;
                }
            } catch (TimeoutException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        solver.removeConstr(lastClauseContr);
        // case safe
        return 1;       
        }

   

private boolean insideMatrix(int i, int j) {
        return !(i > this.x - 1 || i < 0 || j > this.y - 1 || j < 0);
    }


private ArrayList<int[]> neighbors(int[] coords) {
        ArrayList<int[]> neighbors = new ArrayList<int[]>();
        if (insideMatrix(coords[0] + 1, coords[1])) {
            int[] newNeighbor = { coords[0] + 1, coords[1] };
            neighbors.add(newNeighbor);
        }
        if (insideMatrix(coords[0] - 1, coords[1])) {
            int[] newNeighbor = { coords[0] - 1, coords[1] };
            neighbors.add(newNeighbor);
        }
        if (insideMatrix(coords[0], coords[1] + 1)) {
            int[] newNeighbor = { coords[0], coords[1] + 1 };
            neighbors.add(newNeighbor);
        }
        if (insideMatrix(coords[0], coords[1] - 1)) {
            int[] newNeighbor = { coords[0], coords[1] - 1 };
            neighbors.add(newNeighbor);
        }
       
      return neighbors;
    }
private ArrayList<int[]> territoire(int[] coords) {
    	ArrayList<int[]> territoire= new ArrayList<int[]>();        
    	for(int i = -2 ; i<=2;i++) {
    		for(int j = -2 ; j<=2;j++) {
    			if(!(i == 0 && j == 0) && this.insideMatrix(coords[0]+i, coords[1]+j)) {
    				territoire.add(new int[] {coords[0]+i,coords[1]+j});
    			}
    		}
    	}
    	return territoire;
    }
  
private int Vide(int i, int j) {
        return 1 + j + y * i;
    }

private int Wumpus(int i, int j) {
        return 1+ j + y * i + y * x;
    }

private  int Pit(int i, int j) {
        return 1 + j + y * i + y * x * 2;
    }

private int Brise(int i, int j) {
        return 1 + j + y * i + y * x * 3;
    }

private int Odeur(int i, int j) {
        return 1 + j + y * i + y * x * 4;
    }

private int Gold(int i, int j) {
        return 1 + j + y * i + (y * x * 5);
    }
    
private int Eclat(int i, int j) {
        return 1 + j + y * i + (y * x * 6);
    }
    
private void generateDimacs() throws IOException {
    	
    	File dimacsFile = new File("src/res/dimacs.dimacs");
    	FileWriter fw = new FileWriter(dimacsFile);
        BufferedWriter bw = new BufferedWriter(fw);
        //commentaire sur les variable logique
        bw.write("c les varaible cases Vide vont de " + Vide(0,0) +" à " + Vide(x-1,y-1)+System.getProperty("line.separator"));
        bw.write("c les varaible Wumpus vont de " + Wumpus(0,0) +" à " + Wumpus(x-1,y-1)+System.getProperty("line.separator"));
        bw.write("c les varaible Pit vont de " + Pit(0,0) +" à " + Pit(x-1,y-1)+System.getProperty("line.separator"));
        bw.write("c les varaible Brises vide vont de " + Brise(0,0) +" à " + Brise(x-1,y-1)+System.getProperty("line.separator"));
        bw.write("c les varaible Odeur vide vont de " + Odeur(0,0) +" à " + Odeur(x-1,y-1)+System.getProperty("line.separator"));
        bw.write("c les varaible Gold vide vont de " + Gold(0,0) +" à " + Gold(x-1,y-1)+System.getProperty("line.separator"));
        bw.write("c les varaible Eclat vide vont de " + Eclat(0,0) +" à " + Eclat(x-1,y-1)+System.getProperty("line.separator"));
        //initialisation dimacs
        bw.write("p cnf " + ((7*x*y)) + " " +(x*y)+System.getProperty("line.separator"));
        bw.write("c ------------ regles du monde ------------"+System.getProperty("line.separator"));
        for(int i = 0;i < x ; i++) {
        	for(int j = 0 ; j < y; j++) {        		
        	
        		bw.write("c Regles du monde en (" + i +","+j+")"+System.getProperty("line.separator"));
        		
        		// il n'y a pas de superposition  vide, de wumpus et de trou
        		bw.write("c Superpossition de vide , pit et wumpus"+System.getProperty("line.separator"));
        		bw.write("-"+Vide(i,j) + " -"+Pit(i,j) + " 0"+System.getProperty("line.separator"));
        		bw.write("-"+Vide(i,j) + " -"+Wumpus(i,j) + " 0"+System.getProperty("line.separator")); 
        		// il n'y a pas de superposition  Brise , de wumpus et de trou
        		bw.write("c Superpossition de vide , pit et wumpus"+System.getProperty("line.separator"));
        		bw.write("-"+Brise(i,j) + " -"+Pit(i,j) + " 0"+System.getProperty("line.separator"));
        		bw.write("-"+Brise(i,j) + " -"+Wumpus(i,j) + " 0"+System.getProperty("line.separator")); 
        		// il n'y a pas de superposition  Odeur , de wumpus et de trou
        		bw.write("c Superpossition de vide , pit et wumpus"+System.getProperty("line.separator"));
        		bw.write("-"+Odeur(i,j) + " -"+Pit(i,j) + " 0"+System.getProperty("line.separator"));
        		bw.write("-"+Odeur(i,j) + " -"+Wumpus(i,j) + " 0"+System.getProperty("line.separator")); 
        		// il n'y a pas de superposition  d'or, de wumpus et de trou
        		bw.write("c Superpossition de gold,pit et wumpus"+System.getProperty("line.separator"));
        		bw.write("-"+Gold(i,j) + " -"+Pit(i,j) + " 0"+System.getProperty("line.separator"));
        		bw.write("-"+Gold(i,j) + " -"+Wumpus(i,j) + " 0"+System.getProperty("line.separator"));        		
        		bw.write("-"+Wumpus(i,j) + " -"+Pit(i,j) + " 0"+System.getProperty("line.separator"));    
        		
        		//Les wumpus ont un territoire (un carré de 5 cases de côté centré sur le wumpus) où il ne peut pas y a∨oir d'autre wumpus.
        		bw.write("c terrritoire wumpus"+System.getProperty("line.separator"));
        		for(int[] territoire : this.territoire(new int[]  {i,j})) {
        			bw.write("-"+Wumpus(i,j) + " -"+Wumpus(territoire[0],territoire[1])+" 0"+System.getProperty("line.separator"));
        		}
        		
        		//Si on perçoit une odeur, un wumpus est présent sur une des cases adjacentes.
        		bw.write("c Odeur wumpus"+System.getProperty("line.separator"));
        		String clause = "-"+Odeur(i,j);    
        		for(int[] voisin : this.neighbors(new int[] {i,j}) ) {
        			clause += " "+Wumpus(voisin[0],voisin[1]);
        		}
        		clause +=" 0"+System.getProperty("line.separator");
        		bw.write(clause);
        		
        		//Si on perçoit une brise, au moins un trou est présent sur les cases adjacentes.
        		bw.write("c Brise Pit"+System.getProperty("line.separator"));
        		clause = "-"+Brise(i,j);    
        		for(int[] voisin : this.neighbors(new int[] {i,j}) ) {
        			clause += " "+Pit(voisin[0],voisin[1]);
        		}
        		clause +=" 0"+System.getProperty("line.separator");
        		bw.write(clause);
        		
        	}
        }
        bw.write("c ------------ Initialisation des connaisance ------------"+System.getProperty("line.separator"));
        bw.write(Vide(0,0) + " 0"+System.getProperty("line.separator"));
        bw.write("c ------------ Deductions ------------"+System.getProperty("line.separator"));     
        
        
        bw.close();
        fw.close();

    }
private void updateNbClause() throws IOException {  
    	File dimacsFile = new File("src/res/dimacs.dimacs");
		int nbClauses = 0;
        Scanner scanner1 = new Scanner(dimacsFile);			
			while(scanner1.hasNextLine()) {
				String line = scanner1.nextLine();
				if(! (line.startsWith("c") || line.startsWith("p"))){
					nbClauses++;
				}
			}	
		scanner1.close();
		
		List<String> lines = new ArrayList<>();
        int ligneNumber = 1;
        Scanner scanner2 = new Scanner(dimacsFile);
        
        while(scanner2.hasNextLine()) {
        	String line = scanner2.nextLine();
        	if(ligneNumber == 8) {
        		line = "p cnf " + ((7*x*y)) + " " +nbClauses;
        	}
        	lines.add(line);
        	ligneNumber++;
        	
        }        

        FileWriter fw = new FileWriter(dimacsFile);
        BufferedWriter bw = new BufferedWriter(fw);
        for(String line : lines) {
        	bw.write(line);
        	bw.newLine();
        }
        scanner2.close();
        bw.close();
    }
private void addClauseToDimacs(String Clause) throws IOException{

    	File dimacsFile = new File("src/res/dimacs.dimacs");
    	List<String> lines = new ArrayList<>();
        Scanner scanner = new Scanner(dimacsFile);
        
        while(scanner.hasNextLine()) {
        	String line = scanner.nextLine();
        	lines.add(line);
        	
        }        

        FileWriter fw = new FileWriter(dimacsFile);
        BufferedWriter bw = new BufferedWriter(fw);
        for(String line : lines) {
        	bw.write(line);
        	bw.newLine();
        }
        bw.write(Clause);
        scanner.close();
        bw.close();
        
    }

public ISolver getSolver() {
	return this.solver;
}
    
}



 