package solve;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;

import gen.*;
import main.LecteurAudio;

//en explore il va essayer de parcourir toute les case sur non explore
// quand il tombe sur une cas non sur, il passe en backtrack immediatement (priorite sur tout)
// quand il na plus de case sur a explorer, il deduit le wumpus le plus proche et le tue
// repasse en explore.
// conditions d'arret:
//	- si tout les or ont ete trouve, l'agent a gagne
//  - si toute les case ont ete explore et plus de wumpus a deduire = tout explore (map infaisable)

public class Agent {
	// attributs
	Plateau plateau;
	int[] position;
	int[] old_position;
	State etats;
	State old_etats;
	int x, y;
	int[][] explored;
	// celules explore a ajouter au solver
	ArrayList<int[]> cellStack;
	int[] objectif;
	ArrayList<int[]> chemin;
	int Error;
	int nbMvt;	
	
	public Agent(Plateau plateau) {
		this.plateau=plateau;
		// taille des deux dimensions de la matrice
		this.x=plateau.getMatrice().length;
		this.y=plateau.getMatrice()[0].length;
		// init pos agent
		this.position=new int[] {0,0};
		this.old_position=new int[] {0,0};
		// init etat agent
		this.etats=State.EXPLORE;
		this.old_etats=State.EXPLORE;
		// init matrice explore
		this.explored=new int[x][y];
		
		for(int i=0; i<x; i++) {
			for(int j=0; j<y; j++) {
				explored[i][j]=0;
			}
		}
		
		// init premiere case
		explored[0][0]=1;
		// init stack des positions explore
		this.cellStack=new ArrayList<int[]>();
		// init de l'objectif
		this.objectif=new int[] {0,0};
		// init chemin
		this.chemin=new ArrayList<int[]>();
		
		
		
		this.Error=0;	
		this.nbMvt = 0;
	}
	
	// verif que la position correspond a l'objectif
	private boolean isObjAtteint() {
		return (this.position[0]==this.objectif[0]&&this.position[1]==this.objectif[1]);
	}
	
	// deplacer l'agent
	private void moveAgent(int[] destination) {
		this.nbMvt++;
		
		if(this.ManhattanDistance(destination, position)>1) {
			System.out.println("distance depacement illegale ");
			this.Error=1;
		}
		if(this.plateau.getMatrice()[destination[0]][destination[1]] == 4) {
			System.out.println("Agent sur Pit");
			this.Error=2;
		}
		if(explored[destination[0]][destination[1]]==0) {
			explored[destination[0]][destination[1]]=1;
		}
		
		this.old_position[0]=this.position[0];
		this.old_position[1]=this.position[1];
		
		this.position[0]=destination[0];
		this.position[1]=destination[1];
		
		
		boolean success = this.plateau.killWumpus(destination[0], destination[1]);
		
		if(success) {
			LecteurAudio.Rugissement();
		}
		if(!isSafe(position)) {
			this.changeState(State.BACKTRACK);
		}
	}
	
	// prochain coup, si l'obj est atteint alors on determine le prochain obj de l'agent et bouge l'agent a l'aide de MoveAgent
	// METHODE PRINCIPALE
	public boolean nextMove() {
		if(this.Error != 0) {
			System.out.println("Error");
		}
		
		else {
			/*System.out.println("______________________________");
			System.out.println("current obj "+objectif[0]+","+objectif[1]);
			System.out.println("current position "+position[0]+","+position[1]);
			System.out.println("Current state "+this.etats);
			System.out.println("Stack Size "+cellStack.size());*/
			
			if((this.plateau.getMatrice()[position[0]][position[1]]==3) && this.etats==State.KILLWUMPUS) {
				
				this.changeState(State.EXPLORE);
			}
			if(isObjAtteint()) {
				nextObjectif();
			}
			
			if(chemin.size()==0) {
				chemin = this.AStarPathFinding(position, objectif);
				//chemin = this.TriDistanceAgent(chemin);
			}
			
			
			int moved=-1;
			for(int i=0;i<chemin.size();i++) {
				if(moved==-1 && this.ManhattanDistance(chemin.get(i), position)==1) {
					this.moveAgent(chemin.get(i));
					moved=i;
				}
			}			
			if(moved!=-1) {
				chemin.remove(moved);
			}
			
			
			
			int nbOrTrouve=0;
			for(int i=0; i<x; i++) {
				for(int j=0; j<y; j++) {
					if(explored[i][j]==1 && (this.plateau.getMatrice()[i][j]==2 ||
											 this.plateau.getMatrice()[i][j]==9 ||
											 this.plateau.getMatrice()[i][j]==10)) {
						nbOrTrouve++;
					}
				}
			}
			if(nbOrTrouve==this.plateau.getNbOr()) {
				return true;
			}
			
		}
		
		return false;
	}
	
	// determiner le prochain objectif de l'agent
	private void nextObjectif() {
		int[] Obj=new int[] {position[0],position[1]};
		
		switch(this.etats) {
		default:
			this.objectif[0]=0;
			this.objectif[1]=1;
			break;
		case EXPLORE:
			ArrayList<int[]> newCell = newCellExplore();
			if(newCell.size()==0) {
				this.changeState(State.KILLWUMPUS);
			}
			else {
				Obj[0]=newCell.get(0)[0];
				Obj[1]=newCell.get(0)[1];
			}
			break;
		case BACKTRACK:
			Obj[0]=old_position[0];
			Obj[1]=old_position[1];
			this.changeState(old_etats);
			break;
		case KILLWUMPUS:
			ArrayList<int[]> wumpusPotentiel = WumpusADetermine();
			ArrayList<int[]> wumpusTrouve = new ArrayList<int[]>();
			
			if(wumpusPotentiel.size()==0) {
				this.Error=3;
				System.out.println("Aucun wumpus potentiel trouve");
			}			
			for(int[] c1 : wumpusPotentiel) {
				int occurence=0;
				for(int[] c2 : wumpusPotentiel) {
					if(c1[0]==c2[0] && c1[1]==c2[1]) {
						occurence++;
					}
				}
				if(occurence>=2) {
					boolean add = true;
					for(int [] w : wumpusTrouve) {
						if(c1[0] == w[0] && c1[1] == w[1]) {
							add = false;
						}
					}
					if(add) {
						wumpusTrouve.add(c1);
					}
				}
			}
	        
	        

			// verification des incompatibilité
			ArrayList<Integer> impossible = new ArrayList<Integer>();
			for(int i = 0 ; i< wumpusTrouve.size();i++) {
				for(int[] v : this.neighbors(wumpusTrouve.get(i))) {
					if(this.explored[v[0]][v[1]] == 1 && this.plateau.getMatrice()[v[0]][v[1]] != 6 && this.plateau.getMatrice()[v[0]][v[1]] != 7 && this.plateau.getMatrice()[v[0]][v[1]] != 10 ) {
						impossible.add(i);
					}
			}
			}
			
			HashSet<Integer> set = new HashSet<>(impossible);
	        ArrayList<Integer> impossibleWithoutDuplicates = new ArrayList<>(set);
			Comparator<Integer> comparator = Collections.reverseOrder();
	        Collections.sort(impossibleWithoutDuplicates, comparator);
	        
	        
	        
			for(int i : impossibleWithoutDuplicates) {
				if(wumpusTrouve.size() > 1) {
				wumpusTrouve.remove(i);
				}
			}
			
			
			
			int incompatible = 1;			
			while(incompatible > 0) {
				incompatible = 0;
				int[] nbIncompatible = new int[wumpusTrouve.size()];
				for(int i = 0 ; i< wumpusTrouve.size();i++) {
					for(int[] w2 : wumpusTrouve) {
						if(this.insideTerritory(wumpusTrouve.get(i),w2) && this.ManhattanDistance(wumpusTrouve.get(i), w2) > 0) {
							nbIncompatible[i]++;
							incompatible++;
						}
					}
				}
				if(incompatible > 0) {
					// appeller le solver sat
					SolverSAT solver = new SolverSAT(this.plateau);
					ArrayList<int[]> cellToAdd = new  ArrayList<int[]>();
					
					for(int i = 0 ; i<x ; i++) {
						for(int j = 0 ;  j < y ; j++) {
							if(explored[i][j] == 1) {
								cellToAdd.add(new int[] {i,j});
							}
						}
					}
					solver.addCellsToDimacs(cellToAdd);
					ArrayList<Integer> indexToRemove = new ArrayList<Integer>();
					for(int i = 0 ; i < wumpusTrouve.size(); i++) {
						if(solver.solveCell(wumpusTrouve.get(i),solver.getSolver()) != 4) {
							indexToRemove.add(i);
						}
					}
					
			        
					int[] Occurence = new int[wumpusTrouve.size()];
					for(int i = 0 ; i< wumpusTrouve.size();i++) {
						for(int[] v : this.neighbors(wumpusTrouve.get(i))) {
							if(this.explored[v[0]][v[1]] == 1 && (this.plateau.getMatrice()[v[0]][v[1]] == 6 || this.plateau.getMatrice()[v[0]][v[1]] == 7 || this.plateau.getMatrice()[v[0]][v[1]] == 10 )) {
								Occurence[i]++;
							}
					}
					}
					int best= indexToRemove.get(0);
			        for(int i : indexToRemove) {
			        	if(nbIncompatible[i] == nbIncompatible[best]) {
			        		if(Occurence[i] > Occurence[best]) {
			        			best = i;
			        		}
			        	}
			        	if(nbIncompatible[i] > nbIncompatible[best]) {
			        		best = i;
			        	}
			        	
			        	
					}
			        if(wumpusTrouve.size() > 1) {
						wumpusTrouve.remove(best);
		        	}
				}
			}

			int[] Occurence = new int[wumpusTrouve.size()];
			for(int i = 0 ; i< wumpusTrouve.size();i++) {
				for(int[] v : this.neighbors(wumpusTrouve.get(i))) {
					if(this.explored[v[0]][v[1]] == 1 && (this.plateau.getMatrice()[v[0]][v[1]] == 6 || this.plateau.getMatrice()[v[0]][v[1]] == 7 || this.plateau.getMatrice()[v[0]][v[1]] == 10 )) {
						Occurence[i]++;
					}
			}
			}
			int best = 0;
			for(int i = 0 ; i< wumpusTrouve.size();i++) {
				if(Occurence[i] >= Occurence[best]) {
					best = i;
				}
			}
			int[] bestWumpus = wumpusTrouve.get(best);
			if(wumpusTrouve.size()==0) {
				this.Error=4;
				System.out.println("Aucun wumpus trouve");
			}
			else {
				this.TriDistanceAgent(wumpusTrouve);
				Obj[0]=bestWumpus[0];
				Obj[1]=bestWumpus[1];
			}
			break;
		}
		
		this.objectif[0]=Obj[0];
		this.objectif[1]=Obj[1];
	}
	
	// determine la prochaine cellule sur a eplxore par l'agent
	private ArrayList<int[]> newCellExplore() {
		ArrayList<int[]> newCell=new ArrayList<int[]>();
		
		for(int i=0; i<x; i++) {
			for(int j=0; j<y; j++) {
				if(explored[i][j]==1 && isSafe(new int[] {i,j})) {
					for(int[] c : this.neighbors(new int[] {i,j})) {
						if(explored[c[0]][c[1]]==0) {
							newCell.add(c);
						}
					}
				}
			}
		}
		
		return TriDistanceAgent(newCell);
	}
	
	// verifie si la case est sur ou non
	private boolean isSafe(int[] cell) {
		if(explored[cell[0]][cell[1]]==0) {
			return false;
		}
		
		switch(this.plateau.getMatrice()[cell[0]][cell[1]]) {
		default:
			return false;
		case 0: // case vide
			return true;
		case 2: // case Or
			return true;
		case 3: // case Wumpus
			return explored[cell[0]][cell[1]]==1;
		case 8:// wumpus mort
			return true;
		case 7: // odeur + brise
			boolean test = false;
			for(int[] c : this.neighbors(cell)) {
				if((explored[c[0]][c[1]]==1 && this.plateau.getMatrice()[c[0]][c[1]]==3) || this.plateau.getMatrice()[c[0]][c[1]]==8 && this.etats != State.EXPLORE) {
					test=true;
				}
			}			
			return test;
		case 6:// odeur
			test = false;
			for(int[] c : this.neighbors(cell)) {
				if((explored[c[0]][c[1]]==1 && this.plateau.getMatrice()[c[0]][c[1]]==3) || this.plateau.getMatrice()[c[0]][c[1]]==8) {
					test=true;
				}
				
			}
			return test;
		// odeur + or
		case 10:
			test = false;
			for(int[] c : this.neighbors(cell)) {
				if((explored[c[0]][c[1]]==1 && this.plateau.getMatrice()[c[0]][c[1]]==3) || this.plateau.getMatrice()[c[0]][c[1]]==8) {
					test=true;
				}
				
			}
			return test;
		}
	}
	
	private boolean isSafe(int[] cell, int[] End) {
		if(cell[0]==End[0]&&cell[1]==End[1]) {
			return true;
		}
		if(explored[cell[0]][cell[1]]==0) {
			return false;
		}
		switch(this.plateau.getMatrice()[cell[0]][cell[1]]) {
		default:
			return false;
		case 0:
			return true;
		case 2:
			return true;
		case 3:
			return explored[cell[0]][cell[1]]==1;
		// wumpus mort
		case 8:
			return true;
		case 7: // odeur + brise
			boolean test = false;
			for(int[] c : this.neighbors(cell)) {
				if((explored[c[0]][c[1]]==1 && this.plateau.getMatrice()[c[0]][c[1]]==3) || this.plateau.getMatrice()[c[0]][c[1]]==8 && this.etats != State.EXPLORE) {
					test=true;
				}
				if((c[0] == End[0] && c[1] == End[1]) && this.plateau.getMatrice()[c[0]][c[1]]==3 && this.etats != State.EXPLORE) {
					
					test=true;
				}
			}			
			return test;
		// odeur
		case 6:
			test = false;
			for(int[] c : this.neighbors(cell)) {
				if((explored[c[0]][c[1]]==1 && this.plateau.getMatrice()[c[0]][c[1]]==3) || this.plateau.getMatrice()[c[0]][c[1]]==8) {
					test=true;
				}
				if((c[0] == End[0] && c[1] == End[1]) && this.plateau.getMatrice()[c[0]][c[1]]==3) {
					
					test=true;
				}
			}
			
			return test;
		// odeur + or
		case 10:
			test = false;
			for(int[] c : this.neighbors(cell)) {
				if((explored[c[0]][c[1]]==1 && this.plateau.getMatrice()[c[0]][c[1]]==3) || this.plateau.getMatrice()[c[0]][c[1]]==8) {
					test=true;
				}
				if((c[0] == End[0] && c[1] == End[1]) && this.plateau.getMatrice()[c[0]][c[1]]==3) {
					
					test=true;
				}
			}
			return test;
		}
	}
	
	// changer l'etat de l'agent
	private void changeState(State nouvelleEtat) {
		//System.out.println("Changement d'etat vers "+nouvelleEtat);
		switch(this.etats) {
		default:
			break;
		case EXPLORE:
			switch(nouvelleEtat) {
			default:
				break;
			case BACKTRACK:
				this.old_etats=State.EXPLORE;
				this.etats=State.BACKTRACK;
				break;
			case KILLWUMPUS:
				this.etats=State.KILLWUMPUS;
				break;
			}
			break;
		case BACKTRACK:
			this.etats=nouvelleEtat;
			break;
		case KILLWUMPUS:
			switch(nouvelleEtat) {
			default:
				break;
			case EXPLORE:
				this.etats=State.EXPLORE;
				break;
			case BACKTRACK:
				this.old_etats=State.EXPLORE;
				this.etats=State.BACKTRACK;
			}
			break;
		}
	}
	
	// charger le solveur pour nos cellule explore

	
	private ArrayList<int[]> TriDistanceAgent(ArrayList<int[]> cell) {
		// trier tout l'array list selon leur proximite a l'agent
		Collections.sort(cell,new Comparator<int[]>() {
				// ajout d'un critere de trie
				public int compare(int[] x, int[] y) {
					return Integer.compare(ManhattanDistance(x,position), ManhattanDistance(y,position));
				}
			}
		);
		
		return cell;
	}
	
	// determiner les case wumpus potentielle adjavente aux odeurs connus
	private ArrayList<int[]> WumpusADetermine() {
		ArrayList<int[]> stack=new ArrayList<int[]>();
		for(int i=0; i<x; i++) {
			for(int j=0; j<y; j++) {
				if(explored[i][j]==1 && (
						this.plateau.getMatrice()[i][j]==6 ||
						this.plateau.getMatrice()[i][j]==7 ||
						this.plateau.getMatrice()[i][j]==10)) {
					boolean isWumpusDead = true;
					for(int[] c : this.neighbors(new int[] {i,j})) {
						if(explored[c[0]][c[1]]==1 && (this.plateau.getMatrice()[c[0]][c[1]] == 8 || this.plateau.getMatrice()[c[0]][c[1]] == 3)) {
							isWumpusDead = false;
						}
					}
					if(isWumpusDead) {
						for(int[] c : this.neighbors(new int[] {i,j})) {
							if(explored[c[0]][c[1]]==0) {
								stack.add(c);
							}
						}
					}					
				}
			}
		}
		return stack;
	}
	
	public int getError() {
		return Error;
	}
	
	
	
	// methode plateau
	public boolean insideMatrix(int i, int j) {
        return !(i > this.x - 1 || i < 0 || j > this.y - 1 || j < 0);
    }
	
	// methode plateau
	private ArrayList<int[]> neighbors(int[] coords) {
        ArrayList<int[]> neighbors = new ArrayList<int[]>();
        if (insideMatrix(coords[0] + 1, coords[1])) {
            int[] newNeighbor = { coords[0] + 1, coords[1] };
            neighbors.add(newNeighbor);
        }
        if (insideMatrix(coords[0] - 1, coords[1])) {
            int[] newNeighbor = { coords[0] - 1, coords[1] };
            neighbors.add(newNeighbor);
        }
        if (insideMatrix(coords[0], coords[1] + 1)) {
            int[] newNeighbor = { coords[0], coords[1] + 1 };
            neighbors.add(newNeighbor);
        }
        if (insideMatrix(coords[0], coords[1] - 1)) {
            int[] newNeighbor = { coords[0], coords[1] - 1 };
            neighbors.add(newNeighbor);

        }
       
      return neighbors;
    }
	
	// methode plateau
	private int ManhattanDistance(int[] x, int[] y) {
        return Math.abs(x[0] - y[0]) + Math.abs(x[1] - y[1]) ;
    }
	
	
	
	
	
	private ArrayList<int[]> AStarPathFinding( int[] Start, int[] End) {
		
		for(int[] c : this.neighbors(End)) {
		}
		if(Start[0] == End[0] && Start[1] == End[1]) {
			return new ArrayList<int[]>();
		} 
		int[][] matrice=this.plateau.getMatrice();
		//// set up pathfinding
       // parents matrice
       int[][][] parents = new int[x][y][2];
       for (int j = 0; j < x; j++) {
           for (int k = 0; k < y; k++) {
               parents[j][k][0] = -1;
               parents[j][k][1] = -1;
           }
       }
       parents[Start[0]][Start[1]][0] = -2;
       //
       // State matrice
       int[][] state = new int[x][y];
       for (int j = 0; j < x; j++) {
           for (int k = 0; k < y; k++) {
               state[j][k] = 0;
           }
       }
       state[Start[0]][Start[1]] = 2;
       //
   	ArrayList<int[]> Path = new ArrayList<int[]>();
       // init
       boolean endIsFound = false;
   			// ajout des nouveau node a evaluer
   	        while (!endIsFound) {   

   	            for (int i = 0; i < x; i++) {
   	                for (int j = 0; j < y; j++) {
   	                    if (state[i][j] == 2) {// case vu par l'algoritme
   	                        int[] StartCoord = { i, j };
   	                        ArrayList<int[]> voisins = this.neighbors(StartCoord);
   	                        for (int[] coordVoisin : voisins) {
   	                            if (state[coordVoisin[0]][coordVoisin[1]] == 0 && this.isSafe(coordVoisin,End)) {
   	                                state[coordVoisin[0]][coordVoisin[1]] = 1;
   	                                parents[coordVoisin[0]][coordVoisin[1]][0] = i;
   	                                parents[coordVoisin[0]][coordVoisin[1]][1] = j;
   	                            }
   	                        }
   	                    }
   	                }
   	            }
   	            // evaluation des node
   	            double minFinal = -1;
   	            double endFinal = -1;
   	            int[] newNode = { 0, 0 };
   	            for (int i = 0; i < x; i++) {
   	                for (int j = 0; j < y; j++) {
   	                    if (state[i][j] == 1) {// case a évaluer par l'agorithme
   	                    	//calcul des distance euclidienne
   	                        double startDistance = ManhattanDistance(Start, new int[] {i,j});
   	                        double endDistance = ManhattanDistance(End, new int[] {i,j});
   	                        double Somme = (startDistance + (endDistance*100));
   	                        if (minFinal == -1) {
   	                            minFinal = Somme;
   	                            endFinal = endDistance;
   	                            newNode[0] = i;
   	                            newNode[1] = j;
   	                        } else if (Somme < minFinal) {
   	                            minFinal = Somme;
   	                            newNode[0] = i;
   	                            newNode[1] = j;
   	                            endFinal = endDistance;
   	                        } else if (Somme == minFinal) {
   	                            if (endFinal == -1) {
   	                                minFinal = Somme;
   	                                endFinal = endDistance;
   	                                newNode[0] = i;
   	                                newNode[1] = j;
   	                            } else if (endDistance < endFinal) {
   	                                minFinal = Somme;
   	                                endFinal = endDistance;
   	                                newNode[0] = i;
   	                                newNode[1] = j;
   	                            }
   	                        }
   	                    }
   	                }
   	            }
   	            state[newNode[0]][newNode[1]] = 2;

   	            if (newNode[0] == End[0] && newNode[1] == End[1]) {
   	                endIsFound = true;
   	                int[] currentPoint = { newNode[0], newNode[1] };
   	                boolean startFound = false;
   	                while (!startFound) {

   	                    Path.add(currentPoint);

   	                    if (parents[currentPoint[0]][currentPoint[1]][0] != -2
   	                            && parents[currentPoint[0]][currentPoint[1]][1] != -2) {
   	                        currentPoint = parents[currentPoint[0]][currentPoint[1]];
   	                    } else {
   	                        startFound = true;
   	                    }
   	                }

   	            }

   	        }
   	 Path.remove(Path.size()-1);
   	 return Path;

   }
	
private boolean insideTerritory(int[] Wumpus,int[] Cell) {
    	ArrayList<int[]> territoire= new ArrayList<int[]>();        
    	for(int i = -2 ; i<=2;i++) {
    		for(int j = -2 ; j<=2;j++) {
    			if(!(i == 0 && j == 0) && this.insideMatrix(Wumpus[0]+i, Wumpus[1]+j)) {
    				if(Wumpus[0]+i == Cell[0] &&  Wumpus[1]+j == Cell[1]) {
    					return true;
    				}
    			}
    		}
    	}
    	
	return false;
}
public int getNbMvt() {
	return this.nbMvt;
}
	public int[] getPos() {
		return this.position;
		}
	
	
	
}