package solve;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.reader.DimacsReader;
import org.sat4j.reader.ParseFormatException;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;
import org.sat4j.specs.TimeoutException;

import gen.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
/*
* Definition des symbolmes
* Vide(x,y) : (x,y) est vide
* Wumpus(x,y) : (x,y) contient le wumpus
* Pit(x,y) : (x,y) contient un trou
* Brise(x,y) : (x,y) contient une brise
* Odeur(x,y) : (x,y) contient une odeur
* Gold(x,y) : (x,y) contient un gold
* Eclat(x,y) ; (x,yà un eclat est perceptible
* Numérotation unique des symboles en chaque point de la matrice
* pour une grille de taille x , y:
* V -> de (x*y)*0 a (x*y)*1-1 | formule depuis i,j -> j + y*i + (y*x)*0
* W -> de (x*y)*1 a (x*y)*2-1 | formule depuis i,j -> j + y*i + (y*x)*1
* P -> de (x*y)*2 a (x*y)*3-1 | formule depuis i,j -> j + y*i + (y*x)*2
* B -> de (x*y)*3 a (x*y)*4-1 | formule depuis i,j -> j + y*i + (y*x)*3
* O -> de (x*y)*4 a (x*y)*5-1 | formule depuis i,j -> j + y*i + (y*x)*4
* G -> de (x*y)*5 a (x*y)*6-1 | formule depuis i,j -> j + y*i + (y*x)*5
* E -> de (x*y)*6 a (x*y)*7-1 | formule depuis i,j -> j + y*i + (y*x)*6
*/

public class Solver {
    private Plateau plateau;
    private int x;
    private int y;
    ArrayList<int[]> WumpusFound;
    private int[][] explored;
    private int[][] solvedCells;
    private boolean Error;
    
public Solver(Plateau plateau) {
        this.plateau = plateau;
        this.x = plateau.getMatrice().length;
        this.y = plateau.getMatrice()[0].length;  
        this.WumpusFound = new ArrayList<int[]>();  
        this.explored = new int[x][y];
        solvedCells = new int[x][y];   
        this.Error = false;
        
        for(int i = 0 ; i<x ; i++) {
        	for(int j = 0 ; j<y ; j++) {
        		explored[i][j] = 0;
        		solvedCells[i][j] = 0;
        	}
        }
        explored[0][0] = 1;
        solvedCells[0][0] = 2;
        
    }

public boolean isSolvable()  {    	
    	/*
    	 * Codage des entier dans la matrice SolvedCells
    	 * 0 case non evalué
    	 * 2 case vide deja evaluée
    	 * 3 Trou
    	 * 4 Wumpus
    	 */	
    	
	    
	    ArrayList<int[]> cellsToSolve = new ArrayList<int[]>();
	    ArrayList<int[]> newSafeCells = new ArrayList<int[]>();
	    WumpusFound = new ArrayList<int[]>();
	    
	    // initialisation
	    boolean Continue = true;
	    int[] CountOld = new int[5];
	    for(int i = 0 ; i<5;i++) {
	    	CountOld[i] = 0;
	    }
        int[] Count = new int[5];
        do{     
        	
        	// ajout des cellules vides
        	this.addSafeCells();
        	//reset newSafeCells
        	newSafeCells= new ArrayList<int[]>();
        	
        	if(this.nbOrFound(solvedCells) == plateau.getNbOr()) {
            	
        		for(int[] w : this.WumpusFound) {
             		if(this.plateau.getMatrice()[w[0]][w[1]] != 3) {
             			System.out.println("ERROR en "+w[0]+","+w[1]);
             			this.Error = true;
             		}
             	}
        		return true;
        	}
         	
         	cellsToSolve = newcellsToSolve();
         	
         	ArrayList<int[]> newCells = new ArrayList<int[]>();
           // relecture du dimacs   	        
  	       if(cellsToSolve.size() == 0) {
  	    	   return false;
  	       }
  	       ArrayList<int[]> wumpusPotentiel = new ArrayList<int[]>();
         	for(int[] cell : cellsToSolve) {
         		
         		int occurance = 0;
         		for(int [] cell2 : cellsToSolve) {
         			if(cell[0] == cell2[0] && cell[1] == cell2[1]) {
         				occurance++;
         			}
         		}
         		if(occurance > 1) { // && occurance < 4
         			boolean add = true;
         			for(int[] w : wumpusPotentiel) {
         				if(w[0] == cell[0] && w[1] == cell[1]) {
         					add = false;
         				}
         			}
         			if(add) {
             			wumpusPotentiel.add(cell);
         			}
         		}
         		    
         		
         	}       	
         	
         	

         for(int[] w : wumpusPotentiel) {
         }
         //retrait des wumpus impossible
         ArrayList<Integer> impossible = new ArrayList<Integer>();
         for(int i = 0 ; i<wumpusPotentiel.size(); i++) {
        	 for(int[] v : this.neighbors(wumpusPotentiel.get(i))) {
        		 if(this.explored[v[0]][v[1]] == 1 && this.plateau.getMatrice()[v[0]][v[1]] != 6 && this.plateau.getMatrice()[v[0]][v[1]] != 7 && this.plateau.getMatrice()[v[0]][v[1]] != 10 ) {
						boolean add = true;
						for(int j : impossible) {
							if(j == i) {
								add = false;
							}
						}
						if(add) {
							impossible.add(i);
						}
					}
        	 }
         }
         
         Comparator<Integer> comparator = Collections.reverseOrder();
	     Collections.sort(impossible, comparator);

         
         for(int i : impossible) {
        	 wumpusPotentiel.remove(i);
				
		}
         
         //retrait des incompatible
         
         int incompatible = 1;			
			while(incompatible > 0) {
				incompatible = 0;
				int[] nbIncompatible = new int[wumpusPotentiel.size()];
				for(int i = 0 ; i< wumpusPotentiel.size();i++) {
					for(int[] w2 : wumpusPotentiel) {
						if(this.insideTerritory(wumpusPotentiel.get(i),w2) && this.ManhattanDistance(wumpusPotentiel.get(i), w2) > 0) {
							nbIncompatible[i]++;
							incompatible++;
						}
					}
				}
				if(incompatible > 0) {
					// appeller le solver sat
					SolverSAT solver = new SolverSAT(this.plateau);
					ArrayList<int[]> cellToAdd = new  ArrayList<int[]>();
					
					for(int i = 0 ; i<x ; i++) {
						for(int j = 0 ;  j < y ; j++) {
							if(explored[i][j] == 1) {
								cellToAdd.add(new int[] {i,j});
							}
						}
					}
					solver.addCellsToDimacs(cellToAdd);
					
					ArrayList<Integer> indexToRemove = new ArrayList<Integer>();
					for(int i = 0 ; i < wumpusPotentiel.size(); i++) {
						if(solver.solveCell(wumpusPotentiel.get(i),solver.getSolver()) != 4) {
							boolean add = true;
							for(int j : indexToRemove) {
								if(j == i) {
									add = false;
								}
							}
							if(add) {
								indexToRemove.add(i);
							}

						}
					}

					
					int[] Occurence = new int[wumpusPotentiel.size()];
					for(int i = 0 ; i< wumpusPotentiel.size();i++) {
						for(int[] v : this.neighbors(wumpusPotentiel.get(i))) {
							if(this.explored[v[0]][v[1]] == 1 && (this.plateau.getMatrice()[v[0]][v[1]] == 6 || this.plateau.getMatrice()[v[0]][v[1]] == 7 || this.plateau.getMatrice()[v[0]][v[1]] == 10 )) {
								Occurence[i]++;
							}
					}
					}
					if(indexToRemove.size()>0) {
						int best= indexToRemove.get(0);
				        for(int i : indexToRemove) {
				        	if(nbIncompatible[i] == nbIncompatible[best]) {
				        		if(Occurence[i] > Occurence[best]) {
				        			best = i;
				        		}
				        	}
				        	if(nbIncompatible[i] > nbIncompatible[best]) {
				        		best = i;
				        	}
				        	
				        	
						}
						wumpusPotentiel.remove(best);
					}				
		        	
				}
			}
         
         
         
         for(int[] w : wumpusPotentiel) {
  			explored[w[0]][w[1]] = 1;
  			solvedCells[w[0]][w[1]] = 4;
  			this.WumpusFound.add(w);
         }
         for(int i = 0 ;i<x;i++) {
        	 for(int j = 0 ; j<y;j++) {
        		 Count[solvedCells[i][j]]++;
        	 }
         }
         Continue = false;
         
         for(int i = 0 ; i<5;i++) {
        	 if(Count[i] > (x*y)*(x*y) +1) {
            	 return false;
             }
 	    	if(Count[i] != CountOld[i]) {
 	    		Continue = true;
 	    	}
 	    }
        
        }while(Continue);
       
         return false;
    }

public int nbWumpusRequired()  {    	
	if(this.isSolvable()) {		
		return this.WumpusFound.size();
	}else {
		return -1;
	}	
}
private int nbOrFound(int[][] solvedCells) {
	int nbOr = 0;
	for(int i =0 ;i<x;i++) {
		for(int j = 0 ; j<y;j++) {	
			if((solvedCells[i][j] == 1 || solvedCells[i][j] == 2)
					&& (
						this.plateau.getMatrice()[i][j] == 2 || 
						this.plateau.getMatrice()[i][j] == 9 || 
						this.plateau.getMatrice()[i][j] == 10 )) {
				nbOr++;
			}
			}
		}
	
	return nbOr;
}

private int ManhattanDistance(int[] x, int[] y) {
    return Math.abs(x[0] - y[0]) + Math.abs(x[1] - y[1]) ;
}

private ArrayList<int[]> newcellsToSolve(){
	ArrayList<int[]> newCells = new ArrayList<int[]>();
	for(int i =0 ;i<x;i++) {
		for(int j = 0 ; j<y;j++) {	
				if(solvedCells[i][j] == 1 && 
						(this.plateau.getMatrice()[i][j] == 6 ||
						this.plateau.getMatrice()[i][j] == 7  ||
						this.plateau.getMatrice()[i][j] == 10 )) {
					boolean test = true;
					for(int [] coords : this.neighbors(new int[] {i,j})) {
						if(explored[coords[0]][coords[1]] ==1 && this.plateau.getMatrice()[coords[0]][coords[1]] ==3) {
							test = false;
						}
					}
					if(test) {
						for(int [] coords : this.neighbors(new int[] {i,j})) {
							if(explored[coords[0]][coords[1]] == 0) {
								newCells.add(coords);
							}
						}
					}
					
				}
			}		
		}
	return newCells;
}


private void addSafeCells() {
	for(int i = 0;i<x;i++) {
		for(int j = 0 ; j<y;j++) {
			if(solvedCells[i][j] == 1 && this.isSafe(new int[] {i,j})) {
				solvedCells[i][j] = 2;
			}
		}
	}
	int nbaction = 1;
	while(nbaction > 0) {
		nbaction = 0;
		for(int i = 0;i<x;i++) {
			for(int j = 0 ; j<y;j++) {
				if(this.isSafe(new int[] {i,j})) {
					for(int[] c : this.neighbors(new int[] {i,j})) {
						if(explored[c[0]][c[1]] == 0) {
							nbaction++;
							explored[c[0]][c[1]] = 1;
							if(this.isSafe(c)) {
								solvedCells[c[0]][c[1]] = 2;
							}else {
								solvedCells[c[0]][c[1]] = 1;
							}
						}
					}
				}
			}
		}

	}
}

   

private boolean insideMatrix(int i, int j) {
        return !(i > this.x - 1 || i < 0 || j > this.y - 1 || j < 0);
    }


private ArrayList<int[]> neighbors(int[] coords) {
        ArrayList<int[]> neighbors = new ArrayList<int[]>();
        if (insideMatrix(coords[0] + 1, coords[1])) {
            int[] newNeighbor = { coords[0] + 1, coords[1] };
            neighbors.add(newNeighbor);
        }
        if (insideMatrix(coords[0] - 1, coords[1])) {
            int[] newNeighbor = { coords[0] - 1, coords[1] };
            neighbors.add(newNeighbor);
        }
        if (insideMatrix(coords[0], coords[1] + 1)) {
            int[] newNeighbor = { coords[0], coords[1] + 1 };
            neighbors.add(newNeighbor);
        }
        if (insideMatrix(coords[0], coords[1] - 1)) {
            int[] newNeighbor = { coords[0], coords[1] - 1 };
            neighbors.add(newNeighbor);
        }
       
      return neighbors;
    }



	
	private boolean isSafe(int[] cell) {
		if(explored[cell[0]][cell[1]]==0) {
			return false;
		}
		
		switch(this.plateau.getMatrice()[cell[0]][cell[1]]) {
		default:
			return false;
		case 0:
			return true;
		case 2:
			return true;
		case 3:
			return explored[cell[0]][cell[1]]==1;
		// wumpus mort
		case 8:
			return true;
		// odeur
		case 6:
			boolean test = false;
			for(int[] c : this.neighbors(cell)) {
				if((explored[c[0]][c[1]]==1 && this.plateau.getMatrice()[c[0]][c[1]]==3) || this.plateau.getMatrice()[c[0]][c[1]]==8) {
					test=true;
				}
			}
			return test;
		// odeur + or
		case 10:
			test = false;
			for(int[] c : this.neighbors(cell)) {
				if((explored[c[0]][c[1]]==1 && this.plateau.getMatrice()[c[0]][c[1]]==3) || this.plateau.getMatrice()[c[0]][c[1]]==8) {
					test=true;
				}
			}
			return test;
		}
	}
	private boolean insideTerritory(int[] Wumpus,int[] Cell) {
    	ArrayList<int[]> territoire= new ArrayList<int[]>();        
    	for(int i = -2 ; i<=2;i++) {
    		for(int j = -2 ; j<=2;j++) {
    			if(!(i == 0 && j == 0) && this.insideMatrix(Wumpus[0]+i, Wumpus[1]+j)) {
    				if(Wumpus[0]+i == Cell[0] &&  Wumpus[1]+j == Cell[1]) {
    					return true;
    				}
    			}
    		}
    	}
    	
	return false;
}
}



 