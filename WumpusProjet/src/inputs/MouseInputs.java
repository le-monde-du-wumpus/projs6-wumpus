package inputs;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.time.Year;

import entities.Player;
import gamestates.gamestates;
import main.GamePanel;

public class MouseInputs implements MouseListener,MouseMotionListener {
	
	private GamePanel gamePanel;
	public MouseInputs(GamePanel gamePanel) {
		this.gamePanel = gamePanel;	
		
	}
	
	
	@Override
	public void mouseClicked(MouseEvent e) {
		switch(gamestates.state) {
		case MENU:
			gamePanel.getGame().getMenu().MouseClicked(e);;
			break;
		case Regle:
			gamePanel.getGame().getRegle().MouseClicked(e);
			break;
		case PLAYING:
			gamePanel.getGame().getPlaying().MouseClicked(e);
			break;
		case MAP:
			gamePanel.getGame().getMap().MouseClicked(e);
			break;
		case DEATHSCREEN:
			gamePanel.getGame().getdeathScreen().MouseClicked(e);
			break;
		case AGENTWIN:
			gamePanel.getGame().getAgentWin().MouseClicked(e);
			break;
		case WINSCREEN:
			gamePanel.getGame().getwinScreen().MouseClicked(e);
			break;
		default:
			break;
		
		
		}
		
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		switch(gamestates.state) {
		case MENU:
			gamePanel.getGame().getMenu().MouseReleased(e);
			break;
		case Regle:
			gamePanel.getGame().getRegle().MouseReleased(e);
			break;	
		case MAP:
			gamePanel.getGame().getMap().MouseReleased(e);
			break;	
		case DEATHSCREEN:
			gamePanel.getGame().getdeathScreen().MouseReleased(e);
			break;
		case AGENTWIN:
			gamePanel.getGame().getAgentWin().MouseReleased(e);
			break;
		case WINSCREEN:
			gamePanel.getGame().getwinScreen().MouseReleased(e);
			break;
		default:
			break;		
		}		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseDragged(MouseEvent e) {
		
		
	}


	@Override
	public void mouseMoved(MouseEvent e) {
		switch(gamestates.state) {
		case MENU:
			gamePanel.getGame().getMenu().MouseMouved(e);
			break;
		case Regle:
			gamePanel.getGame().getRegle().MouseMouved(e);
			break;		
		case MAP:
			gamePanel.getGame().getMap().MouseMouved(e);
			break;	
		case DEATHSCREEN:
			gamePanel.getGame().getdeathScreen().MouseMouved(e);
			break;
		case AGENTWIN:
			gamePanel.getGame().getAgentWin().MouseMouved(e);
			break;
		case WINSCREEN:
			gamePanel.getGame().getwinScreen().MouseMouved(e);
			break;
		default:
			break;
		
		
		}
		
	}
	
}
