package inputs;

import java.awt.event.KeyEvent;

import java.awt.event.KeyListener;

import gamestates.gamestates;
import main.GamePanel;



public class KeyboardInputs implements KeyListener{
	private GamePanel gamePanel;
	
	
	public KeyboardInputs(GamePanel gamePanel){
		 
		this.gamePanel = gamePanel;	
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		
		
		
	}
	 
	public void keyReleased(KeyEvent e) {

		switch(gamestates.state) {
		case MENU:
			gamePanel.getGame().getMenu().KeyReleased(e);
			break;
		case PLAYING:
			gamePanel.getGame().getPlaying().KeyReleased(e);
			break;
			
		case MAP:
			gamePanel.getGame().getMap().KeyReleased(e);
			break;
		case DEATHSCREEN:
			gamePanel.getGame().getdeathScreen().KeyReleased(e);
			break;	
		case WINSCREEN:
			gamePanel.getGame().getwinScreen().KeyReleased(e);
			break;
		case NEWGAME:
			gamePanel.getGame().getNewGame().KeyReleased(e);
			break;	
		case AGENTWIN:
			gamePanel.getGame().getAgentWin().KeyReleased(e);
			break;	
		case Regle:
			gamePanel.getGame().getRegle().KeyReleased(e);
			break;	
		default:
			break;
		
		
		}
	}
	
	
	public void keyPressed(KeyEvent e) {

		switch(gamestates.state) {
		case MENU:
			gamePanel.getGame().getMenu().KeyPressed(e);
			break;
		case PLAYING:
			gamePanel.getGame().getPlaying().KeyPressed(e);
			break;
		case MAP:
			gamePanel.getGame().getMap().KeyPressed(e);
			break;	
		case DEATHSCREEN:
			gamePanel.getGame().getdeathScreen().KeyPressed(e);
			break;		
		case WINSCREEN:
			gamePanel.getGame().getwinScreen().KeyPressed(e);
			break;
		case NEWGAME:
			gamePanel.getGame().getNewGame().KeyPressed(e);
			break;
		case AGENTWIN:
			gamePanel.getGame().getAgentWin().KeyPressed(e);
			break;	
		case Regle:
			gamePanel.getGame().getRegle().KeyPressed(e);
			break;	
		default:
			break;
		
		
		}
	}
	
	
}

