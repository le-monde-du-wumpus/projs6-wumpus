package main;

import java.util.ArrayList;

import gamestates.Difficultes;
import gamestates.Playing;
import gen.Plateau;
import solve.Agent;
import solve.SolverSAT;
import utilz.Blocks;

public class PlateauHandler {
	private Plateau plateau;
	private Playing playing;
	private int x,y;
	private static final int VIDE = 0;
	private static final int OR = 2;
	private static final int PIT = 4;
	private static final int WUMPUS = 3;	
	private static final int BRISE = 5;
	private static final int ODEUR = 6;
	private static final int ODEUR_BRISE = 7;
	private static final int WUMPUS_MORT = 8;
	private static final int OR_BRISE = 9;
	private static final int OR_ODEUR = 10;
	
	
	public PlateauHandler(int x, int y , int seed , int recursionMax,double tauxRecouvrementMax , int nbOr , int nbWumpus,Playing playing) {
		this.plateau = new Plateau(x, y, seed, recursionMax, tauxRecouvrementMax, nbOr,nbWumpus);		
		this.playing = playing;
		this.x = plateau.getX();
		this.y = plateau.getY();
		
		
	}
	
	public void refresh(int x, int y , int seed , int recursionMax,double tauxRecouvrementMax , int nbOr , int nbWumpus) {
		this.plateau = new Plateau(x, y, seed, recursionMax, tauxRecouvrementMax, nbOr,nbWumpus);		
		this.UpdateExplore(new int[] {0,0});
		this.x = plateau.getX();
		this.y = plateau.getY();
	}
	
	
	public int[][] minicarte(int i , int j){
		int[][] map = new int[3][3]; 
		map[1][1] = this.plateau.getMatrice()[i][j];
		
		
		SolverSAT solver = new SolverSAT();
		if(this.playing.difficultes == Difficultes.facile) {
			solver = new SolverSAT(this.getPlateau());
			ArrayList<int[]> ExploredCells = new ArrayList<int[]>();
    		for (int a = 0; a < x; a++) {
	            for (int b = 0; b < y; b++) { 
	            	if(this.getPlateau().getExplored()[a][b] ==1) {
	            		ExploredCells.add(new int[] {a,b});
	            		}
	            	}
	            }
    		solver.addCellsToDimacs(ExploredCells);
		}
		
		for(int a = -1 ; a<2 ; a++) {
			for(int b = -1 ; b<2 ;b++) {
				if(this.plateau.insideMatrix(i+a, j+b)) {//exploré
					if(this.plateau.getExplored()[i+a][j+b] ==1) { 
						map[1+a][1+b] = this.plateau.getMatrice()[i+a][j+b];
					}else {//inexploré
						switch(this.playing.difficultes) {
							default:
								map[1+a][1+b] = -1;
								break;
							case facile:
								if(this.nextoExplored(new int[] {i+a,j+b})){
					        		//traitement solver
					        		boolean done = false;
					        		int CellValue = 12; // par default pas safe
					        		for(int[] v : this.neighbors(new int[] {i+a,j+b})) {
					        			if(this.isSafe(new int[] {v[0],v[1]})) {
					        				done = true;
						        			CellValue = 11;
					        			}
					        		}
					        		if(!done) {
					        			switch(solver.solveCell(new int[] {i+a,j+b}, solver.getSolver())) {
					        				default:
					        					break;
					        				case 4: // case wumpus
					        					CellValue = 3;
					        					break;
					        					
					        			}
					        		}
					        		map[1+a][1+b] = CellValue;
					        	}else {
					        		map[1+a][1+b] = -1;
					        	}
								
								break;
						}
					}
				}else {//out of bounds
					map[1+a][1+b] = -2;
				}
			}
		}
		
		return map;
	}
	
	private boolean isSafe(int[] cell) {
		if(this.getPlateau().getExplored()[cell[0]][cell[1]] == 0) {
			return false;
		}
		switch(this.getPlateau().getMatrice()[cell[0]][cell[1]]) {
		default:
			return false;
		case 0: // case vide
			return true;
		case 2: // case Or
			return true;
		case 8:// wumpus mort
			return true;		
		case 6:// odeur
			boolean test = false;
			for(int[] c : this.neighbors(cell)) {
				if(this.getPlateau().getMatrice()[c[0]][c[1]]==8) {
					test=true;
				}
				
			}
			return test;
		// odeur + or
		case 10:
			test = false;
			for(int[] c : this.neighbors(cell)) {
				if(this.getPlateau().getMatrice()[c[0]][c[1]]==8) {
					test=true;
				}
				
			}
			return test;
		}
	}
	
	
    public boolean insideMatrix(int i, int j) {
        return !(i > this.x - 1 || i < 0 || j > this.y - 1 || j < 0);
    }
    private ArrayList<int[]> neighbors(int[] coords) {
        ArrayList<int[]> neighbors = new ArrayList<int[]>();
        if (insideMatrix(coords[0] + 1, coords[1])) {
            int[] newNeighbor = { coords[0] + 1, coords[1] };
            neighbors.add(newNeighbor);
        }
        if (insideMatrix(coords[0] - 1, coords[1])) {
            int[] newNeighbor = { coords[0] - 1, coords[1] };
            neighbors.add(newNeighbor);
        }
        if (insideMatrix(coords[0], coords[1] + 1)) {
            int[] newNeighbor = { coords[0], coords[1] + 1 };
            neighbors.add(newNeighbor);
        }
        if (insideMatrix(coords[0], coords[1] - 1)) {
            int[] newNeighbor = { coords[0], coords[1] - 1 };
            neighbors.add(newNeighbor);

        }

      return neighbors;
    }
	public boolean nextoExplored(int[] pos) {
		boolean test = false;
		for(int[] v : this.neighbors(pos)) {
			if(playing.getPlateauHandler().getPlateau().getExplored()[v[0]][v[1]] == 1) {
				test = true;
			}
		}
		return test;
	}
	
	
	
	public Blocks[][] levelMatrice(int i , int j) {
		
		Blocks[][] Level = new Blocks[16][28];	
		for(int a =0 ; a<16 ; a++) {
			for(int b =0 ; b<28 ; b++) {
				Level[a][b] = Blocks.BACKGROUND;
			}
		}
		
		
		switch(plateau.getMatrice()[i][j]) {
			default: // cas vide
				Default(Level);
				break;
			case VIDE: // cas vide
				Default(Level);
				break;
			case OR: // cas or
				Default(Level); //or
				break;
			
		}		
		// DOORS
		// porte OUEST {7,1}
		if(this.plateau.insideMatrix(i, j-1)) {
			Level[7][2] = Blocks.DOOR_LEFT; 
		}
		// porte EST {7,20}
		if(this.plateau.insideMatrix(i, j+1)) {
			Level[7][20] = Blocks.DOOR_RIGHT; 
		}
		// porte NORD {1,10}
		if(this.plateau.insideMatrix(i-1, j)) {
			Level[2][11] = Blocks.DOOR_TOP; 
		}
		// porte SUD {13,10}
		if(this.plateau.insideMatrix(i+1, j)) {
			Level[12][11] = Blocks.DOOR_BOT; 
		}
		
		return Level;		
	}
	
	public void Default(Blocks[][] Level) {
		for(int a = 0;a<28;a++) {
			for(int b = 0 ; b < 16 ; b++) {
				if(b > 0 && b <14 && a > 0 && a < 22) { //PLATEAU DE JEU STANDART 
					if(b < 3 || b >11 ) { 
						if(a>2 && a<20) { //WALL TOP ET BOT
							if(b == 2) { //WALL TOP 
									Level[b][a] = Blocks.WALL_TOP; 
					
							}
							if(b == 12) { //WALL BOT 1
									Level[b][a] = Blocks.WALL_BOT; 

							}
							//CORNER	
						}else { 
							if(a==2 && b==2) { // CORNER HAUT GAUCHE
								Level[b][a] = Blocks.CORNER_TOP_LEFT;
							}
							if(a==2 && b==12) { //CORNET BAS GAUCHE
								Level[b][a] = Blocks.CORNER_BOT_LEFT;
							}
							if(a==20 && b==2) { // CORNER HAUT DROITE
								Level[b][a] = Blocks.CORNER_TOP_RIGHT;
							}
							if(a==20 && b==12) { //CORNET BAS DROITE
								Level[b][a] = Blocks.CORNER_BOT_RIGHT;
							}
							
						}  
							
							
							
							
						}else if( a < 3 || a > 19) { //WALL LEFT RIGHT
							if(b>2 && b<13) { 
								
								if(a == 2) { //WALL LEFT 1
									
										Level[b][a] = Blocks.WALL_LEFT; 
						
								}
								if(a == 20) { //WALL RIGHT 1
									
										Level[b][a] = Blocks.WALL_RIGHT; 
										
									
									
									
								}
							
								
							}
							
						}
					if(a>2 && a<20 && b>2 && b<12) {
						Level[b][a] = Blocks.CAVE_FLOOR_DEFAULT; 
					}
				}else {
					Level[b][a] = Blocks.BACKGROUND; 
				}
			}
		}
	}
	/*
	public void OR(Blocks[][] Level) {
		for(int a = 0;a<28;a++) {
			for(int b = 0 ; b < 16 ; b++) {
				if(b > 0 && b <14 && a > 0 && a < 22) {
					if(b == 1 || b == 13 || a == 1 || a == 21) {
						Level[b][a] = Blocks.WALL_TOP;
					}else {						
						Level[b][a] = Blocks.OR; 
					}					
				}else {
					Level[b][a] = Blocks.BACKGROUND; 
				}
			}
		}
	}*/
	
	public void UpdateExplore(int[] posPlayer) {
		this.plateau.setExplored(posPlayer[0],posPlayer[1],1);
		
	}
	
	public Plateau getPlateau() {
		return this.plateau;
	} 
	
}	




