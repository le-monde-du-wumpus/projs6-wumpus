package main;

import java.io.File;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

public class LecteurAudio {
	
	public LecteurAudio() {
		
	}
	
	public static void Rugissement() {
		try {
	        File fichierAudio = new File("src/res/wumpos_roar.wav");
	        Clip clip = AudioSystem.getClip();
	        clip.open(AudioSystem.getAudioInputStream(fichierAudio));
	        
	      
	        
	        clip.start();
	    } catch (Exception e) {
	        System.err.println(e.getMessage());
	    }
	}
	
	public static void Gold() {
		try {
	        File fichierAudio = new File("src/res/Gold.wav");
	        Clip clip = AudioSystem.getClip();
	        clip.open(AudioSystem.getAudioInputStream(fichierAudio));
	        clip.start();
	    } catch (Exception e) {
	        System.err.println(e.getMessage());
	    }
	}
	public static void ArrowErr() {
		try {
	        File fichierAudio = new File("src/res/ErrorArrow.wav");
	        Clip clip = AudioSystem.getClip();
	        clip.open(AudioSystem.getAudioInputStream(fichierAudio));
	        FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            gainControl.setValue(6.0f); // Réglez ici le niveau sonore (en décibels)
	        clip.start();
	    } catch (Exception e) {
	        System.err.println(e.getMessage());
	    }
	}
	public static void ArrowFire() {
		try {
	        File fichierAudio = new File("src/res/ArrowFire.wav");
	        Clip clip = AudioSystem.getClip();
	        clip.open(AudioSystem.getAudioInputStream(fichierAudio));
	        FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            gainControl.setValue(6.0f); // Réglez ici le niveau sonore (en décibels)
	        clip.start();
	    } catch (Exception e) {
	        System.err.println(e.getMessage());
	    }
	}
	public static void MusiqueAmbiance() {
		 try {
	            File fichierAudio = new File("src/res/music_wumpus.wav");
	            Clip clip = AudioSystem.getClip();
	          
	            
	            clip.open(AudioSystem.getAudioInputStream(fichierAudio));
	            
	            FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
	            gainControl.setValue(-20.0f); // Réglez ici le niveau sonore (en décibels)

	            clip.loop(Clip.LOOP_CONTINUOUSLY); // Joue le son en boucle
	        } catch (Exception e) {
	            System.err.println(e.getMessage());
	        }
	}
    
}



















/*
public static void main(String[] args) {
    
} */