package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.lang.System.Logger;
import java.io.File;
import java.io.IOException;

import entities.Player;
import gamestates.*;
import gen.Plateau;
import levels.LevelManager;
import solve.Agent;
import utilz.HitboxMethods;
import utilz.constants.Projectiles;

public class Game  implements Runnable {
	private GameWindow gameWindow;
	private GamePanel gamePanel;
	private Thread gameThread;
	
	public static final int FPS_SET= 120;
	public static final int UPS_SET = 120;
	
	private Playing playing;
	private Menu menu;
	private Map map;
	private DeathScreen deathScreen;
	private WinScreen winScreen;
	private NewGame newGame;
	private AgentWin agentWin;
	private Regle regle;
	
	
	public Font fontTitre ;
	public Font font ;
	
	
	public final static Color textColor = new Color(234, 134, 133);	
	public final static Color BackGroundColor = new Color(34, 47, 62);
	public final static Color BorderColor = new Color(89, 98, 117);
	public final static Color buttonColor =new Color(119, 140, 163);
	public final static Color buttonHighlightColor =  new Color(250, 211, 144);
	public final static Color inputFieldBackGround =  new Color(200, 214, 229);

	
	public final static int TILES_DEFAULT_SIZE = 60;
	public final static float SCALE = 1f;
	public final static int TILES_IN_WIDTH = 28 ; 
	public final static int TILES_IN_HEIGHT = 16;
	public final static int TILES_SIZE = (int)(TILES_DEFAULT_SIZE * SCALE);
	public final static int GAME_WIDTH = TILES_SIZE * TILES_IN_WIDTH;
	public final static int GAME_HEIGHT = TILES_SIZE * TILES_IN_HEIGHT;
	

	public final static Color buttonTextColor = Game.textColor;
	
	public final static int Buttonwidth = Game.TILES_SIZE*10;
	public final static int buttonnHeight = Game.TILES_SIZE*2;
	public final static int HighlightSize = (int)(Game.TILES_SIZE*0.2);
	public final static int Offset = (int)(Game.TILES_SIZE*2.5);
	
	
	
	public Game() {
		
		
	
		// TEMP
	
		initClasses();
		
		
		
		gamePanel = new GamePanel(this);
		gameWindow = new GameWindow(gamePanel);
		gamePanel.requestFocus();
		
		startGameLoop();
		
	}

	private void initClasses() {
		try {
			File fontFile = new File("src/res/PressStart2P.ttf");
			this.font = Font.createFont(Font.TRUETYPE_FONT,fontFile).deriveFont(Font.PLAIN,Game.TILES_SIZE/3);
			this.fontTitre = Font.createFont(Font.TRUETYPE_FONT, fontFile).deriveFont(Font.BOLD,Game.TILES_SIZE*2/3);
		} catch (FontFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		menu = new Menu(this);
		playing = new Playing(this);
		map = new Map(this);
		deathScreen = new DeathScreen(this);
		winScreen = new WinScreen(this);
		newGame = new NewGame(this);
		agentWin = new AgentWin(this);
		regle = new Regle(this);
	}

	private void startGameLoop() {
		gameThread = new Thread(this);
		gameThread.start();
	}

	public void update() {
		switch(gamestates.state) {
		case MENU:
			menu.update();
			break;
		case PLAYING:
			playing.update();
			break;
		case MAP:
			map.update();
			break;
		case DEATHSCREEN:
			deathScreen.update();
			break;	
		case WINSCREEN:
			winScreen.update();
			break;	
		case NEWGAME:
			newGame.update();
			break;
		case AGENTWIN:
			agentWin.update();
			break;
		case Regle:
			regle.update();
			break;
		default :
			break ;	
		}
		
		
	} 
	
	
	public void render(Graphics g) {
		switch(gamestates.state) {
		case MENU:
			menu.draw(g);
			break;
		case PLAYING:
			playing.draw(g);
			break;
		case MAP:
			map.draw(g);
			break;
		case DEATHSCREEN:
			deathScreen.draw(g);
			break;
		case WINSCREEN:
			winScreen.draw(g);
			break;
		case NEWGAME:
			newGame.draw(g);
			break;
		case AGENTWIN:
			agentWin.draw(g);
			break;
		case Regle:
			regle.draw(g);
			break;
		default :
			break ;	
		}
		
	}
	
	@Override	
	public void run() {
		
		
		double timePerFrame=  1000000000.0/ FPS_SET; // duratrion de chaque frame en nanoseconds*
		double timePerUpdate = 1000000000.0/ UPS_SET; 
		
		long previousTime = System.nanoTime();
		
		int frames = 0;
		int updates = 0;
		long lastCheck = System.currentTimeMillis();
		
		double deltaUpdate = 0; // temps entre chaque update
		double deltaFrame = 0;

		
		
		while(true) {
			
			long currentTime = System.nanoTime(); 
			
			deltaUpdate += (currentTime- previousTime) / timePerUpdate;
			deltaFrame += (currentTime- previousTime) / timePerFrame;
			previousTime = currentTime ;
			
			if(deltaUpdate >= 1) {  //test nouvelle update
				update();
				updates++;
				deltaUpdate--;
			}
			
			if(deltaFrame >=1) {
				gamePanel.repaint();
				frames++;
				deltaFrame--;
			}
		
			
			
			//compteur fps/updates
			if(System.currentTimeMillis() - lastCheck >=1000) {
				lastCheck = System.currentTimeMillis();
				//System.out.println("FPS :" + frames + " | UPS :" + updates );
				frames=0;
				updates = 0;
				}
		}

	}
	public void windowFocusLost() {
		if(gamestates.state == gamestates.PLAYING) {
			playing.getPlayer().resetDirBooleans();
			
		}
	}
	
	
	
	public Menu getMenu() {
		return menu;
	}
	
	public Playing getPlaying() {
		return playing;
	}
	
	public Map getMap() {
		return map;
	}
	
	public DeathScreen getdeathScreen() {
		return deathScreen;
	}
	
	public WinScreen getwinScreen() {
		return winScreen;
	}
	
	public NewGame getNewGame() {
		return newGame;
	}
	
	public GameWindow getGameWindow() {
		return gameWindow;
	}
	
	public GamePanel getGamePanel() {
		return gamePanel;
	}
	public AgentWin getAgentWin() {
		return agentWin;
	}
	public Regle getRegle() {
		return regle;
	}
	
}
