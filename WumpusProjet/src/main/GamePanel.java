package main;

import static main.Game.GAME_HEIGHT;
import static main.Game.GAME_WIDTH;

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

import inputs.KeyboardInputs;
import inputs.MouseInputs; 



public class GamePanel extends JPanel{
	private MouseInputs mouseInputs;
	private Game game;

	public GamePanel(Game game){
		
		mouseInputs = new MouseInputs(this);
		this.game = game;
		setPanelSize();
		addKeyListener(new KeyboardInputs(this));
		addMouseMotionListener(mouseInputs);
		addMouseListener(mouseInputs);
		
	}
	
	private void setPanelSize() {
		Dimension size= new Dimension(GAME_WIDTH,GAME_HEIGHT);
		setMinimumSize(size);
		setPreferredSize(size);
		setMaximumSize(size);
		System.out.println("size = :" + GAME_WIDTH +"|" + GAME_HEIGHT );
		
	}

	public void updateGame() {
		
	}
	
	public void paintComponent(Graphics g) {  // Méthode de JPanel pour dessiner sur la gameWindow
		super.paintComponent(g); // appel de paintCoponent de JPanel 
		game.render(g);
		
		}

	public Game getGame() {
		return game;
	}
	

}
	
