package main;
 
import java.awt.Insets;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;

import javax.swing.JFrame;

public class GameWindow {
	private JFrame jframe;
	
	public GameWindow(GamePanel gamePanel) {  // création de la game window 
		
		jframe = new JFrame();
		
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jframe.add(gamePanel);
		jframe.setLocationRelativeTo(null);
		jframe.setResizable(false);
		jframe.pack(); // Causes this Window to be sized to fit the preferred size and layouts of its subcomponents.
		jframe.setVisible(true);
		jframe.setExtendedState(JFrame.MAXIMIZED_BOTH); 
		Insets insets = jframe.getInsets();
		jframe.setSize(Game.GAME_WIDTH+insets.left+insets.right,Game.GAME_HEIGHT+insets.top+insets.bottom);
		
		jframe.addWindowFocusListener(new WindowFocusListener() {
			
			@Override
			public void windowLostFocus(WindowEvent e) {
				gamePanel.getGame().windowFocusLost();
				
			}
			
			@Override
			public void windowGainedFocus(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
	}
	
	public JFrame getJframe() {
		return jframe;
	}
	
	
}
 