package levels;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import gamestates.Playing;
import main.Game;
import utilz.Blocks;
import utilz.LoadSave;

public class LevelManager {
	
	
	private Playing playing;
	
	private BufferedImage[] levelSprite; 
	private Level CurrentLevel;
	private int[] playerPos;
	
	
	public LevelManager(Playing playing) {
		this.playing = playing;
		playerPos = new int[] {0,0};
		ImportTexturesSprite();
		CurrentLevel = new Level(LoadSave.GetLevelData(playing.getPlateauHandler(),playerPos[0],playerPos[1]));
		
	}
	private void ImportTexturesSprite() { //array de la spritesheet des textures
		BufferedImage img = LoadSave.GetSpriteAtlas(LoadSave.LEVEL_ATLAS);
		levelSprite = new BufferedImage[1056]; 
		for(int j = 0; j<44; j++) { 
			for(int i = 0; i< 24 ; i++) {
				int index = j*24 + i ; 
				levelSprite[index] = img.getSubimage(i*16, j*16, 16, 16); 
			}
		}
	
		
	}
	
	public void refresh(Playing playing) {
		playerPos = new int[] {0,0};
		CurrentLevel = new Level(LoadSave.GetLevelData(playing.getPlateauHandler(),playerPos[0],playerPos[1]));
	}
	
	
	public void draw(Graphics g) {
		
			for(int j=0;j< Game.TILES_IN_HEIGHT;j++) {
				for(int i=0; i<Game.TILES_IN_WIDTH; i++) {
					int index = CurrentLevel.getSpriteIndex(i,j);
					g.drawImage(levelSprite[index],Game.TILES_SIZE*i,Game.TILES_SIZE*j,Game.TILES_SIZE,Game.TILES_SIZE,null);
				}
			}
	}
	public void update() {
		
	}
	
	public Level getCurrentLevel() {
		return CurrentLevel;
	}
	
	public void ChangeMap(int DoorValue) {
		switch (DoorValue){
		case 2: {
			playerPos[0] += -1;
			break;
			}
		case 48: {
			playerPos[1] += -1;
			break;
			}
		
		case 52: {
			playerPos[1] +=1;
			break;
			}
		case 98:{
			playerPos[0] +=1;
			}
		}
		playing.getPlateauHandler().UpdateExplore(playerPos);
		CurrentLevel.setLevelData(LoadSave.GetLevelData(playing.getPlateauHandler(),playerPos[0],playerPos[1]));
	}
	
	public void  updateMap(){
		CurrentLevel.setLevelData(LoadSave.GetLevelData(playing.getPlateauHandler(),playerPos[0],playerPos[1]));
	}
	
	public int[] getPlayerPos() {
		return
		this.playerPos;
		
	}
	
}	
	
