package utilz;

import main.Game;

public class constants {
	
	public static class PlayerConstants{
		public static final int running = 1;
		public static final int idle = 2;
		public static final int flare = 3;
		public static final int shooting = 4;
		public static final int gold =5;
		//go up
		
		public static int GetSpriteAmount(int player_action) {
			
			switch(player_action) {
			
			case idle:
				return 5;
			case flare:
				return 4;
			case running:
				return 3;
			case shooting:
				return 5;
			// case arrow, gold	, go up
			default:
				return 1;
			}
		}
		
		
		
	
}
	public static class Directions{
		public static final int left=0;
		public static final int up=1;
		public static final int right=2;
		public static final int down=3;
		
 }
	public static class Projectiles{
		public static final int Arrow = 0;
		
		public static final int Arrow_width_default = 20;
		public static final int Arrow_height_default = 20;
		
		public static final int Arrow_width = (int)(Arrow_width_default * Game.SCALE);	
		public static final int Arrow_height = (int)(Arrow_height_default * Game.SCALE);
		
		public static int GetSpriteAmount_projectiles(int projectileType) {
			switch (projectileType) {
			case Arrow: {
				
				return 1;
			}
		
				 
		}
	return 0;
	}	
 }	
	
	
}
