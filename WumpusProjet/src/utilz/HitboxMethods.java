package utilz;

import javax.management.ValueExp;

import gamestates.Playing;
import levels.LevelManager;
import main.Game;

public class HitboxMethods {
	private Playing playing;
	
	
	public HitboxMethods(Playing playing) {
		
		this.playing = playing;
	}
	
	public boolean CanMoveHere(float x, float y , float width, float height, int[][] lvlData) {
		
		if(!isSolid(x, y, lvlData)){
			if(!isSolid(x+width, y+height, lvlData)) {
				if(!isSolid(x+width, y, lvlData)) {
					if(!isSolid(x, y+height, lvlData)) {
						return true;
					}
				}
						
			}
		}return false;
	}
	
	private boolean isSolid(float x, float y,int[][] lvlData) {
		if(x<0 || x>= Game.GAME_WIDTH) {
			return true;}
		if(y<0 || y>= Game.GAME_HEIGHT) {
			return true;
			 			}
		float xIndex = x / Game.TILES_SIZE; 
		float yIndex = y/ Game.TILES_SIZE;
		
		//VALUE DE TOUTE LES TILES QUE L'ON PEUT TRAVERSER
		int value = lvlData[(int)yIndex][(int)xIndex];
	
		
		
		
		
		
		if (value >=1000 || value<0 ) { 
			
			return true;
		}
		if (value != 50 && value !=2 &&value !=6 && value!= 26 && value!= 49 && value!= 51 && value!= 74 && value!= 25 && value!= 27 && value!= 73 && value!= 75 && value!= 48 && value!= 52 && value!= 98 ) { 
			
			return true;
		}
	
			
			
			
			
		 return false;
	} 
	public int isOnDoor(float x, float y , int[][] lvlData) {
		
	
			 			
		float xIndex = x / Game.TILES_SIZE; 
		float yIndex = y/ Game.TILES_SIZE;
		
		//VALUE DE TOUTE LES TILES QUE L'ON PEUT TRAVERSER
		int DoorValue = lvlData[(int)yIndex][(int)xIndex];
		
		
		if(DoorValue == 2) {
			playing.levelManager.ChangeMap(DoorValue);
			return 1;
			
		}else if( DoorValue == 48 ) {
			playing.levelManager.ChangeMap(DoorValue);
			return 2;
			
		}else if(DoorValue == 52) {
			playing.levelManager.ChangeMap(DoorValue);
			return 3;
		}else if( DoorValue == 98) {
			playing.levelManager.ChangeMap(DoorValue);
			return 4;
		}
		return -1;
		
		
		
	}
}
