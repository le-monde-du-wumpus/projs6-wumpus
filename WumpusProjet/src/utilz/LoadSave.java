package utilz;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import main.Game;
import main.PlateauHandler;
import utilz.Blocks;


public class LoadSave {
	
	public static final String PLAYER_ATLAS = "perso-spritesheet.png" ; 
	public static final String LEVEL_ATLAS = "textures-spritesheet.png" ; 
	public static final String ARROW_SPRITE = "arrow.png" ;
	public static final String AGENT_SPRITE = "agent.png" ; 
	public static final String GOLD_SPRITE = "gold.png" ;
	public static final String WIND_SPRITE = "wind.png" ;
	public static final String WIND16_SPRITE = "wind16x16.png" ;
	public static final String ODEUR_SPRITE = "Odeur.png" ;
	
	
	public static BufferedImage GetSpriteAtlas(String filename) {
		BufferedImage img = null;
		
		InputStream is = LoadSave.class.getResourceAsStream("/res/" + filename );
		
		try {
			
			 img = ImageIO.read(is);
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return img;
		
	}
	
	public static int[][] GetLevelData(PlateauHandler plateauHandler , int x , int y){
		
		final int background = 101;
		final int cave_floor_default = 50;
		
		final int cave_floor_top=26;
		final int cave_floor_left=49;
		final int cave_floor_right=51;
		final int cave_floor_bot=74;
		
		final int cave_floor_corner_top_left= 25;
		final int cave_floor_corner_top_right= 27;
		final int cave_floor_corner_bot_left= 73;
		final int cave_floor_corner_bot_right= 75;
		
		final int door_top = 2;
		final int door_left = 48;
		final int door_right = 52;
		final int door_bot = 98;
		
		final int wall_top = 1;
		final int wall_left = 24;
		final int wall_right = 28;
		final int wall_bot = 97;
		
		final int corner_top_left=0;
		final int corner_top_right=4;
		final int corner_bot_left=96;
		final int corner_bot_right=100;
	
		final int or = 51;
		
		final int map_inexplorée = 5 ;
		final int map_vide =6;
		final int map_brise = 7; 
		final int map_odeur = 8;
		final int map_or =9;
		final int map_pit =10;
		final int map_wumpus_mort=11;
		final int map_or_brise=12;
		final int map_or_odeur=13;
		final int map_wumpus=14;
		final int map_horsmap=15;
		
		
		int[][] lvlData = new int[Game.TILES_IN_HEIGHT][Game.TILES_IN_WIDTH];
		
		
		//A remplacer par une fonction qui nous donne un double array lorsque on a une nouvelle map
		
		
		/*int[][] lvlTemp = {
				{647,647,647,647,28,647,647,647,647,647,647,647,647,647,647},
				{647,647,647,28,4,28,647,647,647,647,647,647,647,647,647},
				{647,647,28,4,4,4,28,647,647,647,647,647,647,647,647},
				{647,647,28,4,121,4,28,647,647,647,647,647,647,647,647},
				{647,647,28,4,145,4,28,647,647,647,647,647,647,647,647},
				{647,647,24,24,24,24,24,647,647,647,647,647,647,647,647},
				{3,3,3,3,3,3,3,3,3,3,3,3,3,3,3},
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				
		};*/
		Blocks[][] CurrentLvl = plateauHandler.levelMatrice(x, y);
		
		//System.out.println(CurrentLvl[1][2]);
		
		
		for(int j =0; j <CurrentLvl.length ;j++ ) {
			for(int i=0; i < CurrentLvl[0].length; i++) {
				
				
				
				int value;
				//System.out.println(CurrentLvl[j][i]);
				switch(CurrentLvl[j][i]) {
					default:
						value = background;
						break;
					case BACKGROUND:
						value = background;
						break;
						
					//DOORS	
					case DOOR_TOP:
						value = door_top;
						break;
					case DOOR_RIGHT:
						value = door_right;
						break;
					case DOOR_LEFT:
						value = door_left;
						break;	
					case DOOR_BOT:
						value = door_bot;
						break;	
					//WALLS	
					case WALL_TOP:
						value = wall_top;
						break;
					case WALL_BOT:
						value = wall_bot;
						break;
					case WALL_LEFT:	
						value = wall_left;
						break;
					case WALL_RIGHT:
						value = wall_right;
						break;
					//CAVE FLOOR
					case CAVE_FLOOR_CORNER_TOP_LEFT:
						value = cave_floor_corner_top_left;
						break;
					case CAVE_FLOOR_CORNER_TOP_RIGHT:
						value = cave_floor_corner_top_right;
						break;
					case CAVE_FLOOR_CORNER_BOT_LEFT:
						value = cave_floor_corner_bot_left;
						break;
					case CAVE_FLOOR_CORNER_BOT_RIGHT:
						value = cave_floor_corner_bot_right;
						break;
					case CAVE_FLOOR_DEFAULT:
						value = cave_floor_default;
						break;	
					case CAVE_FLOOR_LEFT:
						value = cave_floor_left;
						break;
					case CAVE_FLOOR_RIGHT:
						value = cave_floor_right;
						break;	
					case CAVE_FLOOR_TOP:
						value = cave_floor_top;
						break;
					case CAVE_FLOOR_BOT:
						value = cave_floor_bot;
						break;	
					//CORNER	
					case CORNER_TOP_LEFT:
						value = corner_top_left;
						break;
					case CORNER_TOP_RIGHT:
						value = corner_top_right;
						break;	
					case CORNER_BOT_LEFT:
						value = corner_bot_left;
						break;	
					case CORNER_BOT_RIGHT:
						value = corner_bot_right;
						break;	
					
			
					case OR:
						value = or;
						break;
					
				}
				
				if(value>=1058){  //Nb de sprite de texture différent 
						value=0;
				}
				
				lvlData[j][i] =value;
				 
			}
		}
		
		applyMap(lvlData,plateauHandler.minicarte(x, y));
		return lvlData;
		
	}
	
	private static void applyMap(int[][] lvlData,int[][] map) {		
		int[] Corner = {2,22};	
		
		for(int i = 0 ; i<3;i++) {
			for(int j = 0 ; j<3 ; j++) {
				//System.out.println(map[i][j]);
				switch(map[i][j]) {
					default:
						break;
					case -1: // case inexplorée 
						lvlData[Corner[0]+i][Corner[1]+j] = 5;
						break;
					case -2: // case hors matrice 
						lvlData[Corner[0]+i][Corner[1]+j] = 15;
						break;	
					case 0: // case vide et explorée
						lvlData[Corner[0]+i][Corner[1]+j] = 6;
						break;
					case 3: // case wumpus
						lvlData[Corner[0]+i][Corner[1]+j] = 14;
						break;	
					case 5: // case Brise et explorée
						lvlData[Corner[0]+i][Corner[1]+j] = 7;
						break;
					case 6: // case Odeur et explorée
						lvlData[Corner[0]+i][Corner[1]+j] = 8;
						break;
					case 2: // case Or et explorée
						lvlData[Corner[0]+i][Corner[1]+j] = 9;
						break;
					case 7: // case odeur+brise
						lvlData[Corner[0]+i][Corner[1]+j] = 16;
						break;
					case 8: // caseWumpus mort et explorée
						lvlData[Corner[0]+i][Corner[1]+j] = 11;
						break;
					case 9: // case Or + Brise et explorée
						lvlData[Corner[0]+i][Corner[1]+j] = 12;
						break;
					case 4: // trou
						lvlData[Corner[0]+i][Corner[1]+j] = 10;
						break;	
					case 10: // case Or + Odeuret explorée
						lvlData[Corner[0]+i][Corner[1]+j] = 13;
						break;
					case 11: // green check
						lvlData[Corner[0]+i][Corner[1]+j] = 17;
						break;
					case 12: // red Cross
						lvlData[Corner[0]+i][Corner[1]+j] = 18;
						break;	
				}
			}
		}
		
	}
		
		
}

