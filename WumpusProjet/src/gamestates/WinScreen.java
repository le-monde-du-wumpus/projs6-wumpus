package gamestates;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import entities.Player;
import main.Game;
import utilz.HitboxMethods;
import utilz.LoadSave;

public class WinScreen extends State  {
	private int x,y;
	private BufferedImage[] levelSprite; 
	
	public HitboxMethods hitboxMethods;
	private int[][] MapValue;
	private Playing playing;
	private boolean ExitSelected;
	
	public WinScreen(Game game) {
		super(game);
		this.x = game.getPlaying().getPlateauHandler().getPlateau().getX();
		this.y = game.getPlaying().getPlateauHandler().getPlateau().getY();
		ImportTexturesSprite();
		MapValue = generateMapValue(game.getPlaying());
		
		this.playing = game.getPlaying();
		
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}
	
	public void refresh(Playing playing) {
			
			this.x = playing.getPlateauHandler().getPlateau().getX();
			this.y = playing.getPlateauHandler().getPlateau().getY();
			this.MapValue = generateMapValue(playing);
			
		}
		
	private void ImportTexturesSprite() { //array de la spritesheet des textures
		BufferedImage img = LoadSave.GetSpriteAtlas(LoadSave.LEVEL_ATLAS);
		levelSprite = new BufferedImage[1056]; 
		for(int j = 0; j<44; j++) { 
			for(int i = 0; i< 24 ; i++) {
				int index = j*24 + i ; 
				levelSprite[index] = img.getSubimage(i*16, j*16, 16, 16); 
			}
		}	
	}

	@Override
	public void draw(Graphics g) {
		int tailleX = Game.GAME_WIDTH, tailleY = Game.GAME_HEIGHT;
		
		int size =  Math.min(tailleX/(y*2),tailleY/(x*2));
		int offsetX=(int)(tailleX/(2.0)-(y/2.0)*(size+3)),offsetY = (int)(tailleY/(2.0)-(x/2.0)*(size+3));
		g.setColor(Game.BackGroundColor);
		g.fillRect(0, 0, tailleX, tailleY);
		
		
		 for (int i = 0; i < x; i++) {
	            for (int j = 0; j < y; j++) { 
	        
	     g.setColor(Color.GRAY);
       
        	
	     g.drawImage(levelSprite[MapValue[i][j]],(size+3)*j+ offsetX,(size+3)*i + offsetY,(size),(size),null);
	     
      
       
	   } 
	  }	
		 if(this.ExitSelected) {		
				g.setColor(Game.buttonHighlightColor);	
				g.fillRect((int)(Game.TILES_SIZE)-(int)(Game.HighlightSize/2.0),(int)(Game.TILES_SIZE*1) -(int)(Game.HighlightSize/2.0), Game.TILES_SIZE*3+Game.HighlightSize, Game.TILES_SIZE+Game.HighlightSize);
				g.setColor(Color.black);	
				g.fillRect((int)(Game.TILES_SIZE),(int)(Game.TILES_SIZE*1) , Game.TILES_SIZE*3, Game.TILES_SIZE);
			}else {
				g.setColor(Color.black);	
				g.fillRect((int)(Game.TILES_SIZE),(int)(Game.TILES_SIZE*1) , Game.TILES_SIZE*3, Game.TILES_SIZE);
			}
			
			g.setColor(Color.white);
			g.setFont(super.game.fontTitre.deriveFont((float) (Game.TILES_SIZE*0.40)));
		 String Echapkey = "Echap";
		FontMetrics fm = g.getFontMetrics();
		int prefSizeWidth = fm.stringWidth(Echapkey);
			int prefSizeHeight = fm.getHeight();		 
			g.drawString(Echapkey, (int)(Game.TILES_SIZE*2.5) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*1.5) +  (int)(prefSizeHeight*0.5));
			
			String Or = "Retour au Menu";
			g.setFont(super.game.fontTitre.deriveFont((float) (Game.TILES_SIZE/3.5)));
			g.setColor(Color.white);
			fm = g.getFontMetrics();
			prefSizeWidth = fm.stringWidth(Or);
			prefSizeHeight = fm.getHeight();		
			g.drawString(Or, (int)(Game.TILES_SIZE*2.5) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*0.5) +  (int)(prefSizeHeight*0.5));
				
		 
		 	String titre = "Vous avez gagné";
			g.setColor(new Color(255,215,0));
			g.setFont(super.game.fontTitre.deriveFont(Game.TILES_SIZE*2/3));
			 fm = g.getFontMetrics();
			int prefSize = fm.stringWidth(titre);
			g.drawString(titre, Game.GAME_WIDTH/2 - (int)(prefSize*0.5), (int)(Game.TILES_SIZE*2.5));
			
			
			String Seed = "seed :" + playing.getPlateauHandler().getPlateau().getSeed();
			g.setColor(Game.textColor);
			g.setFont(super.game.fontTitre.deriveFont(Game.TILES_SIZE*2/3));
			fm = g.getFontMetrics();
			prefSize = fm.stringWidth(Seed);
			g.drawString(Seed, Game.GAME_WIDTH/2 - (int)(prefSize*0.5), Game.GAME_HEIGHT - Game.TILES_SIZE);
			
			String Score = "Score :" + playing.getScore();
			g.setColor(Game.textColor);
			g.setFont(super.game.fontTitre);
			fm = g.getFontMetrics();
			prefSize = fm.stringWidth(Score);
			g.drawString(Score, Game.GAME_WIDTH/2 - (int)(prefSize*0.5), Game.GAME_HEIGHT - Game.TILES_SIZE*2);
			
			playing.getPlayer().render(g);
			
			if(playing.Enableagent) {
				playing.EAgent.render(g);
			}
		
	}
	
	private int[][] generateMapValue(Playing playing){
		int[][] mapValue = new int[x][y];
		for(int i = 0; i<x; i++) {
			for(int j = 0; j<y; j++) {
				
				switch(playing.getPlateauHandler().getPlateau().getMatrice()[i][j]) {
				default:
					break;
				case -1: // case inexplorée 
					mapValue[i][j]  = 5;
					break;
				case -2: // case hors matrice 
					mapValue[i][j] = 15;
					break;	
				case 0: // case vide et explorée
					mapValue[i][j] = 6;
					break;
				case 3: // case wumpus
					mapValue[i][j] = 14;
					break;	
				case 5: // case Brise et explorée
					mapValue[i][j] = 7;
					break;
				case 6: // case Odeur et explorée
					mapValue[i][j] = 8;
					break;
				case 2: // case Or et explorée
					mapValue[i][j]= 9;
					break;
				case 7: // case odeur+brise
					mapValue[i][j] = 16;
					break;
				case 8: // caseWumpus mort et explorée
					mapValue[i][j] = 11;
					break;
				case 9: // case Or + Brise et explorée
					mapValue[i][j] = 12;
					break;
				case 4: // trou
					mapValue[i][j] = 10;
					break;	
				case 10: // case Or + Odeuret explorée
					mapValue[i][j] = 13;
					break;
					
			 }
			}
		}
		return mapValue;
	}

	@Override
	public void MouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void MousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void MouseReleased(MouseEvent e) {
		int CornerX0 = Game.TILES_SIZE;
		int SizeX = Game.TILES_SIZE*3;
		int CornerX1 = CornerX0 + SizeX;
				
		int CornerY0 = Game.TILES_SIZE;
		int SizeY = Game.TILES_SIZE;
		int CornerY1 = CornerY0 + SizeY;
				
		if(e.getX() >= CornerX0 && e.getX() <= CornerX1 && e.getY() >= CornerY0 && e.getY() <= CornerY1 ) {
			gamestates.state = gamestates.MENU;
		}
		
	}

	@Override
	public void MouseMouved(MouseEvent e) {
		int CornerX0 = Game.TILES_SIZE;
		int SizeX = Game.TILES_SIZE*3;
		int CornerX1 = CornerX0 + SizeX;
				
		int CornerY0 = Game.TILES_SIZE;
		int SizeY = Game.TILES_SIZE;
		int CornerY1 = CornerY0 + SizeY;
				
		if(e.getX() >= CornerX0 && e.getX() <= CornerX1 && e.getY() >= CornerY0 && e.getY() <= CornerY1 ) {
			this.ExitSelected = true;
		}else {
			this.ExitSelected = false;
		}
		
	}

	@Override
	public void KeyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
			gamestates.state = gamestates.MENU;
		}
	}

	@Override
	public void KeyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
