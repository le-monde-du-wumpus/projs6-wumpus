package gamestates;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import gamestates.gamestates;
import main.Game;
import main.PlateauHandler;
import utilz.LoadSave;

public class Regle extends State {
	
	private BufferedImage Odeur;
	private BufferedImage Brise;
	private BufferedImage Or;
	private BufferedImage Wumpus;
	private BufferedImage WumpusMort;
	private BufferedImage Safe;
	private BufferedImage NotSafe;
	private BufferedImage Agent;
	
	private boolean ExitSelected;
	
	
	private enum regle{
		Menu,
		Monde,
		Diff,
		Agent;
	}
	
	private int selectedButton;
	private regle etat;
	
	
	public Regle(Game game) {
		super(game);
		this.selectedButton = 0;
		this.etat = regle.Menu;
		this.ImportTexturesSprite();
		ExitSelected = false;
	}
	
	private void initFrame() {
		
	}
	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}
	private void ImportTexturesSprite() { //array de la spritesheet des textures
		BufferedImage img = LoadSave.GetSpriteAtlas(LoadSave.LEVEL_ATLAS);
			Odeur= img.getSubimage(8*16, 0*16, 16, 16); 
			Brise= img.getSubimage(7*16, 0*16, 16, 16); 
			Or= img.getSubimage(9*16, 0*16, 16, 16); 
			Wumpus= img.getSubimage(14*16, 0*16, 16, 16); 
			WumpusMort = img.getSubimage(11*16, 0*16, 16, 16); 
			Safe = img.getSubimage(17*16, 0*16, 16, 16); 
			NotSafe = img.getSubimage(18*16, 0*16, 16, 16); 
			Agent =  LoadSave.GetSpriteAtlas(LoadSave.AGENT_SPRITE);
		}
	
		

	@Override
	public void draw(Graphics g) {

		
		switch(this.etat) {
			default:
				g.setColor(Game.BackGroundColor);
				g.fillRect(0, 0,  Game.GAME_WIDTH,Game.GAME_HEIGHT);
				
				
				String titre = "Aides";
				g.setColor(Game.textColor);
				g.setFont(super.game.fontTitre);
				FontMetrics fm = g.getFontMetrics();
				int prefSize = fm.stringWidth(titre);
				g.drawString(titre, Game.GAME_WIDTH/2 - (int)(prefSize*0.5), Game.TILES_SIZE*3);
				
				
				
				g.setColor(Game.buttonHighlightColor);
				switch(this.selectedButton) {
					default:
						break;
					case 0:
						// jouer est select
						g.fillRect(Game.GAME_WIDTH/2 - (int)(Game.Buttonwidth*0.5) - (int)(Game.HighlightSize*0.5), (int)(Game.GAME_HEIGHT/3.5) - (int)(Game.buttonnHeight*0.5) - (int)(Game.HighlightSize*0.5),Game.Buttonwidth+Game.HighlightSize, Game.buttonnHeight+Game.HighlightSize);
						break;
					case 1:
						// jouer est select
						g.fillRect(Game.GAME_WIDTH/2 - (int)(Game.Buttonwidth*0.5) - (int)(Game.HighlightSize*0.5), (int)(Game.GAME_HEIGHT/3.5) - (int)(Game.buttonnHeight*0.5) - (int)(Game.HighlightSize*0.5)+Game.Offset,Game.Buttonwidth+Game.HighlightSize, Game.buttonnHeight+Game.HighlightSize);
						break;
					case 2:
						// jouer est select
						g.fillRect(Game.GAME_WIDTH/2 - (int)(Game.Buttonwidth*0.5) - (int)(Game.HighlightSize*0.5), (int)(Game.GAME_HEIGHT/3.5) - (int)(Game.buttonnHeight*0.5) - (int)(Game.HighlightSize*0.5)+2*Game.Offset,Game.Buttonwidth+Game.HighlightSize, Game.buttonnHeight+Game.HighlightSize);
						break;
					case 3:
						// jouer est select
						g.fillRect(Game.GAME_WIDTH/2 - (int)(Game.Buttonwidth*0.5) - (int)(Game.HighlightSize*0.5), (int)(Game.GAME_HEIGHT/3.5) - (int)(Game.buttonnHeight*0.5) - (int)(Game.HighlightSize*0.5)+3*Game.Offset,Game.Buttonwidth+Game.HighlightSize, Game.buttonnHeight+Game.HighlightSize);
						break;
				}
				
				
				
				g.setColor(Game.buttonColor);
				g.fillRect(Game.GAME_WIDTH/2 - (int)(Game.Buttonwidth*0.5),  (int)(Game.GAME_HEIGHT/3.5) - (int)(Game.buttonnHeight*0.5),Game.Buttonwidth, Game.buttonnHeight);
				
				String sousTitre = "Regles du Jeu";
				g.setFont(super.game.font);
				fm = g.getFontMetrics();
				prefSize = fm.stringWidth(sousTitre);
				int stringHeight = fm.getHeight();
				g.setColor(Game.buttonTextColor);
				g.drawString(sousTitre, Game.GAME_WIDTH/2 - (int)(prefSize*0.5),(int)(Game.GAME_HEIGHT/3.5) +(int)(0.5*stringHeight));
				
				

				g.setColor(Game.buttonColor);
				g.fillRect(Game.GAME_WIDTH/2 - (int)(Game.Buttonwidth*0.5), (int)(Game.GAME_HEIGHT/3.5) - (int)(Game.buttonnHeight*0.5)+Game.Offset,Game.Buttonwidth, Game.buttonnHeight);
				
				sousTitre = "Difficultées";
				g.setFont(super.game.font);
				fm = g.getFontMetrics();
				prefSize = fm.stringWidth(sousTitre);
				stringHeight = fm.getHeight();
				g.setColor(Game.buttonTextColor);
				g.drawString(sousTitre, Game.GAME_WIDTH/2 - (int)(prefSize*0.5), (int)(Game.GAME_HEIGHT/3.5)+(int)(0.5*stringHeight) +Game.Offset);
				

				g.setColor(Game.buttonColor);
				g.fillRect(Game.GAME_WIDTH/2 - (int)(Game.Buttonwidth*0.5), (int)(Game.GAME_HEIGHT/3.5) - (int)(Game.buttonnHeight*0.5)+2*Game.Offset,Game.Buttonwidth, Game.buttonnHeight);
				
				sousTitre = "Options de l'Agent";
				g.setFont(super.game.font);
				fm = g.getFontMetrics();
				prefSize = fm.stringWidth(sousTitre);
				stringHeight = fm.getHeight();
				g.setColor(Game.buttonTextColor);
				g.drawString(sousTitre, Game.GAME_WIDTH/2 - (int)(prefSize*0.5), (int)(Game.GAME_HEIGHT/3.5)+(int)(0.5*stringHeight) + 2*Game.Offset);
				

				g.setColor(Game.buttonColor);
				g.fillRect(Game.GAME_WIDTH/2 - (int)(Game.Buttonwidth*0.5), (int)(Game.GAME_HEIGHT/3.5) - (int)(Game.buttonnHeight*0.5)+3*Game.Offset,Game.Buttonwidth, Game.buttonnHeight);
				
				sousTitre = "Retour au menu principal";
				g.setFont(super.game.font);
				fm = g.getFontMetrics();
				prefSize = fm.stringWidth(sousTitre);
				stringHeight = fm.getHeight();
				g.setColor(Game.buttonTextColor);
				g.drawString(sousTitre, Game.GAME_WIDTH/2 - (int)(prefSize*0.5), (int)(Game.GAME_HEIGHT/3.5)+(int)(0.5*stringHeight) + 3*Game.Offset);
				
				
				

				break;
			case Monde:
				g.setColor(Game.BackGroundColor);
				g.fillRect(0, 0,  Game.GAME_WIDTH,Game.GAME_HEIGHT);
				
				if(this.ExitSelected) {		
					g.setColor(Game.buttonHighlightColor);	
					g.fillRect((int)(Game.TILES_SIZE)-(int)(Game.HighlightSize/2.0),(int)(Game.TILES_SIZE*1) -(int)(Game.HighlightSize/2.0), Game.TILES_SIZE*3+Game.HighlightSize, Game.TILES_SIZE+Game.HighlightSize);
					g.setColor(Color.black);	
					g.fillRect((int)(Game.TILES_SIZE),(int)(Game.TILES_SIZE*1) , Game.TILES_SIZE*3, Game.TILES_SIZE);
				}else {
					g.setColor(Color.black);	
					g.fillRect((int)(Game.TILES_SIZE),(int)(Game.TILES_SIZE*1) , Game.TILES_SIZE*3, Game.TILES_SIZE);
				}
				
				g.setColor(Color.white);
				g.setFont(super.game.fontTitre.deriveFont((float) (Game.TILES_SIZE*0.40)));
				
				String Echapkey = "Echap";
				fm = g.getFontMetrics();
				int prefSizeWidth = fm.stringWidth(Echapkey);
				int prefSizeHeight = fm.getHeight();		 
				g.drawString(Echapkey, (int)(Game.TILES_SIZE*2.5) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*1.5) +  (int)(prefSizeHeight*0.5));
				
				String Titre = "Retour au Menu";
				g.setFont(super.game.fontTitre.deriveFont((float) (Game.TILES_SIZE/3.5)));
				g.setColor(Color.white);
				fm = g.getFontMetrics();
				prefSizeWidth = fm.stringWidth(Titre);
				prefSizeHeight = fm.getHeight();		
				g.drawString(Titre, (int)(Game.TILES_SIZE*2.5) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*0.5) +  (int)(prefSizeHeight*0.5));
				
				titre = "Regles du Jeu";
				g.setColor(Game.textColor);
				g.setFont(super.game.fontTitre);
				fm = g.getFontMetrics();
				prefSize = fm.stringWidth(titre);
				g.drawString(titre, Game.GAME_WIDTH/2 - (int)(prefSize*0.5), Game.TILES_SIZE*3);
				
				//g.drawImage(this.Odeur,Game.TILES_SIZE*5,Game.TILES_SIZE*5,Game.TILES_SIZE,Game.TILES_SIZE,null);

				
				String Text = "Vous êtes à la recherche d'or   dans une cave jonchée de trous et gardée par un monstre,\r\n le Wumpus.\r\n Votre objectif est de trouver tout l'or de la cave en évitant les dangers !\r\n Rencontrer le Wumpus ou tomber dans un trou signifie une mort certaine.\r\n Pour vous aider dans votre tâche,\r\n vous disposez de différents outils : une carte, des flèches et votre perception.\r\n"
						+ "\r\n"
						+ "Les caves peuvent avoir une taille allant de 3x3 à 100x100.\r\nChaque cave possède une seed unique.\r\n C'est le joueur qui décide de la taille et de la seed s'il le souhaite.\r\n"
						+ "\r\n"
						+ "Le nombre d'or, de Wumpus et de flèches\r\n dépendent de la taille de la cave dans laquelle vous vous trouvez.\r\n"
						+ "\r\n"
						+ "Chaque case adjacente à un danger peut être perçue par un indicateur caractéristique :\r\n une brise   indique la présence d'un trou \r\net une puanteur   révèle la présence d'un Wumpus  .\r\n Servez-vous de ces indices pour vous diriger sans danger dans la cave.\r\n"
						+ "\r\n"
						+ "Le joueur se déplace entre les salles par des portes. \r\n Il ne voit que la salle dans laquelle il se trouve.\r\n Cependant, il perçoit les différents indicateurs\r\n qui l'alertent qu'une case dangereuse est à proximité.\r\n"
						+ "\r\n"
						+ "Si vous pensez savoir où se trouve le Wumpus,\r\n vous pourrez lui tirer une flèche pour l'éliminer.\r\n Vous disposez d'autant de flèches qu'il y a de Wumpus dans la cave.\r\n Si vous parvenez à éliminer un Wumpus  ,\r\n vous entendrez la bête agoniser, ce qui confirmera sa mort. ";
				g.setFont(super.game.font.deriveFont(Font.PLAIN,(int)(Game.TILES_SIZE*0.3)));
				fm = g.getFontMetrics();

				String[] lines = Text.split("\r\n");
				int LineOffSet = 25;
				for(int i =0;i < lines.length;i++) {
					String currentLine = lines[i];
					prefSizeWidth = fm.stringWidth(currentLine);
					prefSizeHeight = fm.getHeight();
					g.drawString(currentLine, Game.GAME_WIDTH/2 -  (int)(prefSizeWidth*0.5), Game.GAME_HEIGHT/4 - (int)(prefSizeHeight*0.5) + i * LineOffSet);
				}
				g.drawImage(Or,584,211,(int)(Game.TILES_SIZE*0.4),(int)(Game.TILES_SIZE*0.4),null);
				g.drawImage(Brise,648,585,(int)(Game.TILES_SIZE*0.4),(int)(Game.TILES_SIZE*0.4),null);
				g.drawImage(Odeur,665,610,(int)(Game.TILES_SIZE*0.4),(int)(Game.TILES_SIZE*0.4),null);
				g.drawImage(Wumpus,1252,610,(int)(Game.TILES_SIZE*0.4),(int)(Game.TILES_SIZE*0.4),null);
				g.drawImage(WumpusMort,1160,885,(int)(Game.TILES_SIZE*0.4),(int)(Game.TILES_SIZE*0.4),null);
				break;
			case Diff:
				g.setColor(Game.BackGroundColor);
				g.fillRect(0, 0,  Game.GAME_WIDTH,Game.GAME_HEIGHT);
				
				if(this.ExitSelected) {		
					g.setColor(Game.buttonHighlightColor);	
					g.fillRect((int)(Game.TILES_SIZE)-(int)(Game.HighlightSize/2.0),(int)(Game.TILES_SIZE*1) -(int)(Game.HighlightSize/2.0), Game.TILES_SIZE*3+Game.HighlightSize, Game.TILES_SIZE+Game.HighlightSize);
					g.setColor(Color.black);	
					g.fillRect((int)(Game.TILES_SIZE),(int)(Game.TILES_SIZE*1) , Game.TILES_SIZE*3, Game.TILES_SIZE);
				}else {
					g.setColor(Color.black);	
					g.fillRect((int)(Game.TILES_SIZE),(int)(Game.TILES_SIZE*1) , Game.TILES_SIZE*3, Game.TILES_SIZE);
				}
				
				g.setColor(Color.white);
				g.setFont(super.game.fontTitre.deriveFont((float) (Game.TILES_SIZE*0.40)));
				
				Echapkey = "Echap";
				fm = g.getFontMetrics();
				prefSizeWidth = fm.stringWidth(Echapkey);
				prefSizeHeight = fm.getHeight();		 
				g.drawString(Echapkey, (int)(Game.TILES_SIZE*2.5) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*1.5) +  (int)(prefSizeHeight*0.5));
				
				Titre = "Retour au Menu";
				g.setFont(super.game.fontTitre.deriveFont((float) (Game.TILES_SIZE/3.5)));
				g.setColor(Color.white);
				fm = g.getFontMetrics();
				prefSizeWidth = fm.stringWidth(Titre);
				prefSizeHeight = fm.getHeight();		
				g.drawString(Titre, (int)(Game.TILES_SIZE*2.5) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*0.5) +  (int)(prefSizeHeight*0.5));
				
				titre = "Difficultées";
				g.setColor(Game.textColor);
				g.setFont(super.game.fontTitre);
				fm = g.getFontMetrics();
				prefSize = fm.stringWidth(titre);
				g.drawString(titre, Game.GAME_WIDTH/2 - (int)(prefSize*0.5), Game.TILES_SIZE*3);
				
				Text = "Le jeu dispose de différents modes de difficulté :\r\n"
						+ "\r\n"
						+ "Triche : Le joueur dispose de toutes les indications, la carte est entièrement révélée.\r\n Le joueur connaît donc les emplacements de tous les ors ainsi que de tous les dangers.\r\n"
						+ "\r\n"
						+ "Facile : Le joueur dispose d'une aide qui lui indique quelle case est sûre  \r\net quelle case ne l'est pas   au cours de son exploration.\r\n"
						+ "\r\n"
						+ "Moyen : Le joueur ne dispose d'aucune aide,\r\n sa carte se révèle au fur et à mesure qu'il l'explore.\r\n"
						+ "\r\n"
						+ "Difficile : Le joueur ne dispose pas de carte.\r\n Il devra se souvenir lui-même de tout ce qu'il aura découvert. ";
				g.setFont(super.game.font.deriveFont(Font.PLAIN,(int)(Game.TILES_SIZE*0.3)));
				fm = g.getFontMetrics();

				lines = Text.split("\r\n");
				LineOffSet = 25;
				for(int i =0;i < lines.length;i++) {
					String currentLine = lines[i];
					prefSizeWidth = fm.stringWidth(currentLine);
					prefSizeHeight = fm.getHeight();
					g.drawString(currentLine, Game.GAME_WIDTH/2 -  (int)(prefSizeWidth*0.5), Game.GAME_HEIGHT/4 - (int)(prefSizeHeight*0.5) + i * LineOffSet);
				}
				
				g.drawImage(Safe,1495,335,(int)(Game.TILES_SIZE*0.4),(int)(Game.TILES_SIZE*0.4),null);
				g.drawImage(NotSafe,817,361,(int)(Game.TILES_SIZE*0.4),(int)(Game.TILES_SIZE*0.4),null);
				break;
				
			case Agent:
				g.setColor(Game.BackGroundColor);
				g.fillRect(0, 0,  Game.GAME_WIDTH,Game.GAME_HEIGHT);
				if(this.ExitSelected) {		
					g.setColor(Game.buttonHighlightColor);	
					g.fillRect((int)(Game.TILES_SIZE)-(int)(Game.HighlightSize/2.0),(int)(Game.TILES_SIZE*1) -(int)(Game.HighlightSize/2.0), Game.TILES_SIZE*3+Game.HighlightSize, Game.TILES_SIZE+Game.HighlightSize);
					g.setColor(Color.black);	
					g.fillRect((int)(Game.TILES_SIZE),(int)(Game.TILES_SIZE*1) , Game.TILES_SIZE*3, Game.TILES_SIZE);
				}else {
					g.setColor(Color.black);	
					g.fillRect((int)(Game.TILES_SIZE),(int)(Game.TILES_SIZE*1) , Game.TILES_SIZE*3, Game.TILES_SIZE);
				}
				
				g.setColor(Color.white);
				g.setFont(super.game.fontTitre.deriveFont((float) (Game.TILES_SIZE*0.40)));
				
				Echapkey = "Echap";
				fm = g.getFontMetrics();
				prefSizeWidth = fm.stringWidth(Echapkey);
				prefSizeHeight = fm.getHeight();		 
				g.drawString(Echapkey, (int)(Game.TILES_SIZE*2.5) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*1.5) +  (int)(prefSizeHeight*0.5));
				
				Titre = "Retour au Menu";
				g.setFont(super.game.fontTitre.deriveFont((float) (Game.TILES_SIZE/3.5)));
				g.setColor(Color.white);
				fm = g.getFontMetrics();
				prefSizeWidth = fm.stringWidth(Titre);
				prefSizeHeight = fm.getHeight();		
				g.drawString(Titre, (int)(Game.TILES_SIZE*2.5) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*0.5) +  (int)(prefSizeHeight*0.5));
				
				titre = "Options de l'Agent";
				g.setColor(Game.textColor);
				g.setFont(super.game.fontTitre);
				fm = g.getFontMetrics();
				prefSize = fm.stringWidth(titre);
				g.drawString(titre, Game.GAME_WIDTH/2 - (int)(prefSize*0.5), Game.TILES_SIZE*3);
				

				Text = "Il est possible de jouer contre un agent si vous le souhaitez !\r\n Il essaiera de vous battre dans la cave et de trouver les ors avant vous !\r\n"
						+ "Si activé, l'agent est visible dans les salles et\r\n sur la carte à tout moment, quel que soit le niveau de difficulté.\r\n"
						+ "L'agent dispose lui-même de différents niveaux de difficulté :\r\n"
						+ "\r\n"
						+ "Facile : L'agent joue et se déplace de salle en salle en même temps que le joueur.\r\n"
						+ "\r\n"
						+ "Moyen : L'agent jouera toutes les 8 secondes, indépendamment des actions du joueur.\r\n"
						+ "\r\n"
						+ "Difficile : L'agent jouera toutes les 4 secondes, indépendamment des actions du joueur.\r\n"
						+ "\r\n"
						+ "Si l'agent trouve tous les ors, vous perdez.";
				g.setFont(super.game.font.deriveFont(Font.PLAIN,(int)(Game.TILES_SIZE*0.3)));
				fm = g.getFontMetrics();

				lines = Text.split("\r\n");
				LineOffSet = 25;
				for(int i =0;i < lines.length;i++) {
					String currentLine = lines[i];
					prefSizeWidth = fm.stringWidth(currentLine);
					prefSizeHeight = fm.getHeight();
					g.drawString(currentLine, Game.GAME_WIDTH/2 -  (int)(prefSizeWidth*0.5), Game.GAME_HEIGHT/4 - (int)(prefSizeHeight*0.5) + i * LineOffSet);
				}
				
				g.drawImage(Agent,(int)(Game.GAME_WIDTH/2) - Game.TILES_SIZE,(int)(Game.GAME_HEIGHT/2) + Game.TILES_SIZE*2,(int)(Game.TILES_SIZE*2),(int)(Game.TILES_SIZE*4),null);
				
				break;
		}
		
	}

	@Override
	public void MouseClicked(MouseEvent e) {

		
	}

	@Override
	public void MousePressed(MouseEvent e) {
		//g.fillRect(Game.GAME_WIDTH/2 - (int)(Game.Buttonwidth*0.5),  (int)(Game.GAME_HEIGHT/3.5) - (int)(Game.buttonnHeight*0.5),Game.Buttonwidth, Game.buttonnHeight);
		
		
	}

	@Override
	public void MouseReleased(MouseEvent e) {
		if(this.etat == regle.Menu) {
			int CornerX0 = Game.GAME_WIDTH/2 - (int)(Game.Buttonwidth*0.5);
			int SizeX = Game.Buttonwidth;
			int CornerX1 = CornerX0 + SizeX;
					
			int CornerY0 = (int)(Game.GAME_HEIGHT/3.5) - (int)(Game.buttonnHeight*0.5);
			int SizeY = Game.buttonnHeight;
			int CornerY1 = CornerY0 + SizeY;
					
			if(e.getX() >= CornerX0 && e.getX() <= CornerX1 && e.getY() >= CornerY0 && e.getY() <= CornerY1 ) {
				this.selectedButton = 0;
				this.etat = regle.Monde;
			}
			
			CornerY0 +=Game.Offset;
			SizeY = Game.buttonnHeight;
			CornerY1 = CornerY0 + SizeY;
					
			if(e.getX() >= CornerX0 && e.getX() <= CornerX1 && e.getY() >= CornerY0 && e.getY() <= CornerY1 ) {
				this.selectedButton = 1;
				this.etat = regle.Diff;
			}
			
			CornerY0 +=Game.Offset;
			SizeY = Game.buttonnHeight;
			CornerY1 = CornerY0 + SizeY;
					
			if(e.getX() >= CornerX0 && e.getX() <= CornerX1 && e.getY() >= CornerY0 && e.getY() <= CornerY1 ) {
				this.selectedButton = 2;
				this.etat = regle.Agent;
			}
			
			CornerY0 +=Game.Offset;
			SizeY = Game.buttonnHeight;
			CornerY1 = CornerY0 + SizeY;
					
			if(e.getX() >= CornerX0 && e.getX() <= CornerX1 && e.getY() >= CornerY0 && e.getY() <= CornerY1 ) {
				this.selectedButton = 3;
				gamestates.state = gamestates.MENU;
			}
		}else {
			//g.fillRect((int)(Game.TILES_SIZE),(int)(Game.TILES_SIZE*1) , Game.TILES_SIZE*3, Game.TILES_SIZE);
			int CornerX0 = Game.TILES_SIZE;
			int SizeX = Game.TILES_SIZE*3;
			int CornerX1 = CornerX0 + SizeX;
					
			int CornerY0 = Game.TILES_SIZE;
			int SizeY = Game.TILES_SIZE;
			int CornerY1 = CornerY0 + SizeY;
					
			if(e.getX() >= CornerX0 && e.getX() <= CornerX1 && e.getY() >= CornerY0 && e.getY() <= CornerY1 ) {
				this.etat = regle.Menu;
			}
		}
		
	}

	@Override
	public void MouseMouved(MouseEvent e) {
		if(this.etat == regle.Menu) {
			int CornerX0 = Game.GAME_WIDTH/2 - (int)(Game.Buttonwidth*0.5);
			int SizeX = Game.Buttonwidth;
			int CornerX1 = CornerX0 + SizeX;
					
			int CornerY0 = (int)(Game.GAME_HEIGHT/3.5) - (int)(Game.buttonnHeight*0.5);
			int SizeY = Game.buttonnHeight;
			int CornerY1 = CornerY0 + SizeY;
					
			if(e.getX() >= CornerX0 && e.getX() <= CornerX1 && e.getY() >= CornerY0 && e.getY() <= CornerY1 ) {
				this.selectedButton = 0;
			}
			
			CornerY0 +=Game.Offset;
			SizeY = Game.buttonnHeight;
			CornerY1 = CornerY0 + SizeY;
					
			if(e.getX() >= CornerX0 && e.getX() <= CornerX1 && e.getY() >= CornerY0 && e.getY() <= CornerY1 ) {
				this.selectedButton = 1;
			}
			
			CornerY0 +=Game.Offset;
			SizeY = Game.buttonnHeight;
			CornerY1 = CornerY0 + SizeY;
					
			if(e.getX() >= CornerX0 && e.getX() <= CornerX1 && e.getY() >= CornerY0 && e.getY() <= CornerY1 ) {
				this.selectedButton = 2;
			}
			
			CornerY0 +=Game.Offset;
			SizeY = Game.buttonnHeight;
			CornerY1 = CornerY0 + SizeY;
					
			if(e.getX() >= CornerX0 && e.getX() <= CornerX1 && e.getY() >= CornerY0 && e.getY() <= CornerY1 ) {
				this.selectedButton = 3;
			}
		}else{
			//g.fillRect((int)(Game.TILES_SIZE),(int)(Game.TILES_SIZE*1) , Game.TILES_SIZE*3, Game.TILES_SIZE);
			int CornerX0 = Game.TILES_SIZE;
			int SizeX = Game.TILES_SIZE*3;
			int CornerX1 = CornerX0 + SizeX;
					
			int CornerY0 = Game.TILES_SIZE;
			int SizeY = Game.TILES_SIZE;
			int CornerY1 = CornerY0 + SizeY;
					
			if(e.getX() >= CornerX0 && e.getX() <= CornerX1 && e.getY() >= CornerY0 && e.getY() <= CornerY1 ) {
				this.ExitSelected = true;
			}else {
				this.ExitSelected = false;
			}
		}
		

		
	}

	@Override
	public void KeyPressed(KeyEvent e) {	
		if(this.etat == regle.Menu) {
		if(e.getKeyCode()== KeyEvent.VK_ENTER) {		
			
				switch(this.selectedButton) {
				default:
					break;
				case 0:
					this.etat = regle.Monde;
					break;
				case 1:
					this.etat = regle.Diff;
					break;
				case 2:
					this.etat = regle.Agent;
					break;
				case 3:
					gamestates.state = gamestates.MENU;
					break;
			}
					
		}
		if(e.getKeyCode() == KeyEvent.VK_UP) {
			if(this.selectedButton == 0 ) {
				this.selectedButton = 3;
			}else {
				this.selectedButton--;
			}
		}
		if(e.getKeyCode() == KeyEvent.VK_DOWN) {
			if(this.selectedButton == 3 ) {
				this.selectedButton = 0;
			}else {
				this.selectedButton++;
			}
		}
		if(e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			gamestates.state = gamestates.MENU;
		}
		
		}else {
			if(e.getKeyCode() == KeyEvent.VK_ESCAPE) {
				this.etat = regle.Menu;
			}
		}
	}
		
		

	@Override
	public void KeyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	

}
