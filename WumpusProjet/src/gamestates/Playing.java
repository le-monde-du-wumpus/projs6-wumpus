package gamestates;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import entities.*;
import gen.Plateau;
import levels.LevelManager;
import main.Game;
import main.LecteurAudio;
import main.PlateauHandler;
import solve.Agent;
import utilz.HitboxMethods;
import utilz.LoadSave;

public class Playing extends State{
	
	private Player player;
	private Date StartTime;
	private int Score;
	public LevelManager levelManager;
	private Agent agent;
	private PlateauHandler plateauHandler;
	public Arrow arrow;
	public boolean PlayerWin;
	public boolean AgentWin;
	public boolean isPlayerDead;
	public EAgent EAgent;
	
	public BufferedImage imageGold;
	public BufferedImage imageWind;
	public BufferedImage imageWIND16;
	public BufferedImage imageOdeur;

	public HitboxMethods hitboxMethods;
	
	public int dif_agent ;
	public boolean Enableagent;
	
	public Difficultes difficultes ;

	public boolean WumpusRugissement;
	private int delais;
	private int delaisAgent;
	
	public final static int AgentMoyenDelais = 8;
	public final static int AgentDifficileDelais = 4;
	private int firstUpdate;
	
	private BufferedImage Orimg;
	private BufferedImage WumpusTueImg;
	
	private int x ,y, seed ,recursionMax,  nbWumpus, nbOr;
	double tauxRecouvrementMax;
	
	public Playing(Game game) {
		super(game);
		initClasses();
	}
	
	
	
	private void initClasses() { 
		
		this.StartTime = new Date();
		int x = 10;
		int y = 10;
		Score = 0;
		difficultes = difficultes.moyen;

	    double tauxRecouvrementMax = 0.8;   
	    double moyenne = (x+y)/2; 
	    int recursionMax = Math.max(2,(int) Math.ceil(moyenne/10)); 
	    int nbWumpus = Math.max(1,(int) Math.ceil(moyenne/10));    
	    
	    int nbOr = Math.max(1,(int) Math.ceil(moyenne/10));
	    
	    this.plateauHandler =new PlateauHandler(x, y,  -1, recursionMax, tauxRecouvrementMax ,  nbWumpus, nbOr,this);
		this.plateauHandler.UpdateExplore(new int[] {0,0});
		
		player = new Player(Game.TILES_SIZE*4, Game.TILES_SIZE*4,(int)(50 * Game.SCALE),(int)(100 * Game.SCALE),this); 
		levelManager = new LevelManager(this);
		arrow = new Arrow(game);
		
		hitboxMethods = new HitboxMethods(this);
		
		agent = new Agent(plateauHandler.getPlateau());	
		EAgent = new EAgent((Game.TILES_SIZE*11)+10,Game.TILES_SIZE*7,(int)(50 * Game.SCALE),(int)(100 * Game.SCALE),game);
		
		isPlayerDead = false;
		PlayerWin = false;
		AgentWin = false;
		
		delais=0;
		delaisAgent=0;
		WumpusRugissement = false;
		this.firstUpdate = 0;
		loadImages();
		
		

		
	}
	
	public void windowFocusLost() {
		player.resetDirBooleans();
	}
	public Player getPlayer() {
		return player;
	}
	
	
	public PlateauHandler getPlateauHandler() {
		return plateauHandler;
	}

	
	private void loadImages() { // crée un double Array de toutes les animations de la spritesheet
				
				imageGold = LoadSave.GetSpriteAtlas(LoadSave.GOLD_SPRITE);
				imageWind = LoadSave.GetSpriteAtlas(LoadSave.WIND_SPRITE);
				imageWIND16 = LoadSave.GetSpriteAtlas(LoadSave.WIND16_SPRITE);
				imageOdeur =  LoadSave.GetSpriteAtlas(LoadSave.ODEUR_SPRITE);
				BufferedImage img = LoadSave.GetSpriteAtlas(LoadSave.LEVEL_ATLAS);
				Orimg = img.getSubimage(9*16, 0*16, 16, 16); 
				WumpusTueImg =  img.getSubimage(11*16, 0*16, 16, 16); 
	}


	@Override
	public void update() {
		Date CurrentDate = new Date();
		long diffDate =  CurrentDate.getTime() - this.StartTime.getTime() ;		
		long TimeinSec = TimeUnit.MILLISECONDS.toSeconds(diffDate);
		this.Score = 1000 + this.player.getNbWumpusKill() * 500 + this.player.nbOrRamasses() *1000 - (int)(TimeinSec*1.5);
		
		
		
		if(this.Enableagent) {
			delaisAgent++;
			if(this.dif_agent == 1) {
				if(this.delaisAgent > Game.UPS_SET * this.AgentMoyenDelais) {
					this.MakeAgentPlay();
					delaisAgent = 0;
				}
				
			}
			if(this.dif_agent  == 2) {
				if(this.delaisAgent > Game.UPS_SET * this.AgentDifficileDelais) {
					this.MakeAgentPlay();
					delaisAgent = 0;
				}
			}
			
			
		}
		this.PlayerWin = (this.player.nbOrRamasses() >= this.plateauHandler.getPlateau().getNbOr());
		
		int[] playerPos = this.levelManager.getPlayerPos();
		int valueCell = this.plateauHandler.getPlateau().getMatrice()[playerPos[0]][playerPos[1]];
		
		this.isPlayerDead = (valueCell == 3 || valueCell ==4);
		
		
		
		
		
		if(firstUpdate < 20) {
			if(this.AgentWin || this.isPlayerDead || this.PlayerWin) {
				System.out.printf("test");
				boolean TempAgent = this.Enableagent;
				this.refresh(this.x, this.y,this.seed,this.recursionMax, this.tauxRecouvrementMax, this.nbWumpus, this.nbOr);
				this.Enableagent = TempAgent;
				if(this.difficultes == Difficultes.facile) {
					this.levelManager.updateMap();
				}
			}
			firstUpdate++;
		}
		if(firstUpdate < 4) {
			if(this.difficultes == Difficultes.facile) {
				this.levelManager.updateMap();
			}
		}
		
		
		if(isPlayerDead) {
			System.out.println("dead");
			gamestates.state = gamestates.DEATHSCREEN;
			player.resetDirBooleans();
			refreshBool();
			game.getdeathScreen().refresh(this);
			
		}
		
		if(this.PlayerWin) {
			System.out.println("le joueur a gagné" + this.player.nbOrRamasses());	
			gamestates.state = gamestates.WINSCREEN;
			player.resetDirBooleans();
			refreshBool();
			game.getwinScreen().refresh(this);
			
		}
		if(this.AgentWin) {
			gamestates.state = gamestates.AGENTWIN;
			player.resetDirBooleans();
			refreshBool();
			game.getAgentWin().refresh(this);
		}
		if(agent.getError()!=0) {
			System.out.println("erreur agent");
		}
		
		
		if(WumpusRugissement) {
			delais++;
			if(delais>100) {
				delais = 0;
				WumpusRugissement = false;
				LecteurAudio.Rugissement();
			}
		}
		
		player.update();
		arrow.update();
		
	}
	public void MakeAgentPlay() {
		this.AgentWin = agent.nextMove();
	}
	
	
	public void refresh(int x, int y , int seed , int recursionMax, double tauxRecouvrementMax,int  nbWumpus,int nbOr) {
		plateauHandler.refresh(x, y, seed, recursionMax,tauxRecouvrementMax, nbWumpus,nbOr);
		refreshBool();
		this.StartTime = new Date();
		this.player.refresh();
		this.levelManager.refresh(this);
		this.agent = new Agent(this.plateauHandler.getPlateau());
		this.x = x;
		this.y = y;
		this.seed = seed;
		this.recursionMax = recursionMax;
		this.nbWumpus = nbWumpus;
		this.nbOr = nbOr;		

	}
	
	public void refreshBool() {
		isPlayerDead = false;
		Enableagent= false;
		PlayerWin = false;
		AgentWin = false;
	}


	@Override
	public void draw(Graphics g) {
		
		
		
		levelManager.draw(g);
		
		int[] playerPos = this.getLevelManager().getPlayerPos(); 
		int val = this.getPlateauHandler().getPlateau().getMatrice()[playerPos[0]][playerPos[1]];
		switch(val) {
			default: 
				break;
			case 2:
				g.drawImage(imageGold, Game.TILES_SIZE*11, Game.TILES_SIZE*6, 254/2,198/2, null);
				break;
			case 5 : 
				g.drawImage(this.imageWind,  Game.TILES_SIZE*4, Game.TILES_SIZE*4, 378/4,293/3, null);
				g.drawImage(this.imageWind,  Game.TILES_SIZE*17, Game.TILES_SIZE*9, 378/4,293/3, null);
				break;
			case 6 : 
				g.drawImage(imageOdeur,  Game.TILES_SIZE*4, Game.TILES_SIZE*4, 890/4,225/4, null);
				g.drawImage(imageOdeur,  Game.TILES_SIZE*15, Game.TILES_SIZE*10, 890/4,225/4, null);
				break;
			case 7 :
				//odeur + brise
				g.drawImage(this.imageWind,  Game.TILES_SIZE*4, Game.TILES_SIZE*4, 378/4,293/3, null);
				g.drawImage(this.imageWind,  Game.TILES_SIZE*17, Game.TILES_SIZE*9, 378/4,293/3, null);
				g.drawImage(imageOdeur,  Game.TILES_SIZE*15, Game.TILES_SIZE*4, 890/4,225/4, null);
				g.drawImage(imageOdeur,  Game.TILES_SIZE*4, Game.TILES_SIZE*10, 890/4,225/4, null);
				break;
			case 9:
				g.drawImage(imageGold, Game.TILES_SIZE*11, Game.TILES_SIZE*6, 254/2,198/2, null);
				g.drawImage(this.imageWind,  Game.TILES_SIZE*4, Game.TILES_SIZE*4, 378/4,293/3, null);
				g.drawImage(this.imageWind,  Game.TILES_SIZE*17, Game.TILES_SIZE*9, 378/4,293/3, null);
				break;
			case 10 : 
				g.drawImage(imageGold, Game.TILES_SIZE*11, Game.TILES_SIZE*6, 254/2,198/2, null);
				g.drawImage(imageOdeur,  Game.TILES_SIZE*4, Game.TILES_SIZE*4, 890/4,225/4, null);
				g.drawImage(imageOdeur,  Game.TILES_SIZE*15, Game.TILES_SIZE*10, 890/4,225/4, null);
				
				break;
			
		}
		player.render(g);
		
		//input du jeu en mode playing
		g.setColor(Color.white);
		g.fillRect((int)(Game.TILES_SIZE*23.5),Game.TILES_SIZE*6 , Game.TILES_SIZE, Game.TILES_SIZE);
		g.setColor(Color.BLACK);
		g.setFont( super.game.fontTitre);
		
		String Zkey = "Z";
		FontMetrics fm = g.getFontMetrics();
		int prefSizeWidth = fm.stringWidth(Zkey);
		int prefSizeHeight = fm.getHeight();		
		g.drawString(Zkey, (int)(Game.TILES_SIZE*24) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*6.5) +  (int)(prefSizeHeight*0.5));
		
		g.setColor(Color.white);
		g.fillRect((int)(Game.TILES_SIZE*23.5),Game.TILES_SIZE*7 +3 , Game.TILES_SIZE, Game.TILES_SIZE);
		g.setColor(Color.BLACK);
		g.setFont(super.game.fontTitre);
		
		String Skey = "S";
		fm = g.getFontMetrics();
		prefSizeWidth = fm.stringWidth(Skey);
		prefSizeHeight = fm.getHeight();		
		g.drawString(Skey, (int)(Game.TILES_SIZE*24) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*7.5) +  (int)(prefSizeHeight*0.5)+3);
		
		g.setColor(Color.white);
		g.fillRect((int)(Game.TILES_SIZE*24.5)+3,Game.TILES_SIZE*7 +3, Game.TILES_SIZE, Game.TILES_SIZE);
		g.setColor(Color.BLACK);
		g.setFont(super.game.fontTitre);
		
		String Dkey = "D";
		fm = g.getFontMetrics();
		prefSizeWidth = fm.stringWidth(Dkey);
		prefSizeHeight = fm.getHeight();		
		g.drawString(Dkey, (int)(Game.TILES_SIZE*25) -  (int)(prefSizeWidth*0.5)+3, (int)(Game.TILES_SIZE*7.5) +  (int)(prefSizeHeight*0.5)+3);
		
		g.setColor(Color.white);
		g.fillRect((int)(Game.TILES_SIZE*22.5)-3,Game.TILES_SIZE*7 +3 , Game.TILES_SIZE, Game.TILES_SIZE);
		g.setColor(Color.BLACK);
		g.setFont(super.game.fontTitre);
		
		String Qkey = "Q";
		fm = g.getFontMetrics();
		prefSizeWidth = fm.stringWidth(Qkey);
		prefSizeHeight = fm.getHeight();		
		g.drawString(Qkey, (int)(Game.TILES_SIZE*23) -  (int)(prefSizeWidth*0.5)-3, (int)(Game.TILES_SIZE*7.5) +  (int)(prefSizeHeight*0.5)+3);
		
		
		
		
		
		
		String Move = "Deplacements";
		g.setFont(super.game.fontTitre.deriveFont((float) (Game.TILES_SIZE/3.5)));
		g.setColor(Color.white);
		fm = g.getFontMetrics();
		prefSizeWidth = fm.stringWidth(Move);
		prefSizeHeight = fm.getHeight();		
		g.drawString(Move, (int)(Game.TILES_SIZE*24) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*5.5) +  (int)(prefSizeHeight*0.5));
		
		
		String Fire = "Tirer une fleche";
		g.setFont(super.game.fontTitre.deriveFont((float) (Game.TILES_SIZE/3.5)));
		g.setColor(Color.white);
		fm = g.getFontMetrics();
		prefSizeWidth = fm.stringWidth(Fire);
		prefSizeHeight = fm.getHeight();		
		g.drawString(Fire, (int)(Game.TILES_SIZE*24) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*9) +  (int)(prefSizeHeight*0.5));
		
		
		
		
		
		
		g.setColor(Color.white);
		g.fillRect((int)(Game.TILES_SIZE*23.5),(int)(Game.TILES_SIZE*9.5), Game.TILES_SIZE, Game.TILES_SIZE);
		g.setColor(Color.BLACK);
		g.setFont(new Font("Arial Unicode MS",Font.BOLD,(int)(Game.TILES_SIZE)));
		
		String UpKey = "↑";
		fm = g.getFontMetrics();
		prefSizeWidth = fm.stringWidth(UpKey);
		prefSizeHeight = fm.getHeight();		
		g.drawString(UpKey, (int)(Game.TILES_SIZE*24) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*10) +  (int)(prefSizeHeight*0.5) - (int)(Game.TILES_SIZE*0.3));
		
		
		g.setColor(Color.white);
		g.fillRect((int)(Game.TILES_SIZE*23.5),(int)(Game.TILES_SIZE*10.5) +3, Game.TILES_SIZE, Game.TILES_SIZE);
		g.setColor(Color.BLACK);
		g.setFont(new Font("Arial Unicode MS",Font.BOLD,(int)(Game.TILES_SIZE)));
		
		String DownKey = "↓";
		fm = g.getFontMetrics();
		prefSizeWidth = fm.stringWidth(DownKey);
		prefSizeHeight = fm.getHeight();		
		g.drawString(DownKey, (int)(Game.TILES_SIZE*24) -  (int)(prefSizeWidth*0.5),3+ (int)(Game.TILES_SIZE*11) +  (int)(prefSizeHeight*0.5) - (int)(Game.TILES_SIZE*0.3));
		
		
		g.setColor(Color.white);
		g.fillRect((int)(Game.TILES_SIZE*22.5)-3,(int)(Game.TILES_SIZE*10.5)+3 , Game.TILES_SIZE, Game.TILES_SIZE);
		g.setColor(Color.BLACK);
		g.setFont(new Font("Arial Unicode MS",Font.BOLD,(int)(Game.TILES_SIZE)));
		
		String LeftKey = "←";
		fm = g.getFontMetrics();
		prefSizeWidth = fm.stringWidth(LeftKey);
		prefSizeHeight = fm.getHeight();		
		g.drawString(LeftKey, (int)(Game.TILES_SIZE*23) -  (int)(prefSizeWidth*0.5)-3,3+ (int)(Game.TILES_SIZE*11) +  (int)(prefSizeHeight*0.5) - (int)(Game.TILES_SIZE*0.3));
		
		
		g.setColor(Color.white);
		g.fillRect((int)(Game.TILES_SIZE*24.5)+3,(int)(Game.TILES_SIZE*10.5)+3 , Game.TILES_SIZE, Game.TILES_SIZE);
		g.setColor(Color.BLACK);
		g.setFont(new Font("Arial Unicode MS",Font.BOLD,(int)(Game.TILES_SIZE)));
		
		String RightKey = "→";
		fm = g.getFontMetrics();
		prefSizeWidth = fm.stringWidth(RightKey);
		prefSizeHeight = fm.getHeight();		
		g.drawString(RightKey, (int)(Game.TILES_SIZE*25) -  (int)(prefSizeWidth*0.5)+3,3+ (int)(Game.TILES_SIZE*11) +  (int)(prefSizeHeight*0.5) - (int)(Game.TILES_SIZE*0.3));
		
		String Carte = "Carte et Menu";
		g.setFont(super.game.fontTitre.deriveFont((float) (Game.TILES_SIZE/3.5)));
		g.setColor(Color.white);
		fm = g.getFontMetrics();
		prefSizeWidth = fm.stringWidth(Carte);
		prefSizeHeight = fm.getHeight();		
		g.drawString(Carte, (int)(Game.TILES_SIZE*24) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*12) +  (int)(prefSizeHeight*0.5));
		
		
		g.setColor(Color.white);
		g.fillRect((int)(Game.TILES_SIZE*23.5),(int)(Game.TILES_SIZE*12.5) , Game.TILES_SIZE, Game.TILES_SIZE);
		g.setColor(Color.BLACK);
		g.setFont(super.game.fontTitre.deriveFont(Game.TILES_SIZE*2/3));
		
		String Ckey = "C";
		fm = g.getFontMetrics();
		prefSizeWidth = fm.stringWidth(Ckey);
		prefSizeHeight = fm.getHeight();		 
		g.drawString(Ckey, (int)(Game.TILES_SIZE*24) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*13) +  (int)(prefSizeHeight*0.5));
		
		g.setColor(Color.white);
		g.fillRect((int)(Game.TILES_SIZE*23.5),(int)(Game.TILES_SIZE*14.5) , Game.TILES_SIZE, Game.TILES_SIZE);
		g.fillRect((int)(Game.TILES_SIZE*22.5),(int)(Game.TILES_SIZE*14.5) , Game.TILES_SIZE, Game.TILES_SIZE);
		g.fillRect((int)(Game.TILES_SIZE*24.5),(int)(Game.TILES_SIZE*14.5) , Game.TILES_SIZE, Game.TILES_SIZE);
		g.setColor(Color.BLACK);
		g.setFont(super.game.fontTitre.deriveFont((float) (Game.TILES_SIZE*0.40)));
		
		String Spacekey = "Espace";
		fm = g.getFontMetrics();
		prefSizeWidth = fm.stringWidth(Spacekey);
		prefSizeHeight = fm.getHeight();		 
		g.drawString(Spacekey, (int)(Game.TILES_SIZE*24) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*15) +  (int)(prefSizeHeight*0.5));
		
		String Or = "Ramasser Or";
		g.setFont(super.game.fontTitre.deriveFont((float) (Game.TILES_SIZE/3.5)));
		g.setColor(Color.white);
		fm = g.getFontMetrics();
		prefSizeWidth = fm.stringWidth(Or);
		prefSizeHeight = fm.getHeight();		
		g.drawString(Or, (int)(Game.TILES_SIZE*24) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*14) +  (int)(prefSizeHeight*0.5));
		
		String Fleche = "Flèches restantes :";
		g.setFont(super.game.fontTitre.deriveFont(Font.BOLD,(float) (Game.TILES_SIZE/3.5)));
		g.setColor(Color.white);
		fm = g.getFontMetrics();
		prefSizeWidth = fm.stringWidth(Fleche);
		prefSizeHeight = fm.getHeight();		
		g.drawString(Fleche, (int)(Game.TILES_SIZE*5) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*1.5) +  (int)(prefSizeHeight*0.5));
		
		String CountFleche = "" + this.getPlayer().getArrow();
		g.setFont(super.game.fontTitre.deriveFont(Font.BOLD,(float) (Game.TILES_SIZE/3.5)));
		g.setColor(Color.white);
		fm = g.getFontMetrics();
		prefSizeWidth = fm.stringWidth(CountFleche);
		prefSizeHeight = fm.getHeight();		
		g.drawString(CountFleche, (int)(Game.TILES_SIZE*8.2) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*1.5) +  (int)(prefSizeHeight*0.5));
		
		String OrRamassesText = "Ors Ramassés :";
		g.setFont(super.game.fontTitre.deriveFont(Font.BOLD,(float) (Game.TILES_SIZE/3.5)));
		g.setColor(Color.white);
		fm = g.getFontMetrics();
		prefSizeWidth = fm.stringWidth(OrRamassesText);
		prefSizeHeight = fm.getHeight();		
		g.drawString(OrRamassesText, (int)(Game.TILES_SIZE*4.3) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*13.5) +  (int)(prefSizeHeight*0.5));
		
		String OrRamassé = "" + this.getPlayer().nbOrRamasses() + "/" + this.plateauHandler.getPlateau().getNbOr();
		g.setFont(super.game.fontTitre.deriveFont(Font.BOLD,(float) (Game.TILES_SIZE/3.5)));
		g.setColor(Color.white);
		fm = g.getFontMetrics();
		prefSizeWidth = fm.stringWidth(OrRamassé);
		prefSizeHeight = fm.getHeight();		
		g.drawString(OrRamassé, (int)(Game.TILES_SIZE*7) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*13.5) +  (int)(prefSizeHeight*0.5));
		
		g.drawImage(Orimg,(int)(Game.TILES_SIZE*7.6),(int)(Game.TILES_SIZE*13.23),(int)(Game.TILES_SIZE*0.5),(int)(Game.TILES_SIZE*0.5),null);
		
		String TempsText = "Temps Ecoulé";
		g.setFont(super.game.fontTitre.deriveFont(Font.BOLD,(float) (Game.TILES_SIZE/3.5)));
		g.setColor(Color.white);
		fm = g.getFontMetrics();
		prefSizeWidth = fm.stringWidth(TempsText);
		prefSizeHeight = fm.getHeight();		
		g.drawString(TempsText, (int)(Game.TILES_SIZE*4) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*14) +  (int)(prefSizeHeight*0.5));
		Date CurrentDate = new Date();
		long diffDate =  CurrentDate.getTime() - this.StartTime.getTime() ;
		
		long TimeinSec = TimeUnit.MILLISECONDS.toSeconds(diffDate);
		
		long Sec = TimeinSec;
		long Min = 0;
		long Hour = 0;
		while(Sec > 60) {
			Sec -= 60;
			Min++;
		}
		while(Min > 60) {
			Min -= 60;
			Hour++;
		}
		
		String HourText = "" + Hour;
		if(Hour < 10) {
			HourText = "0"+Hour;
		}
		
		String MinText = "" + Min;
		if(Min < 10) {
			MinText = "0"+Min;
		}
		
		String SecText = "" + Sec;
		if(Sec < 10) {
			SecText = "0"+Sec;
		}
		
		String Temps = HourText+":"+MinText+":"+SecText;
		g.setFont(super.game.fontTitre.deriveFont(Font.BOLD,(float) (Game.TILES_SIZE/3.5)));
		g.setColor(Color.white);
		fm = g.getFontMetrics();
		prefSizeWidth = fm.stringWidth(Temps);
		prefSizeHeight = fm.getHeight();		
		g.drawString(Temps, (int)(Game.TILES_SIZE*7.3) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*14) +  (int)(prefSizeHeight*0.5));

		String WumpusTuesText = "Wumpus tués :";
		g.setFont(super.game.fontTitre.deriveFont(Font.BOLD,(float) (Game.TILES_SIZE/3.5)));
		g.setColor(Color.white);
		fm = g.getFontMetrics();
		prefSizeWidth = fm.stringWidth(WumpusTuesText);
		prefSizeHeight = fm.getHeight();		
		g.drawString(WumpusTuesText, (int)(Game.TILES_SIZE*18.2) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*13.5) +  (int)(prefSizeHeight*0.5));
		
		String WumpusTues = "" + this.getPlayer().getNbWumpusKill();
		g.setFont(super.game.fontTitre.deriveFont(Font.BOLD,(float) (Game.TILES_SIZE/3.5)));
		g.setColor(Color.white);
		fm = g.getFontMetrics();
		prefSizeWidth = fm.stringWidth(WumpusTues);
		prefSizeHeight = fm.getHeight();		
		g.drawString(WumpusTues, (int)(Game.TILES_SIZE*20.3) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*13.5) +  (int)(prefSizeHeight*0.5));
		
		g.drawImage(WumpusTueImg,(int)(Game.TILES_SIZE*20.55),(int)(Game.TILES_SIZE*13.23),(int)(Game.TILES_SIZE*0.5),(int)(Game.TILES_SIZE*0.5),null);

		
		String ScoreText = "Score :";
		g.setFont(super.game.fontTitre.deriveFont(Font.BOLD,(float) (Game.TILES_SIZE/3.5)));
		g.setColor(Color.white);
		fm = g.getFontMetrics();
		prefSizeWidth = fm.stringWidth(ScoreText);
		prefSizeHeight = fm.getHeight();		
		g.drawString(ScoreText, (int)(Game.TILES_SIZE*17.35) -  (int)(prefSizeWidth*0.5), (int)(Game.TILES_SIZE*14) +  (int)(prefSizeHeight*0.5));
		//((or * 1000)  + ((tempsFin/Ormax - moyenTemp)) 5) - (ormax - ortrouver1000)

		this.Score = 1000 + this.player.getNbWumpusKill() * 500 + this.player.nbOrRamasses() *1000 - (int)(TimeinSec*1.5);

		String ScoreValueText = "" + this.Score;
		g.setFont(super.game.fontTitre.deriveFont(Font.BOLD,(float) (Game.TILES_SIZE/3.5)));
		g.setColor(Color.white);
		fm = g.getFontMetrics();
		prefSizeWidth = fm.stringWidth(ScoreValueText);
		prefSizeHeight = fm.getHeight();		
		g.drawString(ScoreValueText, (int)(Game.TILES_SIZE*18.4) , (int)(Game.TILES_SIZE*14) +  (int)(prefSizeHeight*0.5));
		
		if(agent.getPos()[0] == this.getLevelManager().getPlayerPos()[0] && agent.getPos()[1] == this.getLevelManager().getPlayerPos()[1] && this.Enableagent ){
			this.EAgent.render(g);
		}
		arrow.draw(g);
		
		
	}

	

	@Override
	public void MouseClicked(MouseEvent e) {
		
		
	}



	@Override
	public void MousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void MouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void MouseMouved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void KeyPressed(KeyEvent e) {
		switch(e.getKeyCode()) { 
		default :
			break;
		case KeyEvent.VK_Z:	
			player.setUp(true);
			 break;
		case KeyEvent.VK_Q:
			player.setLeft(true);
			 break;
		case KeyEvent.VK_S:
			player.setDown(true);
			 break;
		case KeyEvent.VK_D:
			player.setRight(true);
			 break;
		case KeyEvent.VK_SPACE:
			player.ramasserOr();
			System.out.println(player.nbOrRamasses() + " nombre d'or ramasses par le joueur" );
			System.out.println(plateauHandler.getPlateau().getNbOr() + " or total ");
			break;
		case KeyEvent.VK_C:	
			gamestates.state = gamestates.MAP;	
			game.getMap().refresh(this);
			player.resetDirBooleans();
			break;	
		case KeyEvent.VK_RIGHT:
			
				player.tuerWumpus(0);
				player.setShooting(true);
			break;
		case KeyEvent.VK_DOWN:
			
			player.tuerWumpus(1);
			player.setShooting(true);
			break;
		case KeyEvent.VK_LEFT:
			
			player.tuerWumpus(2);
			player.setShooting(true);
			break;	
		case KeyEvent.VK_UP:
			
			player.tuerWumpus(3);
			player.setShooting(true);
			break;		
		}			
	}	
		
	
		
		
		
	



	@Override
	public void KeyReleased(KeyEvent e) {
		switch(e.getKeyCode()) { 
		case KeyEvent.VK_Z:	
			 player.setUp(false);
			 break;
		
		case KeyEvent.VK_Q:
			player.setLeft(false);
			 break;
		
		case KeyEvent.VK_S:
			player.setDown(false);
			 break;
		
		case KeyEvent.VK_D:
			player.setRight(false);
			 break;
		} 
		
	}
	
	public Playing getPlaying() {
		return this;
	}
	
	public LevelManager getLevelManager() {
		return levelManager;
	}
	public Agent getAgent() {
		return this.agent;
	}
	public int getScore() {
		return this.Score;
	}



	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
} 
