package gamestates;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import javax.swing.text.PlainDocument;

import gamestates.gamestates;
import main.Game;
import main.PlateauHandler;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;

public class NewGame extends State {
	
	JTextField inputFieldX ;
	JTextField inputFieldY ;
	JTextField inputSeed;
	JLabel TailleCarte;
	JLabel Titre;
	JLabel TitreSeed;
	JLabel TitreOption;
	JLabel TitreAgent;
	JLabel TitreDiffAgent;
	
	JCheckBox Triche;
	JCheckBox Facile;
	JCheckBox Moyen;
	JCheckBox Difficile;
	
	JCheckBox AgentActive;
	JCheckBox FacileAgent;
	JCheckBox MoyenAgent;
	JCheckBox DifficileAgent;
	
	
	JButton ButtonCommencer;
	
	public NewGame(Game game ) {
		super(game);
		//Labels
		
		
		
		TailleCarte =new JLabel("Taille de la carte");
		Titre = new  JLabel("Nouvelle Partie");
		TitreSeed = new  JLabel("Seed de la carte");
		TitreOption =  new  JLabel("Difficulté de la partie");
		TitreAgent = new  JLabel("Agent");
		TitreDiffAgent =new  JLabel("Difficulté de l'Agent");
				
		Font fontTitre = super.game.fontTitre;
		Titre.setFont(fontTitre);
		
		Font fontSeed = super.game.font;
		TitreSeed.setFont(fontSeed);
		
		Font fontTailleCarte =  super.game.font;
		TailleCarte.setFont(fontTailleCarte);
		
		Font fontOption =  super.game.font;
		TitreOption.setFont(fontOption);
		
		Font fontOptionAgent =  super.game.font;
		TitreAgent.setFont(fontOptionAgent);
		
		Font fontDiffAgent =  super.game.font;
		TitreDiffAgent.setFont(fontDiffAgent);
		
		
		Titre.setForeground(Game.textColor);
		TitreSeed.setForeground(Game.textColor);
		TailleCarte.setForeground(Game.textColor);
		TitreOption.setForeground(Game.textColor);
		TitreAgent.setForeground(Game.textColor);
		TitreDiffAgent.setForeground(Game.textColor);
		
		
		Dimension SizeTitre = Titre.getPreferredSize();
		Dimension SizeSeed = TitreSeed.getPreferredSize();
		Dimension SizeCarte = TailleCarte.getPreferredSize();
		Dimension SizeOption = TitreOption.getPreferredSize();
		Dimension SizeOptionAgent = TitreAgent.getPreferredSize();
		Dimension SizeDiffAgent = TitreDiffAgent.getPreferredSize();
		
		Titre.setBounds((game.GAME_WIDTH/2 )-(SizeTitre.width/2),game.TILES_SIZE,SizeTitre.width,SizeTitre.height); //position du champ
		TitreSeed.setBounds((game.GAME_WIDTH/2 )-(SizeSeed.width/2),game.TILES_SIZE*3,SizeSeed.width,SizeSeed.height); //position du champ
		TailleCarte.setBounds((game.GAME_WIDTH/2 )-(SizeCarte.width/2),(int)(game.TILES_SIZE*4.5),SizeCarte.width,SizeCarte.height); //position du champ
		TitreOption.setBounds((game.GAME_WIDTH/2 )-(SizeOption.width/2),(int)(game.TILES_SIZE*5.5),SizeOption.width,SizeOption.height); //position du champ
		TitreAgent.setBounds((game.GAME_WIDTH/2 )-(SizeOptionAgent.width/2),(int)(game.TILES_SIZE*6.5),SizeOptionAgent.width,SizeOptionAgent.height); //position du champ
		TitreDiffAgent.setBounds((game.GAME_WIDTH/2 )-(SizeDiffAgent.width/2),(int)(game.TILES_SIZE*7.5),SizeDiffAgent.width,SizeDiffAgent.height); 
		//Inputs fields

		inputFieldY = new JTextField(20); 
		inputFieldX = new JTextField(20); 
		
		int offset = 10;
		

		inputFieldX.setBounds((game.GAME_WIDTH/2)-((int)(Game.TILES_SIZE*0.8))-offset,game.TILES_SIZE*5,(int)(Game.TILES_SIZE*0.8),(int)(Game.TILES_SIZE/3)); 
		inputFieldX.setHorizontalAlignment(SwingConstants.CENTER);
		inputFieldX.setFont(super.game.font.deriveFont(Font.PLAIN,(int)(Game.TILES_SIZE*0.20)));
		inputFieldX.setBorder(BorderFactory.createLineBorder(Game.BorderColor,2));
		inputFieldX.setBackground(Game.inputFieldBackGround);
		
		inputFieldX.addFocusListener(new FocusListener() {
		    @Override
		    public void focusGained(FocusEvent e) {
		    }
		    @Override
		    public void focusLost(FocusEvent e) {
		       if(!TestDimMapX()) {
		    	   if(!inputFieldX.getText().isEmpty()) {
		   			int valueX = Integer.parseInt(inputFieldX.getText());	
		   			if(valueX < 3) {
		   				inputFieldX.setText("3");
		   			}
		   			if(valueX > 100) {
		   				inputFieldX.setText("100");
		   			}
		   		}else {
		   			inputFieldX.setText("5");
		   		}
		       }
		    }
		});
		

		inputFieldY.setBounds((game.GAME_WIDTH/2)+offset,game.TILES_SIZE*5,(int)(Game.TILES_SIZE*0.8),(int)(Game.TILES_SIZE/3));
		inputFieldY.setHorizontalAlignment(SwingConstants.CENTER);
		inputFieldY.setFont(super.game.font.deriveFont(Font.PLAIN,(int)(Game.TILES_SIZE*0.2)));
		inputFieldY.setBorder(BorderFactory.createLineBorder(Game.BorderColor,2));
		inputFieldY.setBackground(Game.inputFieldBackGround);
		
		inputFieldY.addFocusListener(new FocusListener() {
		    @Override
		    public void focusGained(FocusEvent e) {
		    }
		    @Override
		    public void focusLost(FocusEvent e) {
		       if(!TestDimMapY()) {
		    	   if(!inputFieldY.getText().isEmpty()) {
		   			int valueX = Integer.parseInt(inputFieldX.getText());	
		   			if(valueX < 3) {
		   				inputFieldY.setText("3");
		   			}
		   			if(valueX > 100) {
		   				inputFieldY.setText("100");
		   			}
		   		}else {
		   			inputFieldY.setText("5");
		   		}
		       }
		    }
		});
		
		
		inputSeed = new JTextField(20);
		inputSeed.setBorder(new LineBorder(Color.black));		inputSeed.setBounds((Game.GAME_WIDTH/2 )-(100),(int)(Game.TILES_SIZE*3.5),(int)(Game.TILES_SIZE*3.3),(int)(Game.TILES_SIZE/2));
		inputSeed.setHorizontalAlignment(SwingConstants.CENTER);
		inputSeed.setFont(super.game.font.deriveFont(Font.PLAIN,(int)(Game.TILES_SIZE*0.2)));
		inputSeed.setBorder(BorderFactory.createLineBorder(Game.BorderColor,3));
		inputSeed.setBackground(Game.inputFieldBackGround);

		
		inputSeed.addFocusListener(new FocusListener() {
		    @Override
		    public void focusGained(FocusEvent e) {
		    }
		    @Override
		    public void focusLost(FocusEvent e) {
		       if(!TestSeed()) {
		    	   if(!inputSeed.getText().isEmpty() && inputSeed.getText().length() < 8) {		    		
		   			int seed = Integer.parseInt(inputSeed.getText());	
		   			if(seed < 0) {
		   				Random rand = new Random();
			   			int newseed = rand.nextInt(0,99999999);		
			   			inputSeed.setText(String.valueOf(newseed));
		   			}
		   		}else {
		   			Random rand = new Random();
		   			int seed = rand.nextInt(0,99999999);		
		   			inputSeed.setText(String.valueOf(seed));
		   		}
		       }
		    }
		});

		((AbstractDocument) inputSeed.getDocument()).setDocumentFilter(new IntegerFilter());
		
		((AbstractDocument) inputFieldX.getDocument()).setDocumentFilter(new IntegerFilter());
		((AbstractDocument) inputFieldY.getDocument()).setDocumentFilter(new IntegerFilter());
		
		

		
		//Checkbox

		ButtonGroup groupDiffMap = new ButtonGroup();
		Triche = new JCheckBox("Triche");
		Facile = new JCheckBox("Facile");
		Moyen = new JCheckBox("Moyen");
		Difficile = new JCheckBox("Difficile");
		groupDiffMap.add(Triche);
		groupDiffMap.add(Facile); 
		groupDiffMap.add(Moyen);
		groupDiffMap.add(Difficile);
		
		
		AgentActive = new JCheckBox("Activer Agent");
		
		ButtonGroup groupDiffAgent = new ButtonGroup();
		FacileAgent = new JCheckBox("Facile");
		MoyenAgent = new JCheckBox("Moyen");
		DifficileAgent = new JCheckBox("Difficile");
		groupDiffAgent.add(FacileAgent);
		groupDiffAgent.add(MoyenAgent);
		groupDiffAgent.add(DifficileAgent);
		
		Triche.setFont(super.game.font.deriveFont(Font.PLAIN,(int)(Game.TILES_SIZE*0.25)));
		Facile.setFont(super.game.font.deriveFont(Font.PLAIN,(int)(Game.TILES_SIZE*0.25)));
		Moyen.setFont(super.game.font.deriveFont(Font.PLAIN,(int)(Game.TILES_SIZE*0.25)));
		Difficile.setFont(super.game.font.deriveFont(Font.PLAIN,(int)(Game.TILES_SIZE*0.25)));
		
		AgentActive.setFont(super.game.font.deriveFont(Font.BOLD,(int)(Game.TILES_SIZE*0.3)));		
		
		FacileAgent.setFont(super.game.font.deriveFont(Font.PLAIN,(int)(Game.TILES_SIZE*0.25)));
		MoyenAgent.setFont(super.game.font.deriveFont(Font.PLAIN,(int)(Game.TILES_SIZE*0.25)));
		DifficileAgent.setFont(super.game.font.deriveFont(Font.PLAIN,(int)(Game.TILES_SIZE*0.25)));
		
		Triche.setBackground(Game.BackGroundColor);
		Facile.setBackground(Game.BackGroundColor);
		Moyen.setBackground(Game.BackGroundColor);
		Difficile.setBackground(Game.BackGroundColor);
		
		AgentActive.setBackground(Game.BackGroundColor);		
		
		FacileAgent.setBackground(Game.BackGroundColor);
		MoyenAgent.setBackground(Game.BackGroundColor);
		DifficileAgent.setBackground(Game.BackGroundColor);
		
		
		Triche.setForeground(Game.textColor);
		Facile.setForeground(Game.textColor);
		Moyen.setForeground(Game.textColor);
		Difficile.setForeground(Game.textColor);
		
		AgentActive.setForeground(Game.textColor);
		
		
		FacileAgent.setForeground(Game.textColor);
		MoyenAgent.setForeground(Game.textColor);
		DifficileAgent.setForeground(Game.textColor);
		

		Triche.setIcon(new ImageIcon("IconCheckBox.png"));
		Triche.setDisabledIcon(new ImageIcon("IconCheckBox.png"));
		Triche.setSelectedIcon(new ImageIcon("IconCheckBox.png"));
		
		Facile.setIcon(new ImageIcon("IconCheckBox.png"));
		Facile.setDisabledIcon(new ImageIcon("IconCheckBox.png"));
		Facile.setSelectedIcon(new ImageIcon("IconCheckBox.png"));
		
		Moyen.setIcon(new ImageIcon("IconCheckBox.png"));
		Moyen.setDisabledIcon(new ImageIcon("IconCheckBox.png"));
		Moyen.setSelectedIcon(new ImageIcon("IconCheckBox.png"));
		
		Difficile.setIcon(new ImageIcon("IconCheckBox.png"));
		Difficile.setDisabledIcon(new ImageIcon("IconCheckBox.png"));
		Difficile.setSelectedIcon(new ImageIcon("IconCheckBox.png"));
		
		AgentActive.setIcon(new ImageIcon("IconCheckBox.png"));
		AgentActive.setDisabledIcon(new ImageIcon("IconCheckBox.png"));
		AgentActive.setSelectedIcon(new ImageIcon("IconCheckBox.png"));
		
		FacileAgent.setIcon(new ImageIcon("IconCheckBox.png"));
		FacileAgent.setDisabledIcon(new ImageIcon("IconCheckBox.png"));
		FacileAgent.setSelectedIcon(new ImageIcon("IconCheckBox.png"));
		
		MoyenAgent.setIcon(new ImageIcon("IconCheckBox.png"));
		MoyenAgent.setDisabledIcon(new ImageIcon("IconCheckBox.png"));
		MoyenAgent.setSelectedIcon(new ImageIcon("IconCheckBox.png"));
		
		DifficileAgent.setIcon(new ImageIcon("IconCheckBox.png"));
		DifficileAgent.setDisabledIcon(new ImageIcon("IconCheckBox.png"));
		DifficileAgent.setSelectedIcon(new ImageIcon("IconCheckBox.png"));
		
		Triche.setFocusPainted(false);
		Facile.setFocusPainted(false);
		Moyen.setFocusPainted(false);
		Difficile.setFocusPainted(false);
		
		AgentActive.setFocusPainted(false);
		FacileAgent.setFocusPainted(false);
		MoyenAgent.setFocusPainted(false);
		DifficileAgent.setFocusPainted(false);
		
		Triche.setBorderPainted(true);
		Facile.setBorderPainted(true);
		Moyen.setBorderPainted(true);
		Difficile.setBorderPainted(true);
		
		AgentActive.setBorderPainted(true);
		
		FacileAgent.setBorderPainted(true);
		MoyenAgent.setBorderPainted(true);
		DifficileAgent.setBorderPainted(true);
		
		Triche.setHorizontalAlignment(SwingConstants.CENTER);
		Facile.setHorizontalAlignment(SwingConstants.CENTER);
		Moyen.setHorizontalAlignment(SwingConstants.CENTER);
		Difficile.setHorizontalAlignment(SwingConstants.CENTER);
		
		AgentActive.setHorizontalAlignment(SwingConstants.CENTER);
		
		FacileAgent.setHorizontalAlignment(SwingConstants.CENTER);
		MoyenAgent.setHorizontalAlignment(SwingConstants.CENTER);
		DifficileAgent.setHorizontalAlignment(SwingConstants.CENTER);
		
		
		
		Dimension SizeTriche = Triche.getPreferredSize();
		Dimension SizeFacile = Facile.getPreferredSize();
		Dimension SizeMoyen = Moyen.getPreferredSize();  
		Dimension SizeDifficile = Difficile.getPreferredSize();
		Dimension SizeTitreAgent = AgentActive.getPreferredSize();
		
		
		Triche.setBounds((game.GAME_WIDTH/2)-(SizeTriche.width)-(SizeTriche.width)-offset,game.TILES_SIZE*6,SizeTriche.width,SizeTriche.height);
		Facile.setBounds((game.GAME_WIDTH/2)-(SizeFacile.width/2)-(SizeFacile.width)/2-offset/2,game.TILES_SIZE*6,SizeFacile.width,SizeFacile.height);
		Moyen.setBounds((game.GAME_WIDTH/2)+offset/2,game.TILES_SIZE*6,SizeMoyen.width,SizeMoyen.height);
		Difficile.setBounds((game.GAME_WIDTH/2)+(SizeMoyen.width)+offset,game.TILES_SIZE*6,SizeDifficile.width,SizeDifficile.height);
		
		
		
		
		AgentActive.setBounds((game.GAME_WIDTH/2)-(SizeTitreAgent.width/2),game.TILES_SIZE*7,SizeTitreAgent.width,SizeTitreAgent.height);
		
		FacileAgent.setBounds((game.GAME_WIDTH/2)-(SizeMoyen.width/2)-(SizeFacile.width)-offset,game.TILES_SIZE*8,SizeFacile.width,SizeFacile.height);
		MoyenAgent.setBounds((game.GAME_WIDTH/2)-(SizeMoyen.width/2),game.TILES_SIZE*8,SizeMoyen.width,SizeMoyen.height);
		DifficileAgent.setBounds((game.GAME_WIDTH/2)+(SizeMoyen.width/2)+offset,game.TILES_SIZE*8,SizeDifficile.width,SizeDifficile.height);
		
		
		
		// Button
		
		ButtonCommencer = new JButton("Commencer");
		ButtonCommencer.setFont(new Font("Press Start 2P",Font.BOLD,(int)(Game.TILES_SIZE/2)));
		
		Dimension sizeButton = ButtonCommencer.getPreferredSize();
		
		ButtonCommencer.setBounds((game.GAME_WIDTH/2)-(int)((sizeButton.width*1.20)/2),(int)(game.TILES_SIZE*9),(int)(sizeButton.width*1.20),(int)(sizeButton.height*1.20));
		
		ButtonCommencer.setBackground(Game.buttonColor);
		ButtonCommencer.setBorder(BorderFactory.createLineBorder(Game.BorderColor,3));
		ButtonCommencer.setForeground(Game.textColor);
		ButtonCommencer.setFocusPainted(false);
		ButtonCommencer.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent e) {
		        // Call your method here
		    	startGame();
		    }
		});
		
		this.initValue();
	}
	public void startGame() {
			if(TestDimMapX() && TestDimMapY() && TestSeed()) {
				gamestates.state = gamestates.PLAYING;
				
				int x = Integer.parseInt(inputFieldX.getText());
				int y = Integer.parseInt(inputFieldY.getText());				
			    int seed = Integer.parseInt(inputSeed.getText());
				
				Random rand = new Random(seed);
				
				double tauxRecouvrementMax = rand.nextDouble(0.70,0.85);   
			    double moyenne = (x+y)/2; 
			    int recursionMax = Math.max(2,(int) Math.ceil(moyenne/10)); 
			    int nbWumpus = Math.max(1,(int) Math.ceil(moyenne/10));    			    
			    int nbOr = Math.max(1,(int) Math.ceil(moyenne/10));
			    
			   
			    
			    
			    this.game.getPlaying().refresh( x, y,  seed, recursionMax, tauxRecouvrementMax ,  nbWumpus, nbOr);
			    
			    if(this.Triche.isSelected()) {
			    	game.getPlaying().difficultes = Difficultes.triche;
			    }
			    if(this.Facile.isSelected()) {
			    	game.getPlaying().difficultes = Difficultes.facile;
			    }
			    if(this.Moyen.isSelected()) {
			    	game.getPlaying().difficultes = Difficultes.moyen;
			    }
			    if(this.Difficile.isSelected()) {
			    	game.getPlaying().difficultes = Difficultes.difficile;
			    }
			    
			    
			    
			    
			    if(this.AgentActive.isSelected()) {
			    	game.getPlaying().Enableagent = true;
			    }
			    
			    if(this.FacileAgent.isSelected()) {
			    	game.getPlaying().dif_agent = 0;
			    }
			    if(this.MoyenAgent.isSelected()) {
			    	game.getPlaying().dif_agent = 1;
			    }
			    if(this.DifficileAgent.isSelected()) {
			    	game.getPlaying().dif_agent = 2;
			    }
			    
			    this.resetFrame();
			}else {
				System.out.println("erreur dimension map");
			}
			
	}
	private boolean TestDimMapX() {
		
		if( !inputFieldX.getText().isEmpty()  ) {
			int x = Integer.parseInt(inputFieldX.getText());
			if(x <3 || x >100) {
				return false;
			}else {
				return true;
			}
		}else {
			return false;
		}
	}
private boolean TestSeed() {		
		if(!inputSeed.getText().isEmpty()) {
			if(inputSeed.getText().length() <= 8 ) {
				int x = Integer.parseInt(inputSeed.getText());
				if(x <0) {
					return false;
				}else {
					return true;
				}
			}else {
				return false;
			}			
		}else {
			return false;
		}
	}
private boolean TestDimMapY() {
		
		if(!inputFieldY.getText().isEmpty() ) {
			int y = Integer.parseInt(inputFieldY.getText());
			if( y <3 || y>100) {
				return false;
			}else {
				return true;
			}
		}else {
			return false;
		}
	}
	public void initNewGame() {
		this.initFrame();
		this.initValue();
	}
	public void initValue() {
		
		//Fields
		inputFieldX.setText("5");
		inputFieldY.setText("5");

		Random rand = new Random();
		int seed = rand.nextInt(0,99999999);		
		inputSeed.setText(String.valueOf(seed));
		
		//Checkbox
		Moyen.setSelected(true);
		MoyenAgent.setSelected(true);
		
	}
	
	public void initFrame() {
		game.getGamePanel().setVisible(false);
		game.getGameWindow().getJframe().getContentPane().setLayout(null);
		game.getGameWindow().getJframe().getContentPane().setBackground(Game.BackGroundColor);
		
		//Labels
		game.getGameWindow().getJframe().getContentPane().add(TailleCarte);	
		game.getGameWindow().getJframe().getContentPane().add(Titre);	
		game.getGameWindow().getJframe().getContentPane().add(TitreSeed);
		game.getGameWindow().getJframe().getContentPane().add(TitreOption);
		game.getGameWindow().getJframe().getContentPane().add(TitreAgent);
		
		//Checkbox

		game.getGameWindow().getJframe().getContentPane().add(Triche);
		game.getGameWindow().getJframe().getContentPane().add(Facile);
		game.getGameWindow().getJframe().getContentPane().add(Moyen);
		game.getGameWindow().getJframe().getContentPane().add(Difficile);
		game.getGameWindow().getJframe().getContentPane().add(AgentActive);
		
		//Fields
		game.getGameWindow().getJframe().add(inputFieldX);		
		game.getGameWindow().getJframe().add(inputFieldY);	
		game.getGameWindow().getJframe().add(inputSeed);		
		
		//Button		
		game.getGameWindow().getJframe().add(ButtonCommencer);	
		

		
		
	}
	
	

	public void resetFrame() {
		
		
		game.getGamePanel().setVisible(true);
		game.getGamePanel().requestFocusInWindow();
		// reset des component de la Jframe
		
		if(game.getGameWindow().getJframe().getContentPane().isAncestorOf(TailleCarte)) {
			game.getGameWindow().getJframe().getContentPane().remove(TailleCarte);
		}		
		if(game.getGameWindow().getJframe().getContentPane().isAncestorOf(Titre)) {
			game.getGameWindow().getJframe().getContentPane().remove(Titre);
		}		
		if(game.getGameWindow().getJframe().getContentPane().isAncestorOf(TitreSeed)) {
			game.getGameWindow().getJframe().getContentPane().remove(TitreSeed);
		}		
		if(game.getGameWindow().getJframe().getContentPane().isAncestorOf(TitreOption)) {
			game.getGameWindow().getJframe().getContentPane().remove(TitreOption);
		}
		if(game.getGameWindow().getJframe().getContentPane().isAncestorOf(TitreAgent)) {
			game.getGameWindow().getJframe().getContentPane().remove(TitreAgent);
		}
		if(game.getGameWindow().getJframe().getContentPane().isAncestorOf(Triche)) {
			game.getGameWindow().getJframe().getContentPane().remove(Triche);
		}
		if(game.getGameWindow().getJframe().getContentPane().isAncestorOf(Facile)) {
			game.getGameWindow().getJframe().getContentPane().remove(Facile);
		}
		if(game.getGameWindow().getJframe().getContentPane().isAncestorOf(Moyen)) {
			game.getGameWindow().getJframe().getContentPane().remove(Moyen);
		}
		if(game.getGameWindow().getJframe().getContentPane().isAncestorOf(Difficile)) {
			game.getGameWindow().getJframe().getContentPane().remove(Difficile);
		}
		if(game.getGameWindow().getJframe().getContentPane().isAncestorOf(AgentActive)) {
			game.getGameWindow().getJframe().getContentPane().remove(AgentActive);
		}
		if(game.getGameWindow().getJframe().getContentPane().isAncestorOf(inputFieldX)) {
			game.getGameWindow().getJframe().getContentPane().remove(inputFieldX);
		}
		if(game.getGameWindow().getJframe().getContentPane().isAncestorOf(inputFieldY)) {
			game.getGameWindow().getJframe().getContentPane().remove(inputFieldY);
		}
		if(game.getGameWindow().getJframe().getContentPane().isAncestorOf(inputSeed)) {
			game.getGameWindow().getJframe().getContentPane().remove(inputSeed);
		}
		if(game.getGameWindow().getJframe().getContentPane().isAncestorOf(ButtonCommencer)) {
			game.getGameWindow().getJframe().getContentPane().remove(ButtonCommencer);
		}
		if(game.getGameWindow().getJframe().getContentPane().isAncestorOf(FacileAgent)) {
			game.getGameWindow().getJframe().getContentPane().remove(FacileAgent);
		}
		
		if(game.getGameWindow().getJframe().getContentPane().isAncestorOf(MoyenAgent)) {
			game.getGameWindow().getJframe().getContentPane().remove(MoyenAgent);
		}
		
		if(game.getGameWindow().getJframe().getContentPane().isAncestorOf(DifficileAgent)) {
			game.getGameWindow().getJframe().getContentPane().remove(DifficileAgent);
		}
		
		if(game.getGameWindow().getJframe().getContentPane().isAncestorOf(TitreDiffAgent)) {
			game.getGameWindow().getJframe().getContentPane().remove(TitreDiffAgent);
		}
		
		
		
	}

	@Override
	public void update() {
		
		if(Triche.isSelected()) {
			Triche.setBorder(BorderFactory.createLineBorder(Game.buttonHighlightColor,3));
		}else {
			Triche.setBorder(BorderFactory.createLineBorder(Game.BorderColor,3));
		}
		if(Facile.isSelected()) {
			Facile.setBorder(BorderFactory.createLineBorder(Game.buttonHighlightColor,3));
		}else {
			Facile.setBorder(BorderFactory.createLineBorder(Game.BorderColor,3));
		}
		if(Moyen.isSelected()) {
			Moyen.setBorder(BorderFactory.createLineBorder(Game.buttonHighlightColor,3));
		}else {
			Moyen.setBorder(BorderFactory.createLineBorder(Game.BorderColor,3));
		}
		if(Difficile.isSelected()) {
			Difficile.setBorder(BorderFactory.createLineBorder(Game.buttonHighlightColor,3));
		}else {
			Difficile.setBorder(BorderFactory.createLineBorder(Game.BorderColor,3));
		}
		if(this.AgentActive.isSelected()) {
			AgentActive.setBorder(BorderFactory.createLineBorder(Game.buttonHighlightColor,3));
		}else {
			AgentActive.setBorder(BorderFactory.createLineBorder(Game.BorderColor,3));
		}
		if(this.FacileAgent.isSelected()) {
			FacileAgent.setBorder(BorderFactory.createLineBorder(Game.buttonHighlightColor,3));
		}else {
			FacileAgent.setBorder(BorderFactory.createLineBorder(Game.BorderColor,3));
		}
		if(this.MoyenAgent.isSelected()) {
			MoyenAgent.setBorder(BorderFactory.createLineBorder(Game.buttonHighlightColor,3));
		}else {
			MoyenAgent.setBorder(BorderFactory.createLineBorder(Game.BorderColor,3));
		}
		if(this.DifficileAgent.isSelected()) {
			DifficileAgent.setBorder(BorderFactory.createLineBorder(Game.buttonHighlightColor,3));
		}else {
			DifficileAgent.setBorder(BorderFactory.createLineBorder(Game.BorderColor,3));
		}
		
		boolean repaint = false;
		if(AgentActive.isSelected()) {
			
			
			if(!game.getGameWindow().getJframe().getContentPane().isAncestorOf(FacileAgent)) {
				game.getGameWindow().getJframe().getContentPane().add(FacileAgent);
				repaint=true;
			}
			
			if(!game.getGameWindow().getJframe().getContentPane().isAncestorOf(MoyenAgent)) {
				game.getGameWindow().getJframe().getContentPane().add(MoyenAgent);
				repaint=true;
			}
			
			if(!game.getGameWindow().getJframe().getContentPane().isAncestorOf(DifficileAgent)) {
				game.getGameWindow().getJframe().getContentPane().add(DifficileAgent);
				repaint=true;
			}
			
			if(!game.getGameWindow().getJframe().getContentPane().isAncestorOf(TitreDiffAgent)) {
				game.getGameWindow().getJframe().getContentPane().add(TitreDiffAgent);
				repaint=true;
			}
			
		}else {
			
			if(game.getGameWindow().getJframe().getContentPane().isAncestorOf(FacileAgent)) {
				game.getGameWindow().getJframe().getContentPane().remove(FacileAgent);
				repaint=true;
			}
			
			if(game.getGameWindow().getJframe().getContentPane().isAncestorOf(MoyenAgent)) {
				game.getGameWindow().getJframe().getContentPane().remove(MoyenAgent);
				repaint=true;
			}
			
			if(game.getGameWindow().getJframe().getContentPane().isAncestorOf(DifficileAgent)) {
				game.getGameWindow().getJframe().getContentPane().remove(DifficileAgent);
				repaint=true;
			}
			
			if(game.getGameWindow().getJframe().getContentPane().isAncestorOf(TitreDiffAgent)) {
				game.getGameWindow().getJframe().getContentPane().remove(TitreDiffAgent);
				repaint=true;
			}
			
		}
		if (repaint) {
			game.getGameWindow().getJframe().repaint();
		}
	}
		
	@Override
	public void draw(Graphics g) {
     
	}

	@Override
	public void MouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void MousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void MouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void MouseMouved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void KeyPressed(KeyEvent e) {

	}

	@Override
	public void KeyReleased(KeyEvent e) {		
		
	}

	public class IntegerFilter extends DocumentFilter {
        @Override
        public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr) throws BadLocationException {
            if (string.matches("[0-9]+")) {
            	
            	int value = Integer.parseInt(string);
                    super.insertString(fb, offset, string, attr);
            	
            }
        }

        @Override
        public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
            if (text.matches("[0-9]+")) {
                super.replace(fb, offset, length, text, attrs);
            }
        }

    }

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	
}
