package gamestates;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import gamestates.gamestates;
import main.Game;
import main.PlateauHandler;
import utilz.LoadSave;

public class Menu extends State {
	private int selectedButton;

	
	public Menu(Game game) {
		super(game);
		this.selectedButton = 0;
	}
	
	private void initFrame() {
		
	}
	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	
	@Override
	public void draw(Graphics g) {

		g.setColor(Game.BackGroundColor);
		g.fillRect(0, 0,  Game.GAME_WIDTH,Game.GAME_HEIGHT);
		
		
		String titre = "Wumpus World";
		g.setColor(Game.textColor);
		g.setFont(super.game.fontTitre);
		FontMetrics fm = g.getFontMetrics();
		int prefSize = fm.stringWidth(titre);
		g.drawString(titre, Game.GAME_WIDTH/2 - (int)(prefSize*0.5), Game.TILES_SIZE*3);
		
		
		
		g.setColor(Game.buttonHighlightColor);
		switch(this.selectedButton) {
			default:
				break;
			case 0:
				// jouer est select
				g.fillRect(Game.GAME_WIDTH/2 - (int)(Game.Buttonwidth*0.5) - (int)(Game.HighlightSize*0.5),  Game.GAME_HEIGHT/2 - (int)(Game.buttonnHeight*0.5) - (int)(Game.HighlightSize*0.5),Game.Buttonwidth+Game.HighlightSize, Game.buttonnHeight+Game.HighlightSize);
				break;
			case 1:
				// jouer est select
				g.fillRect(Game.GAME_WIDTH/2 - (int)(Game.Buttonwidth*0.5) - (int)(Game.HighlightSize*0.5),  Game.GAME_HEIGHT/2 - (int)(Game.buttonnHeight*0.5) - (int)(Game.HighlightSize*0.5)+Game.Offset,Game.Buttonwidth+Game.HighlightSize, Game.buttonnHeight+Game.HighlightSize);
				break;
		}
		
		
		
		g.setColor(Game.buttonColor);
		g.fillRect(Game.GAME_WIDTH/2 - (int)(Game.Buttonwidth*0.5),  Game.GAME_HEIGHT/2 - (int)(Game.buttonnHeight*0.5),Game.Buttonwidth, Game.buttonnHeight);
		
		String sousTitre = "Jouer";
		g.setFont(super.game.font);
		fm = g.getFontMetrics();
		prefSize = fm.stringWidth(sousTitre);
		int stringHeight = fm.getHeight();
		g.setColor(Game.buttonTextColor);
		g.drawString(sousTitre, Game.GAME_WIDTH/2 - (int)(prefSize*0.5), Game.GAME_HEIGHT/2+(int)(0.5*stringHeight));
		
		
		
		g.setColor(Game.buttonColor);
		g.fillRect(Game.GAME_WIDTH/2 - (int)(Game.Buttonwidth*0.5),  Game.GAME_HEIGHT/2 - (int)(Game.buttonnHeight*0.5)+Game.Offset,Game.Buttonwidth, Game.buttonnHeight);
		
		sousTitre = "Aides";
		g.setFont(super.game.font);
		fm = g.getFontMetrics();
		prefSize = fm.stringWidth(sousTitre);
		stringHeight = fm.getHeight();
		g.setColor(Game.buttonTextColor);
		g.drawString(sousTitre, Game.GAME_WIDTH/2 - (int)(prefSize*0.5), Game.GAME_HEIGHT/2+(int)(0.5*stringHeight) + Game.Offset);
		
		
		
		
		g.setColor(Game.textColor);
		String Projet = "Projet Universitaire S6 2023";
		prefSize = fm.stringWidth(Projet);
		g.drawString(Projet, (int)(Game.TILES_SIZE*0.1) , Game.GAME_HEIGHT-(int)(Game.TILES_SIZE*0.2));
		
		String Cariat = "Cariat Baptiste";
		prefSize = fm.stringWidth(Cariat);
		g.drawString(Cariat, Game.GAME_WIDTH-(prefSize)-(int)(Game.TILES_SIZE*0.1) , Game.GAME_HEIGHT-(int)(Game.TILES_SIZE*1.4));
		
		String Mecheri = "Mecheri Wassim";
		prefSize = fm.stringWidth(Mecheri);
		g.drawString(Mecheri, Game.GAME_WIDTH-(prefSize)-(int)(Game.TILES_SIZE*0.1) , Game.GAME_HEIGHT-(int)(Game.TILES_SIZE*0.8));
		
		String Burns = "Burns Elliot";
		prefSize = fm.stringWidth(Burns);
		g.drawString(Burns, Game.GAME_WIDTH-(prefSize)-(int)(Game.TILES_SIZE*0.1) , Game.GAME_HEIGHT-(int)(Game.TILES_SIZE*0.2));
	}

	@Override
	public void MouseClicked(MouseEvent e) {
	}

	@Override
	public void MousePressed(MouseEvent e) {

		
		
	}

	@Override
	public void MouseReleased(MouseEvent e) {
		int CornerX0 = Game.GAME_WIDTH/2 - (int)(Game.Buttonwidth*0.5);
		int SizeX = Game.Buttonwidth;
		int CornerX1 = CornerX0 + SizeX;
		
		int CornerY0 = Game.GAME_HEIGHT/2 - (int)(Game.buttonnHeight*0.5);
		int SizeY = Game.buttonnHeight;
		int CornerY1 = CornerY0 + SizeY;
		
		if(e.getX() >= CornerX0 && e.getX() <= CornerX1 && e.getY() >= CornerY0 && e.getY() <= CornerY1 ) {
			this.selectedButton = 0;
			gamestates.state = gamestates.NEWGAME;
			game.getNewGame().initNewGame();
		}
		//g.fillRect(Game.GAME_WIDTH/2 - (int)(Game.Buttonwidth*0.5),  Game.GAME_HEIGHT/2 - (int)(Game.buttonnHeight*0.5)+Game.Offset,Game.Buttonwidth, Game.buttonnHeight);

		CornerY0 = Game.GAME_HEIGHT/2 - (int)(Game.buttonnHeight*0.5) + +Game.Offset;
		SizeY = Game.buttonnHeight;
		CornerY1 = CornerY0 + SizeY;
		
		if(e.getX() >= CornerX0 && e.getX() <= CornerX1 && e.getY() >= CornerY0 && e.getY() <= CornerY1 ) {
			this.selectedButton = 1;
			gamestates.state = gamestates.Regle;
		}
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {

		
		
	}
	@Override
	public void MouseMouved(MouseEvent e) {
		//g.fillRect(Game.GAME_WIDTH/2 - (int)(Game.Buttonwidth*0.5),  Game.GAME_HEIGHT/2 - (int)(Game.buttonnHeight*0.5),Game.Buttonwidth, Game.buttonnHeight);
		int CornerX0 = Game.GAME_WIDTH/2 - (int)(Game.Buttonwidth*0.5);
		int SizeX = Game.Buttonwidth;
		int CornerX1 = CornerX0 + SizeX;
		
		int CornerY0 = Game.GAME_HEIGHT/2 - (int)(Game.buttonnHeight*0.5);
		int SizeY = Game.buttonnHeight;
		int CornerY1 = CornerY0 + SizeY;
		
		if(e.getX() >= CornerX0 && e.getX() <= CornerX1 && e.getY() >= CornerY0 && e.getY() <= CornerY1 ) {
			this.selectedButton = 0;
		}
		//g.fillRect(Game.GAME_WIDTH/2 - (int)(Game.Buttonwidth*0.5),  Game.GAME_HEIGHT/2 - (int)(Game.buttonnHeight*0.5)+Game.Offset,Game.Buttonwidth, Game.buttonnHeight);

		CornerY0 = Game.GAME_HEIGHT/2 - (int)(Game.buttonnHeight*0.5) + +Game.Offset;
		SizeY = Game.buttonnHeight;
		CornerY1 = CornerY0 + SizeY;
		
		if(e.getX() >= CornerX0 && e.getX() <= CornerX1 && e.getY() >= CornerY0 && e.getY() <= CornerY1 ) {
			this.selectedButton = 1;
		}
		
	}

	@Override
	public void KeyPressed(KeyEvent e) {		
		if(e.getKeyCode()== KeyEvent.VK_ENTER) {		
			switch(this.selectedButton) {
				default:
					break;
				case 0:
					gamestates.state = gamestates.NEWGAME;
					game.getNewGame().initNewGame();
					break;
				case 1:
					gamestates.state = gamestates.Regle;
					break;
			}
			
		}
		if(e.getKeyCode() == KeyEvent.VK_UP) {
			if(this.selectedButton == 0 ) {
				this.selectedButton = 1;
			}else {
				this.selectedButton--;
			}
		}
		if(e.getKeyCode() == KeyEvent.VK_DOWN) {
			if(this.selectedButton == 1 ) {
				this.selectedButton = 0;
			}else {
				this.selectedButton++;
			}
		}
	}
		
		

	@Override
	public void KeyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	

}
